// CashVoucherDialog.cpp : implementation file
//
/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   03/08/13    Send message to reinit printer template when the Reset button is pressed.
Robert Fuller   09/22/14    Added a button in cash voucher tech screen to clear queued up drawing tickets.
*/

#include "stdafx.h"
#include "xp_controller.h"
#include "CashVoucherDialog.h"


// CCashVoucherDialog dialog

IMPLEMENT_DYNAMIC(CCashVoucherDialog, CDialog)

CCashVoucherDialog::CCashVoucherDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CCashVoucherDialog::IDD, pParent)
{

}

CCashVoucherDialog::~CCashVoucherDialog()
{
}

void CCashVoucherDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CASH_VOUCHER_PRINTER_LIST, m_cash_voucher_printer_listbox);
}


BEGIN_MESSAGE_MAP(CCashVoucherDialog, CDialog)
	ON_BN_CLICKED(IDC_CASH_VOUCHER_EXIT_BUTTON, &CCashVoucherDialog::OnBnClickedCashVoucherExitButton)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_CASH_VOUCHER_PRINTER_RESET_BUTTON, &CCashVoucherDialog::OnBnClickedCashVoucherPrinterResetButton)
	ON_BN_CLICKED(IDC_CASH_VOUCHER_PRINTER_STATUS_BUTTON, &CCashVoucherDialog::OnBnClickedCashVoucherPrinterStatusButton)
	ON_BN_CLICKED(IDC_CASH_VOUCHER_PRINTER_QUEUE_BUTTON, &CCashVoucherDialog::OnBnClickedCashVoucherPrinterQueueButton)
	ON_BN_CLICKED(IDC_CASH_VOUCHER_TEST_PRINTER_BUTTON, &CCashVoucherDialog::OnBnClickedCashVoucherTestPrinterButton)
	ON_BN_CLICKED(IDC_CASH_VOUCHER_PRINTER_KEYBOARD_BUTTON, &CCashVoucherDialog::OnBnClickedCashVoucherPrinterKeyboardButton)
	ON_BN_CLICKED(IDC_CASH_VOUCHER_PRINTER_SAVE_BUTTON, &CCashVoucherDialog::OnBnClickedCashVoucherPrinterSaveButton)
	ON_BN_CLICKED(IDC_CASH_VOUCHER_DELETE_PENDING_TICKETS_BUTTON, &CCashVoucherDialog::OnBnClickedCashVoucherDeletePendingTicketsButton)
END_MESSAGE_MAP()


// CCashVoucherDialog message handlers

void CCashVoucherDialog::OnBnClickedCashVoucherExitButton()
{
	DestroyWindow();
}

void CCashVoucherDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CCashVoucherDialog::OnBnClickedCashVoucherPrinterResetButton()
{
    m_cash_voucher_printer_listbox.ResetContent();
    m_cash_voucher_printer_listbox.AddString("Cash Voucher Printer Reset.");
    m_cash_voucher_printer_listbox.AddString("");

    if (!theApp.pGbProManager->memory_gb_pro_settings.cash_voucher_printer_port)
        m_cash_voucher_printer_listbox.AddString("Cash Voucher printer does not exist.");
    else
    {
        theApp.pGbProManager->ResetCvPrinter();
        m_cash_voucher_printer_listbox.AddString("Cash Voucher printer reset.");
		theApp.pGbProManager->ProcessCvPrinterReset();
    }
}

void CCashVoucherDialog::OnBnClickedCashVoucherPrinterStatusButton()
{
    m_cash_voucher_printer_listbox.ResetContent();
    m_cash_voucher_printer_listbox.AddString("Cash Voucher Printer Status.");
    m_cash_voucher_printer_listbox.AddString("");

    if (!theApp.pGbProManager->memory_gb_pro_settings.cash_voucher_printer_port)
        m_cash_voucher_printer_listbox.AddString("Cash Voucher printer does not exist.");
    else
    {
        if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[0] & 0x40 &&
            theApp.pGbProManager->cv_printer_idle_msg_status_bits[1] & 0x40 &&
            theApp.pGbProManager->cv_printer_idle_msg_status_bits[2] & 0x40 &&
            theApp.pGbProManager->cv_printer_idle_msg_status_bits[3] & 0x40 &&
            theApp.pGbProManager->cv_printer_idle_msg_status_bits[4] & 0x40)
        {
            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[0] & 0x10)
                m_cash_voucher_printer_listbox.AddString("SYSTEM ERROR!");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[0] & 0x20)
                m_cash_voucher_printer_listbox.AddString("PRINTER BUSY PROCESSING COMMANDS");
            else
                m_cash_voucher_printer_listbox.AddString("PRINTER IDLE");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[0] & 0x08)
                m_cash_voucher_printer_listbox.AddString("PRINT HEAD IS UP ERROR");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[0] & 0x04)
                m_cash_voucher_printer_listbox.AddString("PAPER OUT ERROR");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[0] & 0x02)
                m_cash_voucher_printer_listbox.AddString("PRINT HEAD ERROR, SERVICE PRINTER");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[0] & 0x01)
                m_cash_voucher_printer_listbox.AddString("VOLTAGE SUPPLY OUT-OF-RANGE ERROR");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[1] & 0x20)
                m_cash_voucher_printer_listbox.AddString("PRINT HEAD TEMPERATURE ERROR");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[1] & 0x10)
                m_cash_voucher_printer_listbox.AddString("LIBRARY REFERENCE ERROR");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[1] & 0x08)
                m_cash_voucher_printer_listbox.AddString("PRINT REGION DATA ERROR");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[1] & 0x04)
                m_cash_voucher_printer_listbox.AddString("LIBRARY LOAD ERROR");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[1] & 0x02)
                m_cash_voucher_printer_listbox.AddString("BUFFER OVERFLOW ERROR");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[1] & 0x01)
                m_cash_voucher_printer_listbox.AddString("PRINT JOB TOO LARGE ERROR");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[2] & 0x20)
                m_cash_voucher_printer_listbox.AddString("PRINTER COMMAND ERROR");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[2] & 0x10)
                m_cash_voucher_printer_listbox.AddString("PRINTER LIBRARY CORRUPTED ERROR, RELOAD");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[2] & 0x08)
                m_cash_voucher_printer_listbox.AddString("TICKET IN CHUTE, PLEASE REMOVE");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[2] & 0x04)
                m_cash_voucher_printer_listbox.AddString("FLASH PROGRAMMING ERROR");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[2] & 0x02)
                m_cash_voucher_printer_listbox.AddString("PRINTER OFFLINE ERROR");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[2] & 0x01)
                m_cash_voucher_printer_listbox.AddString("MISSING PAPER SUPPLY INDEX MARKS ERROR");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[3] & 0x04)
                m_cash_voucher_printer_listbox.AddString("PAPER CUTTER ERROR");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[3] & 0x02)
                m_cash_voucher_printer_listbox.AddString("POSSIBLE PAPER JAM ERROR");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[3] & 0x01)
                m_cash_voucher_printer_listbox.AddString("PAPER ROLL LOW");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[4] & 0x04)
                m_cash_voucher_printer_listbox.AddString("PRINTER DRAWER OPEN");

            if (theApp.pGbProManager->cv_printer_idle_msg_status_bits[4] & 0x01)
                m_cash_voucher_printer_listbox.AddString("PRINTER RESET OR POWER UP");
        }
        else
            m_cash_voucher_printer_listbox.AddString("Cash Voucher printer not responding.");
    }
}

void CCashVoucherDialog::OnBnClickedCashVoucherPrinterQueueButton()
{
 CString edit_string;

    m_cash_voucher_printer_listbox.ResetContent();
    m_cash_voucher_printer_listbox.AddString("Cash Voucher Printer Queues.");
	m_cash_voucher_printer_listbox.AddString("");

    if (theApp.pGbProManager->cv_pending_printer_tickets_queue.empty())
        edit_string = "Cash Vouchers pending to be printed: 0";
    else
        edit_string.Format("Cash Vouchers pending to be printed: %u",
            theApp.pGbProManager->cv_pending_printer_tickets_queue.size());

    m_cash_voucher_printer_listbox.AddString(edit_string);

    if (theApp.pGbProManager->cv_printer_queue.empty())
        edit_string = "Cash Voucher printer messages to be transmitted: 0";
    else
        edit_string.Format("Cash Voucher printer messages to be transmitted: %u",
            theApp.pGbProManager->cv_printer_queue.size());

    m_cash_voucher_printer_listbox.AddString(edit_string);
}

void CCashVoucherDialog::OnBnClickedCashVoucherTestPrinterButton()
{
 CashVoucherTickets test_ticket;

	if (!theApp.pGbProManager->memory_gb_pro_settings.cash_voucher_printer_port)
		MessageWindow(false, "ERROR!", "Cash Voucher Printer Port Not Set");
	else
	{
		MessageWindow(false, "PRINTING TICKET", "Check Printer For Test Ticket\nIf No Ticket, Check Printer IP Address And/Or Port");

		memset(&test_ticket, 0, sizeof(test_ticket));
		sprintf_s(test_ticket.player_name, sizeof(test_ticket.player_name), "TEST ONLY");
		test_ticket.player_account = 123456789;
		test_ticket.cash_voucher_amount = 1;
		test_ticket.game_ucmc_id = 54321;
		test_ticket.game_mux_id = 1;
		test_ticket.game_denomination = 25;
		test_ticket.credits_wagered = 5;
		test_ticket.time_stamp = time(NULL);
		test_ticket.suppress_printing_on_lat = true;

		theApp.pGbProManager->cv_pending_printer_tickets_queue.push(test_ticket);
	}
}

void CCashVoucherDialog::OnBnClickedCashVoucherPrinterKeyboardButton()
{
	ShellExecute(NULL, "open", "osk.exe", NULL, NULL, SW_SHOWMAXIMIZED);
}

BOOL CCashVoucherDialog::OnInitDialog()
{
 BYTE *ip_address_ptr;
 CString edit_string;

	CDialog::OnInitDialog();

	if (theApp.pGbProManager)
	{
		ip_address_ptr = (BYTE*)&theApp.pGbProManager->memory_gb_pro_settings.cash_voucher_printer_ip_address;

		edit_string.Format("%u.%u.%u.%u:%u", ip_address_ptr[0], ip_address_ptr[1], ip_address_ptr[2], ip_address_ptr[3],
			theApp.pGbProManager->memory_gb_pro_settings.cash_voucher_printer_port);
		SetDlgItemText(IDC_CASH_VOUCHER_PRINTER_IP_PORT_EDIT, edit_string);
	}

 return TRUE;
}

void CCashVoucherDialog::OnBnClickedCashVoucherPrinterSaveButton()
{
 BYTE *ip_address_ptr;
 WORD ip_port;
 DWORD ip_address;
 CString edit_string;
 FileGbProManagerSettings file_settings;

	if (theApp.pGbProManager)
	{
		GetDlgItemText(IDC_CASH_VOUCHER_PRINTER_IP_PORT_EDIT, edit_string);

		if (edit_string.GetLength())
		{
			theApp.GetIpAddressAndPort(edit_string, &ip_address, &ip_port);
			ip_address_ptr = (BYTE*)&ip_address;
			edit_string.Format("IP Address: %u.%u.%u.%u\nIP Port: %u\nA system reboot is required for changes.",
				ip_address_ptr[0], ip_address_ptr[1], ip_address_ptr[2], ip_address_ptr[3], ip_port);

			EnableWindow(FALSE);
			MessageWindow(true, "SAVE DATA?", edit_string);
			while (theApp.message_window_queue.empty())
				theApp.OnIdle(0);
			EnableWindow();

			if (theApp.message_window_queue.front() == IDOK && fileRead(GB_PRO_SETTINGS_INI, 0, &file_settings, sizeof(file_settings)))
			{
				theApp.pGbProManager->memory_gb_pro_settings.cash_voucher_printer_ip_address = ip_address;
				theApp.pGbProManager->memory_gb_pro_settings.cash_voucher_printer_port = ip_port;
				file_settings.memory.cash_voucher_printer_ip_address = ip_address;
				file_settings.memory.cash_voucher_printer_port = ip_port;
				fileWrite(GB_PRO_SETTINGS_INI, 0, &file_settings, sizeof(file_settings));

				edit_string.Format("%u.%u.%u.%u:%u", ip_address_ptr[0], ip_address_ptr[1], ip_address_ptr[2], ip_address_ptr[3], ip_port);
				SetDlgItemText(IDC_CASH_VOUCHER_PRINTER_IP_PORT_EDIT, edit_string);
			}
		}
	}
}

void CCashVoucherDialog::OnBnClickedCashVoucherDeletePendingTicketsButton()
{
	theApp.DeletePendingCashVoucherTickets();
}
