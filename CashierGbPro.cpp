/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   05/01/09    Removed code associated with now defunct Million Dollar Ticket lottery system.
Dave Elquist    06/24/05    Initial Revision.
*/


// CashierGbPro.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include ".\cashiergbpro.h"
#include "TimerWindow.h"


// CCashierGbPro dialog

IMPLEMENT_DYNAMIC(CCashierGbPro, CDialog)
CCashierGbPro::CCashierGbPro(CWnd* pParent /*=NULL*/)
	: CDialog(CCashierGbPro::IDD, pParent)
{
    current_edit_focus = 0;
}

CCashierGbPro::~CCashierGbPro()
{
}

void CCashierGbPro::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CCashierGbPro, CDialog)
	ON_BN_CLICKED(IDC_CASHIER_MDT_PAY_BUTTON, OnBnClickedCashierMdtPayButton)
	ON_BN_CLICKED(IDC_CASHIER_MDT_CHECK_BUTTON, OnBnClickedCashierMdtCheckButton)
	ON_BN_CLICKED(IDC_CASHIER_MDT_DRAW_RESULTS_BUTTON, OnBnClickedCashierMdtDrawResultsButton)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_CASHIER_MDT_EXIT_BUTTON, OnBnClickedCashierMdtExitButton)
	ON_BN_CLICKED(IDC_CASHIER_MDT_BUTTON1, OnBnClickedCashierMdtButton1)
	ON_BN_CLICKED(IDC_CASHIER_MDT_BUTTON2, OnBnClickedCashierMdtButton2)
	ON_BN_CLICKED(IDC_CASHIER_MDT_BUTTON3, OnBnClickedCashierMdtButton3)
	ON_BN_CLICKED(IDC_CASHIER_MDT_BUTTON4, OnBnClickedCashierMdtButton4)
	ON_BN_CLICKED(IDC_CASHIER_MDT_BUTTON5, OnBnClickedCashierMdtButton5)
	ON_BN_CLICKED(IDC_CASHIER_MDT_BUTTON6, OnBnClickedCashierMdtButton6)
	ON_BN_CLICKED(IDC_CASHIER_MDT_BUTTON7, OnBnClickedCashierMdtButton7)
	ON_BN_CLICKED(IDC_CASHIER_MDT_BUTTON8, OnBnClickedCashierMdtButton8)
	ON_BN_CLICKED(IDC_CASHIER_MDT_BUTTON9, OnBnClickedCashierMdtButton9)
	ON_BN_CLICKED(IDC_CASHIER_MDT_BUTTON0, OnBnClickedCashierMdtButton0)
	ON_BN_CLICKED(IDC_CASHIER_MDT_CLEAR_BUTTON, OnBnClickedCashierMdtClearButton)
	ON_EN_SETFOCUS(IDC_CASHIER_MDT_DAY_EDIT, OnEnSetfocusCashierMdtDayEdit)
	ON_EN_SETFOCUS(IDC_CASHIER_MDT_NUMBER_EDIT, OnEnSetfocusCashierMdtNumberEdit)
END_MESSAGE_MAP()


// CCashierGbPro message handlers

void CCashierGbPro::OnBnClickedCashierMdtPayButton()
{
}

void CCashierGbPro::OnBnClickedCashierMdtCheckButton()
{
}

void CCashierGbPro::OnBnClickedCashierMdtDrawResultsButton()
{
}

void CCashierGbPro::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CCashierGbPro::OnBnClickedCashierMdtExitButton()
{
}

void CCashierGbPro::ProcessButtonClick(BYTE button_press)
{
}

void CCashierGbPro::OnBnClickedCashierMdtButton1()
{
}

void CCashierGbPro::OnBnClickedCashierMdtButton2()
{
}

void CCashierGbPro::OnBnClickedCashierMdtButton3()
{
}

void CCashierGbPro::OnBnClickedCashierMdtButton4()
{
}

void CCashierGbPro::OnBnClickedCashierMdtButton5()
{
}

void CCashierGbPro::OnBnClickedCashierMdtButton6()
{
}

void CCashierGbPro::OnBnClickedCashierMdtButton7()
{
}

void CCashierGbPro::OnBnClickedCashierMdtButton8()
{
}

void CCashierGbPro::OnBnClickedCashierMdtButton9()
{
}

void CCashierGbPro::OnBnClickedCashierMdtButton0()
{
}

void CCashierGbPro::OnBnClickedCashierMdtClearButton()
{
}

void CCashierGbPro::OnEnSetfocusCashierMdtDayEdit()
{
}

void CCashierGbPro::OnEnSetfocusCashierMdtNumberEdit()
{
                }
