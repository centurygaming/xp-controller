/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   06/09/09    Added code for custom picture buttons.
Dave Elquist    06/09/05    Initial Revision.
*/


#pragma once
#include "FixedButton.h"


// CCashierReports dialog

class CCashierReports : public CDialog
{
	DECLARE_DYNAMIC(CCashierReports)

public:
	CCashierReports(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCashierReports();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_CASHIER_REPORTS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnPaint();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCashierReportsDispenserButton();
	afx_msg void OnBnClickedCashierReportsTicketsButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedCashierReportsExitButton();

private:
	CFixedButton dispenser_button;
	CFixedButton tickets_button;
	CFixedButton exit_button;
};
