/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	05/02/12	Added support for the 4 cassette Puloon dispenser
Robert Fuller	09/17/12	Added code to fill the ucmc id field in the dispense message we send to the IDC for accounting purposes.
Robert Fuller   06/28/10    Added code to keep track of the last impress amount. 
Robert Fuller   06/10/10    Added code to support the new $5 $20 LCDM-2000 configuration. 
Robert Fuller   05/02/08    Added code to support LCDM2000 dispenser.
Robert Fuller   05/02/08    Added code to support CD2000 dispenser.
Robert Fuller   11/05/07    Added code to support Diebold dispenser.
Dave Elquist    07/15/05    Changed dispenser(lat) message from the Stratus.
Dave Elquist    05/26/05    Initial Revision.
*/


// CollectorImpressSet.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "CollectorImpressSet.h"
#include ".\collectorimpressset.h"


// CCollectorImpressSet dialog

IMPLEMENT_DYNAMIC(CCollectorImpressSet, CDialog)
CCollectorImpressSet::CCollectorImpressSet(CWnd* pParent /*=NULL*/)
	: CDialog(CCollectorImpressSet::IDD, pParent)
    , m_idc_collector_100_static(_T("")), m_idc_collector_20_static(_T(""))
{
    current_edit_focus = 0;
    impress100 = 0;
    impress20 = 0;
}

CCollectorImpressSet::~CCollectorImpressSet()
{
}

void CCollectorImpressSet::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_COLLECTOR_100_STATIC, m_idc_collector_100_static);
	DDX_Text(pDX, IDC_COLLECTOR_20_STATIC, m_idc_collector_20_static);
	DDX_Control(pDX, IDC_COLLECTOR_IMPRESS_20_EDIT, m_idc_collector_impress_20_edit);
}


BEGIN_MESSAGE_MAP(CCollectorImpressSet, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON9, OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON10, OnBnClickedButton10)
	ON_BN_CLICKED(IDC_BUTTON11, OnBnClickedButton11)
	ON_BN_CLICKED(IDC_BUTTON12, OnBnClickedButton12)
	ON_EN_SETFOCUS(IDC_COLLECTOR_IMPRESS_100_EDIT, OnEnSetfocusCollectorImpress100Edit)
	ON_EN_SETFOCUS(IDC_COLLECTOR_IMPRESS_20_EDIT, OnEnSetfocusCollectorImpress20Edit)
	ON_BN_CLICKED(IDC_BUTTON13, OnBnClickedButton13)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


void CCollectorImpressSet::ProcessButtonClick(BYTE button_press)
{
 CString edit_string;

    if (current_edit_focus == IDC_COLLECTOR_IMPRESS_100_EDIT)
    {
        impress100 = impress100 * 10 + button_press;
        edit_string.Format("$%9.2f", (double)impress100 / 100);
        SetDlgItemText(IDC_COLLECTOR_IMPRESS_100_EDIT, edit_string);
    }
    else if (current_edit_focus == IDC_COLLECTOR_IMPRESS_20_EDIT)
    {
        impress20 = impress20 * 10 + button_press;
        edit_string.Format("$%9.2f", (double)impress20 / 100);
        SetDlgItemText(IDC_COLLECTOR_IMPRESS_20_EDIT, edit_string);
    }
}

void CCollectorImpressSet::OnBnClickedButton1()
{
    ProcessButtonClick(1);
}

void CCollectorImpressSet::OnBnClickedButton2()
{
    ProcessButtonClick(2);
}

void CCollectorImpressSet::OnBnClickedButton3()
{
    ProcessButtonClick(3);
}

void CCollectorImpressSet::OnBnClickedButton4()
{
    ProcessButtonClick(4);
}

void CCollectorImpressSet::OnBnClickedButton5()
{
    ProcessButtonClick(5);
}

void CCollectorImpressSet::OnBnClickedButton6()
{
    ProcessButtonClick(6);
}

void CCollectorImpressSet::OnBnClickedButton7()
{
    ProcessButtonClick(7);
}

void CCollectorImpressSet::OnBnClickedButton8()
{
    ProcessButtonClick(8);
}

void CCollectorImpressSet::OnBnClickedButton9()
{
    ProcessButtonClick(9);
}

void CCollectorImpressSet::OnBnClickedButton10()
{
    ProcessButtonClick(0);
}

void CCollectorImpressSet::OnBnClickedButton11()
{
 CString edit_string;

    switch (theApp.memory_dispenser_config.dispenser_type)
    {
        case DISPENSER_TYPE_ECASH:
        case DISPENSER_TYPE_DIEBOLD:
        case DISPENSER_TYPE_CD2000:
        case DISPENSER_TYPE_LCDM2000_20_100:
        case DISPENSER_TYPE_LCDM4000_20_100:
            edit_string.Format("$100 IMPRESS: $%9.2f\n $20 IMPRESS: $%9.2f",
            (double)impress100 / 100, (double)impress20 / 100);
            break;
        default:
            edit_string.Format("IMPRESS: $%9.2f", (double)impress100 / 100);
            break;
    }

    EnableWindow(FALSE);
    MessageWindow(true, "SAVE DATA?", edit_string);

    while (theApp.message_window_queue.empty())
        theApp.OnIdle(0);

    EnableWindow();

    if (theApp.message_window_queue.front() != IDOK)
    {
        SetDlgItemText(IDC_COLLECTOR_IMPRESS_100_EDIT, "");

        if ((theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_ECASH) ||
            (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CD2000))
            SetDlgItemText(IDC_COLLECTOR_IMPRESS_20_EDIT, "");
    }
    else
    {
        theApp.dispenserControl.memory_dispenser_values.last_impress_amount1 =
            theApp.dispenserControl.memory_dispenser_values.impress_amount1;

        theApp.dispenserControl.memory_dispenser_values.last_impress_amount2 =
            theApp.dispenserControl.memory_dispenser_values.impress_amount2;

        theApp.dispenserControl.memory_dispenser_values.impress_amount1 = impress100;
        theApp.dispenserControl.memory_dispenser_values.impress_amount2 = impress20;
        theApp.dispenserControl.WriteDispenserFile();

        theApp.printer_busy = true;
        theApp.PrinterHeader(0);
        theApp.PrinterData(" IMPRESS SET\n");
        theApp.PrinterId();

        switch (theApp.memory_dispenser_config.dispenser_type)
        {
            case DISPENSER_TYPE_ECASH:
            case DISPENSER_TYPE_CD2000:
            case DISPENSER_TYPE_DIEBOLD:
            case DISPENSER_TYPE_LCDM2000_20_100:
            case DISPENSER_TYPE_LCDM4000_20_100:
                edit_string.Format("$100 IMPRESS:%9.2f\n",
                    (double)theApp.dispenserControl.memory_dispenser_values.impress_amount1 / 100);
                theApp.PrinterData(edit_string.GetBuffer(0));
                edit_string.Format("$ 20 IMPRESS:%9.2f\n\n",
                    (double)theApp.dispenserControl.memory_dispenser_values.impress_amount2 / 100);
                theApp.PrinterData(edit_string.GetBuffer(0));
                break;
            default:
                edit_string.Format(" IMPRESS:%9.2f\n",
                    (double)theApp.dispenserControl.memory_dispenser_values.impress_amount1 / 100);
                theApp.PrinterData(edit_string.GetBuffer(0));
                break;
        }

        theApp.printer_busy = false;
        theApp.SendDispenserBillCount(true, 0);

        DestroyWindow();
    }
}

void CCollectorImpressSet::OnBnClickedButton12()
{
    if (current_edit_focus == IDC_COLLECTOR_IMPRESS_100_EDIT)
    {
        impress100 = 0;
        SetDlgItemText(IDC_COLLECTOR_IMPRESS_100_EDIT, "");
    }
    else if (current_edit_focus == IDC_COLLECTOR_IMPRESS_20_EDIT)
    {
        impress20 = 0;
        SetDlgItemText(IDC_COLLECTOR_IMPRESS_20_EDIT, "");
    }
}

void CCollectorImpressSet::OnEnSetfocusCollectorImpress100Edit()
{
    current_edit_focus = IDC_COLLECTOR_IMPRESS_100_EDIT;
}

void CCollectorImpressSet::OnEnSetfocusCollectorImpress20Edit()
{
    current_edit_focus = IDC_COLLECTOR_IMPRESS_20_EDIT;
}

void CCollectorImpressSet::OnBnClickedButton13()
{
    DestroyWindow();
}

void CCollectorImpressSet::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

BOOL CCollectorImpressSet::OnInitDialog()
{
	CDialog::OnInitDialog();

    switch (theApp.memory_dispenser_config.dispenser_type)
    {
        case DISPENSER_TYPE_ECASH:
        case DISPENSER_TYPE_CD2000:
        case DISPENSER_TYPE_DIEBOLD:
        case DISPENSER_TYPE_LCDM2000_20_100:
        case DISPENSER_TYPE_LCDM4000_20_100:
            m_idc_collector_100_static = "$100";
            m_idc_collector_20_static = "$20 Impress";
            break;
        default:
            m_idc_collector_impress_20_edit.DestroyWindow();
            break;
    };

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
