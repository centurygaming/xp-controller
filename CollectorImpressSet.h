/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    07/15/05    Moved function.
Dave Elquist    05/26/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"


// CCollectorImpressSet dialog

class CCollectorImpressSet : public CDialog
{
	DECLARE_DYNAMIC(CCollectorImpressSet)

public:
	CCollectorImpressSet(CWnd* pParent = NULL);   // standard constructor
	virtual ~CCollectorImpressSet();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_COLLECTOR_IMPRESS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButton6();
	afx_msg void OnBnClickedButton7();
	afx_msg void OnBnClickedButton8();
	afx_msg void OnBnClickedButton9();
	afx_msg void OnBnClickedButton10();
	afx_msg void OnBnClickedButton11();
	afx_msg void OnBnClickedButton12();
	afx_msg void OnEnSetfocusCollectorImpress100Edit();
	afx_msg void OnEnSetfocusCollectorImpress20Edit();
	afx_msg void OnBnClickedButton13();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);

//private functions
private:
    void ProcessButtonClick(BYTE button_press);

// private variables
private:
	CString m_idc_collector_100_static;
	CString m_idc_collector_20_static;

	CEdit m_idc_collector_impress_20_edit;

    DWORD current_edit_focus, impress100, impress20;
};
