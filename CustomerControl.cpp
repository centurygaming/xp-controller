/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    07/21/05    Initial Revision.
*/


// CustomerControl.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "CustomerControl.h"
#include ".\customercontrol.h"


IMPLEMENT_DYNCREATE(CCustomerControlListen, CAsyncSocket)
IMPLEMENT_DYNCREATE(CCustomerControlClient, CMyAsyncSocket)


CCustomerControlClient::CCustomerControlClient(CCustomerControlListen *pSocket /* = NULL */)
{
    delete_socket = false;
    validated_level = 0;
    client_address = 0;
    pServerSocket = pSocket;

    memset(&rx_buffer, 0, sizeof(rx_buffer));
    memset(&client_data_display, 0, sizeof(client_data_display));
}

CCustomerControlClient::~CCustomerControlClient()
{
}

void CCustomerControlClient::OnClose(int nErrorCode)
{
 int iii;
 EventMessage event_message;

    for (iii=0; iii < MAXIMUM_CUSTOMER_CONTROL_CLIENTS; iii++)
    {
        if (pServerSocket && pServerSocket->pClient[iii] && pServerSocket->pClient[iii]->m_hSocket == m_hSocket)
        {
            delete_socket = true;

            while (!client_message_queue.empty())
                client_message_queue.pop();

            memset(&event_message, 0, sizeof(event_message));
            event_message.head.time_stamp = time(NULL);
            event_message.head.event_type = CC_CLIENT_SOCKET_CLOSE;
            event_message.head.data_length = 4;
            memcpy(event_message.data, &client_address, 4);
            theApp.event_message_queue.push(event_message);

            client_address = 0;
            memset(&client_data_display, 0, sizeof(client_data_display));
        }
    }

	CMyAsyncSocket::OnClose(nErrorCode);
}


CCustomerControlListen::CCustomerControlListen()
{
 int iii;

    for (iii=0; iii < MAXIMUM_CUSTOMER_CONTROL_CLIENTS; iii++)
        pClient[iii] = NULL;
}

CCustomerControlListen::~CCustomerControlListen()
{
 int iii;

    for (iii=0; iii < MAXIMUM_CUSTOMER_CONTROL_CLIENTS; iii++)
    {
        if (pClient[iii])
        {
            if (pClient[iii]->m_hSocket != INVALID_SOCKET)
                pClient[iii]->Close();

            delete pClient[iii];
        }
    }
}

void CCustomerControlListen::OnAccept(int nErrorCode)
{
 int iii, bytes_sent, last_error;
 UINT client_port;
 CString message, client_address;
 EventMessage event_message;
 SocketMessage client_message;
 CCustomerControlClient *pNewSocket = new CCustomerControlClient(this);

    if (!Accept(*pNewSocket))
    {
        message.Format("CCustomerControlListen::OnAccept - error %d.", GetLastError());
        logMessage(message);
    }
    else
    {
        if (!pNewSocket->GetPeerName(client_address, client_port))
            client_address = "0.0.0.0";
        else
            theApp.GetIpAddressAndPort(client_address, &pNewSocket->client_address);

        if (!pNewSocket->client_address)
            logMessage("CCustomerControlListen::OnAccept - error getting client ip address.");
        else
        {
            for (iii=0; iii < MAXIMUM_CUSTOMER_CONTROL_CLIENTS; iii++)
            {
                if (!pClient[iii])
                {
                    pClient[iii] = pNewSocket;
                    pNewSocket = NULL;
                    break;
                }
            }

            if (pNewSocket)
            {
                logMessage("CCustomerControlListen::OnAccept - maximum connnections exceeded: " + client_address);

                // deny request
                memset(&client_message, 0, sizeof(client_message));
                client_message.head.header = LONG_HEADER_IDENTIFIER;
                client_message.head.command = CC_CLIENT_ASCII_STRING;
                sprintf_s(client_message.data, sizeof(client_message.data), "Connection denied. Too many active connections.");
                client_message.head.data_length = (DWORD)strlen(client_message.data);
                pNewSocket->Send(&client_message, sizeof(client_message.head) + client_message.head.data_length);
            }
            else
            {
                memset(&event_message, 0, sizeof(event_message));
                event_message.head.time_stamp = time(NULL);
                event_message.head.event_type = CC_CLIENT_INITIAL_CONNECTION;
                event_message.head.data_length = client_address.GetLength();
                memcpy(event_message.data, client_address.GetBuffer(0), event_message.head.data_length);
                theApp.event_message_queue.push(event_message);

                memset(&client_message, 0, sizeof(client_message));
                client_message.head.header = LONG_HEADER_IDENTIFIER;
                client_message.head.command = CC_CLIENT_PASSWORD_PROMPT;

				bytes_sent = pClient[iii]->Send(&client_message, sizeof(client_message.head));

				if (bytes_sent == SOCKET_ERROR)
				{
					last_error = GetLastError();

					if (last_error != WSAEINPROGRESS && last_error != WSAEWOULDBLOCK)
						pClient[iii]->Close();
				}
				else if (bytes_sent != sizeof(client_message.head))
					pClient[iii]->Close();
            }
        }
    }

    if (pNewSocket)
    {
        if (pNewSocket->m_hSocket != INVALID_SOCKET)
            pNewSocket->Close();

        delete pNewSocket;
    }

	CAsyncSocket::OnAccept(nErrorCode);
}

void CCustomerControlListen::OnClose(int nErrorCode)
{
 int iii;

    for (iii=0; iii < MAXIMUM_CUSTOMER_CONTROL_CLIENTS; iii++)
    {
        if (pClient[iii])
        {
            if (pClient[iii]->m_hSocket != INVALID_SOCKET)
                pClient[iii]->Close();

            delete pClient[iii];
            pClient[iii] = NULL;
        }
    }

	CAsyncSocket::OnClose(nErrorCode);
}

void CCustomerControlListen::CheckCustomerControlMessageQueue()
{
 int iii, bytes_sent, last_error;
 SocketMessage socket_message;

    for (iii=0; iii < MAXIMUM_CUSTOMER_CONTROL_CLIENTS; iii++)
    {
        if (pClient[iii] && !pClient[iii]->delete_socket && pClient[iii]->validated_level &&
			!pClient[iii]->client_message_queue.empty())
        {
            socket_message = pClient[iii]->client_message_queue.front();
			pClient[iii]->client_message_queue.pop();
            socket_message.head.header = LONG_HEADER_IDENTIFIER;
			bytes_sent = pClient[iii]->Send(&socket_message, sizeof(socket_message.head) + socket_message.head.data_length);

			if (bytes_sent == SOCKET_ERROR)
			{
				last_error = GetLastError();

				if (last_error != WSAEINPROGRESS && last_error != WSAEWOULDBLOCK)
					pClient[iii]->Close();
			}
			else if (bytes_sent != sizeof(socket_message.head) + socket_message.head.data_length)
				pClient[iii]->Close();
        }
    }
}
