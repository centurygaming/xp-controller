/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    07/21/05    Initial Revision.
*/


#pragma once


class CCustomerControlListen;


class CCustomerControlClient : public CMyAsyncSocket
{
    DECLARE_DYNCREATE(CCustomerControlClient)

// public functions
public:
	CCustomerControlClient(CCustomerControlListen *pSocket = NULL);
	virtual ~CCustomerControlClient();
	virtual void OnClose(int nErrorCode);

// public variables
public:
    bool delete_socket;
    BYTE validated_level;
    DWORD client_address;
    DataDisplayStruct client_data_display;
	ReceiveIpBuffer rx_buffer;

    queue <SocketMessage, deque<SocketMessage> > client_message_queue;

// private variables
private:
    CCustomerControlListen *pServerSocket;
};


class CCustomerControlListen : public CAsyncSocket
{
    DECLARE_DYNCREATE(CCustomerControlListen)

public:
	CCustomerControlListen();
	virtual ~CCustomerControlListen();
	virtual void OnAccept(int nErrorCode);
	virtual void OnClose(int nErrorCode);
    void CheckCustomerControlMessageQueue();

// public variables
public:
    CCustomerControlClient *pClient[MAXIMUM_CUSTOMER_CONTROL_CLIENTS];
};
