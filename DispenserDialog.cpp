// DispenserDialog.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "DispenserDialog.h"
#include ".\dispenserdialog.h"


// CDispenserDialog dialog

IMPLEMENT_DYNAMIC(CDispenserDialog, CDialog)
CDispenserDialog::CDispenserDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CDispenserDialog::IDD, pParent)
{
	destroy_dialog = false;
}

CDispenserDialog::~CDispenserDialog()
{
}

void CDispenserDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_DISPENSER_RAW_DATA_CHECK, m_display_dispenser_raw_data);
	DDX_Control(pDX, IDC_DISPENSER_VERBOSE_DATA_CHECK, m_display_dispenser_verbose_data);
	DDX_Control(pDX, IDC_DISPENSER_DISPLAY_LIST, m_dispenser_display_listbox);
}


BEGIN_MESSAGE_MAP(CDispenserDialog, CDialog)
	ON_BN_CLICKED(IDC_DISPENSER_DIALOG_EXIT_BUTTON, OnBnClickedDispenserDialogExitButton)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_DISPENSER_RESET_BUTTON, OnBnClickedDispenserResetButton)
	ON_BN_CLICKED(IDC_DISPENSER_STATUS_BUTTON, OnBnClickedDispenserStatusButton)
END_MESSAGE_MAP()


// CDispenserDialog message handlers

void CDispenserDialog::OnBnClickedDispenserDialogExitButton()
{
	destroy_dialog = true;
}

void CDispenserDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CDispenserDialog::OnBnClickedDispenserResetButton()
{
	theApp.ResetDispenser();
}

void CDispenserDialog::OnBnClickedDispenserStatusButton()
{
 char display_buffer[500], *return_string;
 size_t iii;
 int buffer_index;

	m_dispenser_display_listbox.ResetContent();
	return_string = theApp.SendDispenserInfo(NULL);
	buffer_index = 0;
	memset(display_buffer, 0, sizeof(display_buffer));

	for (iii=0; iii < strlen(return_string); iii++)
	{
		if (return_string[iii] == '\r')
		{
			if (display_buffer[0])
				m_dispenser_display_listbox.AddString(display_buffer);

			buffer_index = 0;
			memset(display_buffer, 0, sizeof(display_buffer));
			iii++;
		}
		else
		{
			if (buffer_index < sizeof(display_buffer))
				display_buffer[buffer_index] = return_string[iii];

			buffer_index++;
		}
	}

	if (display_buffer[0])
		m_dispenser_display_listbox.AddString(display_buffer);
}

BOOL CDispenserDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_dispenser_display_listbox.SetHorizontalExtent(4000);
	m_display_dispenser_raw_data.SetCheck(BST_CHECKED);
	m_display_dispenser_verbose_data.SetCheck(BST_CHECKED);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
