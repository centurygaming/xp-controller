/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    06/09/05    Initial Revision.
*/


// DrawerDenomCount.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "DrawerDenomCount.h"
#include ".\drawerdenomcount.h"


// CDrawerDenomCount dialog

IMPLEMENT_DYNAMIC(CDrawerDenomCount, CDialog)
CDrawerDenomCount::CDrawerDenomCount(CWnd* pParent /*=NULL*/)
	: CDialog(CDrawerDenomCount::IDD, pParent)
{
    current_edit_focus = 0;
    total_amount = 0;
    hard_penny = 0;
    hard_nickel = 0;
    hard_dime = 0;
    hard_quarter = 0;
    hard_half = 0;
    hard_dollar = 0;
    hard_other = 0;
    soft_one = 0;
    soft_five = 0;
    soft_ten = 0;
    soft_twenty = 0;
    soft_fifty = 0;
    soft_hundred = 0;
    soft_other = 0;
}

CDrawerDenomCount::~CDrawerDenomCount()
{
}

void CDrawerDenomCount::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDrawerDenomCount, CDialog)
	ON_BN_CLICKED(IDC_DRAWER_DENOMINATION_BUTTON1, OnBnClickedDrawerDenominationButton1)
	ON_BN_CLICKED(IDC_DRAWER_DENOMINATION_BUTTON2, OnBnClickedDrawerDenominationButton2)
	ON_BN_CLICKED(IDC_DRAWER_DENOMINATION_BUTTON3, OnBnClickedDrawerDenominationButton3)
	ON_BN_CLICKED(IDC_DRAWER_DENOMINATION_BUTTON4, OnBnClickedDrawerDenominationButton4)
	ON_BN_CLICKED(IDC_DRAWER_DENOMINATION_BUTTON5, OnBnClickedDrawerDenominationButton5)
	ON_BN_CLICKED(IDC_DRAWER_DENOMINATION_BUTTON6, OnBnClickedDrawerDenominationButton6)
	ON_BN_CLICKED(IDC_DRAWER_DENOMINATION_BUTTON7, OnBnClickedDrawerDenominationButton7)
	ON_BN_CLICKED(IDC_DRAWER_DENOMINATION_BUTTON8, OnBnClickedDrawerDenominationButton8)
	ON_BN_CLICKED(IDC_DRAWER_DENOMINATION_BUTTON9, OnBnClickedDrawerDenominationButton9)
	ON_BN_CLICKED(IDC_DRAWER_DENOMINATION_BUTTON0, OnBnClickedDrawerDenominationButton0)
	ON_BN_CLICKED(IDC_DRAWER_DENOMINATION_ENTER_BUTTON, OnBnClickedDrawerDenominationEnterButton)
	ON_BN_CLICKED(IDC_DRAWER_DENOMINATION_CANCEL_BUTTON, OnBnClickedDrawerDenominationCancelButton)
	ON_BN_CLICKED(IDC_DRAWER_DENOMINATION_CLEAR_BUTTON, OnBnClickedDrawerDenominationClearButton)
	ON_EN_SETFOCUS(IDC_DRAWER_DENOMINATION_PENNY_EDIT, OnEnSetfocusDrawerDenominationPennyEdit)
	ON_EN_SETFOCUS(IDC_DRAWER_DENOMINATION_NICKEL_EDIT, OnEnSetfocusDrawerDenominationNickelEdit)
	ON_EN_SETFOCUS(IDC_DRAWER_DENOMINATION_DIME_EDIT, OnEnSetfocusDrawerDenominationDimeEdit)
	ON_EN_SETFOCUS(IDC_DRAWER_DENOMINATION_QUARTER_EDIT, OnEnSetfocusDrawerDenominationQuarterEdit)
	ON_EN_SETFOCUS(IDC_DRAWER_DENOMINATION_HALF_EDIT, OnEnSetfocusDrawerDenominationHalfEdit)
	ON_EN_SETFOCUS(IDC_DRAWER_DENOMINATION_DOLLAR_EDIT, OnEnSetfocusDrawerDenominationDollarEdit)
	ON_EN_SETFOCUS(IDC_DRAWER_DENOMINATION_HARD_OTHER_EDIT, OnEnSetfocusDrawerDenominationHardOtherEdit)
	ON_EN_SETFOCUS(IDC_DRAWER_DENOMINATION_ONE_EDIT, OnEnSetfocusDrawerDenominationOneEdit)
	ON_EN_SETFOCUS(IDC_DRAWER_DENOMINATION_FIVE_EDIT, OnEnSetfocusDrawerDenominationFiveEdit)
	ON_EN_SETFOCUS(IDC_DRAWER_DENOMINATION_TEN_EDIT, OnEnSetfocusDrawerDenominationTenEdit)
	ON_EN_SETFOCUS(IDC_DRAWER_DENOMINATION_TWENTY_EDIT, OnEnSetfocusDrawerDenominationTwentyEdit)
	ON_EN_SETFOCUS(IDC_DRAWER_DENOMINATION_FIFTY_EDIT, OnEnSetfocusDrawerDenominationFiftyEdit)
	ON_EN_SETFOCUS(IDC_DRAWER_DENOMINATION_HUNDRED_EDIT, OnEnSetfocusDrawerDenominationHundredEdit)
	ON_EN_SETFOCUS(IDC_DRAWER_DENOMINATION_SOFT_OTHER_EDIT, OnEnSetfocusDrawerDenominationSoftOtherEdit)
	ON_EN_SETFOCUS(IDC_DRAWER_TOTAL_AMOUNT_EDIT, OnEnSetfocusDrawerTotalAmountEdit)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


void CDrawerDenomCount::ProcessButtonClick(BYTE button_press)
{
 CString edit_string;

    if (current_edit_focus != IDC_DRAWER_TOTAL_AMOUNT_EDIT)
    {
        total_amount = 0;
        SetDlgItemText(IDC_DRAWER_TOTAL_AMOUNT_EDIT, "");
    }

    switch (current_edit_focus)
    {
        case IDC_DRAWER_DENOMINATION_PENNY_EDIT:
            hard_penny = hard_penny * 10 + button_press;
            edit_string.Format("$%9.2f", (double)hard_penny / 100);
            SetDlgItemText(IDC_DRAWER_DENOMINATION_PENNY_EDIT, edit_string);
            break;

        case IDC_DRAWER_DENOMINATION_NICKEL_EDIT:
            hard_nickel = hard_nickel * 10 + button_press;
            edit_string.Format("$%9.2f", (double)hard_nickel / 100);
            SetDlgItemText(IDC_DRAWER_DENOMINATION_NICKEL_EDIT, edit_string);
            break;

        case IDC_DRAWER_DENOMINATION_DIME_EDIT:
            hard_dime = hard_dime * 10 + button_press;
            edit_string.Format("$%9.2f", (double)hard_dime / 100);
            SetDlgItemText(IDC_DRAWER_DENOMINATION_DIME_EDIT, edit_string);
            break;

        case IDC_DRAWER_DENOMINATION_QUARTER_EDIT:
            hard_quarter = hard_quarter * 10 + button_press;
            edit_string.Format("$%9.2f", (double)hard_quarter / 100);
            SetDlgItemText(IDC_DRAWER_DENOMINATION_QUARTER_EDIT, edit_string);
            break;

        case IDC_DRAWER_DENOMINATION_HALF_EDIT:
            hard_half = hard_half * 10 + button_press;
            edit_string.Format("$%9.2f", (double)hard_half / 100);
            SetDlgItemText(IDC_DRAWER_DENOMINATION_HALF_EDIT, edit_string);
            break;

        case IDC_DRAWER_DENOMINATION_DOLLAR_EDIT:
            hard_dollar = hard_dollar * 10 + button_press;
            edit_string.Format("$%9.2f", (double)hard_dollar / 100);
            SetDlgItemText(IDC_DRAWER_DENOMINATION_DOLLAR_EDIT, edit_string);
            break;

        case IDC_DRAWER_DENOMINATION_HARD_OTHER_EDIT:
            hard_other = hard_other * 10 + button_press;
            edit_string.Format("$%9.2f", (double)hard_other / 100);
            SetDlgItemText(IDC_DRAWER_DENOMINATION_HARD_OTHER_EDIT, edit_string);
            break;

        case IDC_DRAWER_DENOMINATION_ONE_EDIT:
            soft_one = soft_one * 10 + button_press;
            edit_string.Format("$%9.2f", (double)soft_one / 100);
            SetDlgItemText(IDC_DRAWER_DENOMINATION_ONE_EDIT, edit_string);
            break;

        case IDC_DRAWER_DENOMINATION_FIVE_EDIT:
            soft_five = soft_five * 10 + button_press;
            edit_string.Format("$%9.2f", (double)soft_five / 100);
            SetDlgItemText(IDC_DRAWER_DENOMINATION_FIVE_EDIT, edit_string);
            break;

        case IDC_DRAWER_DENOMINATION_TEN_EDIT:
            soft_ten = soft_ten * 10 + button_press;
            edit_string.Format("$%9.2f", (double)soft_ten / 100);
            SetDlgItemText(IDC_DRAWER_DENOMINATION_TEN_EDIT, edit_string);
            break;

        case IDC_DRAWER_DENOMINATION_TWENTY_EDIT:
            soft_twenty = soft_twenty * 10 + button_press;
            edit_string.Format("$%9.2f", (double)soft_twenty / 100);
            SetDlgItemText(IDC_DRAWER_DENOMINATION_TWENTY_EDIT, edit_string);
            break;

        case IDC_DRAWER_DENOMINATION_FIFTY_EDIT:
            soft_fifty = soft_fifty * 10 + button_press;
            edit_string.Format("$%9.2f", (double)soft_fifty / 100);
            SetDlgItemText(IDC_DRAWER_DENOMINATION_TWENTY_EDIT, edit_string);
            break;

        case IDC_DRAWER_DENOMINATION_HUNDRED_EDIT:
            soft_hundred = soft_hundred * 10 + button_press;
            edit_string.Format("$%9.2f", (double)soft_hundred / 100);
            SetDlgItemText(IDC_DRAWER_DENOMINATION_HUNDRED_EDIT, edit_string);
            break;

        case IDC_DRAWER_DENOMINATION_SOFT_OTHER_EDIT:
            soft_other = soft_other * 10 + button_press;
            edit_string.Format("$%9.2f", (double)soft_other / 100);
            SetDlgItemText(IDC_DRAWER_DENOMINATION_SOFT_OTHER_EDIT, edit_string);
            break;

        case IDC_DRAWER_TOTAL_AMOUNT_EDIT:
            hard_penny = 0;
            hard_nickel = 0;
            hard_dime = 0;
            hard_quarter = 0;
            hard_half = 0;
            hard_dollar = 0;
            hard_other = 0;
            soft_one = 0;
            soft_five = 0;
            soft_ten = 0;
            soft_twenty = 0;
            soft_fifty = 0;
            soft_hundred = 0;
            soft_other = 0;
            total_amount = total_amount * 10 + button_press;
            edit_string.Format("$%9.2f", (double)total_amount / 100);
            SetDlgItemText(IDC_DRAWER_TOTAL_AMOUNT_EDIT, edit_string);
            SetDlgItemText(IDC_DRAWER_DENOMINATION_PENNY_EDIT, "");
            SetDlgItemText(IDC_DRAWER_DENOMINATION_NICKEL_EDIT, "");
            SetDlgItemText(IDC_DRAWER_DENOMINATION_DIME_EDIT, "");
            SetDlgItemText(IDC_DRAWER_DENOMINATION_QUARTER_EDIT, "");
            SetDlgItemText(IDC_DRAWER_DENOMINATION_HALF_EDIT, "");
            SetDlgItemText(IDC_DRAWER_DENOMINATION_DOLLAR_EDIT, "");
            SetDlgItemText(IDC_DRAWER_DENOMINATION_HARD_OTHER_EDIT, "");
            SetDlgItemText(IDC_DRAWER_DENOMINATION_ONE_EDIT, "");
            SetDlgItemText(IDC_DRAWER_DENOMINATION_FIVE_EDIT, "");
            SetDlgItemText(IDC_DRAWER_DENOMINATION_TEN_EDIT, "");
            SetDlgItemText(IDC_DRAWER_DENOMINATION_TWENTY_EDIT, "");
            SetDlgItemText(IDC_DRAWER_DENOMINATION_FIFTY_EDIT, "");
            SetDlgItemText(IDC_DRAWER_DENOMINATION_HUNDRED_EDIT, "");
            SetDlgItemText(IDC_DRAWER_DENOMINATION_SOFT_OTHER_EDIT, "");
            break;
    }
}

void CDrawerDenomCount::OnBnClickedDrawerDenominationButton1()
{
    ProcessButtonClick(1);
}

void CDrawerDenomCount::OnBnClickedDrawerDenominationButton2()
{
    ProcessButtonClick(2);
}

void CDrawerDenomCount::OnBnClickedDrawerDenominationButton3()
{
    ProcessButtonClick(3);
}

void CDrawerDenomCount::OnBnClickedDrawerDenominationButton4()
{
    ProcessButtonClick(4);
}

void CDrawerDenomCount::OnBnClickedDrawerDenominationButton5()
{
    ProcessButtonClick(5);
}

void CDrawerDenomCount::OnBnClickedDrawerDenominationButton6()
{
    ProcessButtonClick(6);
}

void CDrawerDenomCount::OnBnClickedDrawerDenominationButton7()
{
    ProcessButtonClick(7);
}

void CDrawerDenomCount::OnBnClickedDrawerDenominationButton8()
{
    ProcessButtonClick(8);
}

void CDrawerDenomCount::OnBnClickedDrawerDenominationButton9()
{
    ProcessButtonClick(9);
}

void CDrawerDenomCount::OnBnClickedDrawerDenominationButton0()
{
    ProcessButtonClick(0);
}

void CDrawerDenomCount::OnBnClickedDrawerDenominationEnterButton()
{
 DWORD sub_total;
 CString prt_buf;

    theApp.printer_busy = true;

    if (total_amount)
    {
        prt_buf.Format(" TOTAL CURRENCY\n Total Amount: %10.2f\n\n", total_amount);
        theApp.PrinterData(prt_buf.GetBuffer(0));
    }
    else
    {
        theApp.PrinterTitle("DENOM COUNT");
        theApp.PrinterId();
 		theApp.PrinterData("\n LOOSE COIN\n");
        prt_buf.Format("    PENNY    : $%9.2f\n", (double)hard_penny / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("    NICKEL   : $%9.2f\n", (double)hard_nickel / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("    DIME     : $%9.2f\n", (double)hard_dime / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("    QUARTER  : $%9.2f\n", (double)hard_quarter / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("    HALF     : $%9.2f\n", (double)hard_half / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("    DOLLAR   : $%9.2f\n", (double)hard_dollar / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("    OTHER    : $%9.2f\n", (double)hard_other / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));

        sub_total = hard_penny + hard_nickel + hard_dime + hard_quarter + hard_half + hard_dollar + hard_other;
        total_amount = sub_total;

        prt_buf.Format("\n SUBTOTAL  %9.2f *\n\n", (double)sub_total / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));

        theApp.PrinterData(" CURRENCY\n");
        prt_buf.Format("    ONE      : $%9.2f\n", (double)soft_one / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("    FIVE     : $%9.2f\n", (double)soft_five / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("    TEN      : $%9.2f\n", (double)soft_ten / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("    TWENTY   : $%9.2f\n", (double)soft_twenty / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("    FIFTY    : $%9.2f\n", (double)soft_fifty / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("    HUNDRED  : $%9.2f\n", (double)soft_hundred / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format("    OTHER    : $%9.2f\n", (double)soft_other / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));

        sub_total = soft_one + soft_five + soft_ten + soft_twenty + soft_fifty + soft_hundred + soft_other;
        total_amount += sub_total;

        prt_buf.Format("\n SUBTOTAL  %9.2f *\n\n", (double)sub_total / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        prt_buf.Format(" TOTAL          %9.2f **\n\n", (double)total_amount / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));
    }

    theApp.message_window_queue.push(total_amount);
    DestroyWindow();
}

void CDrawerDenomCount::OnBnClickedDrawerDenominationCancelButton()
{
    theApp.message_window_queue.push(-1);
    DestroyWindow();
}

void CDrawerDenomCount::OnBnClickedDrawerDenominationClearButton()
{
    switch (current_edit_focus)
    {
        case IDC_DRAWER_DENOMINATION_PENNY_EDIT:
            hard_penny = 0;
            SetDlgItemText(IDC_DRAWER_DENOMINATION_PENNY_EDIT, "");
            break;

        case IDC_DRAWER_DENOMINATION_NICKEL_EDIT:
            hard_nickel = 0;
            SetDlgItemText(IDC_DRAWER_DENOMINATION_NICKEL_EDIT, "");
            break;

        case IDC_DRAWER_DENOMINATION_DIME_EDIT:
            hard_dime = 0;
            SetDlgItemText(IDC_DRAWER_DENOMINATION_DIME_EDIT, "");
            break;

        case IDC_DRAWER_DENOMINATION_QUARTER_EDIT:
            hard_quarter = 0;
            SetDlgItemText(IDC_DRAWER_DENOMINATION_QUARTER_EDIT, "");
            break;

        case IDC_DRAWER_DENOMINATION_HALF_EDIT:
            hard_half = 0;
            SetDlgItemText(IDC_DRAWER_DENOMINATION_HALF_EDIT, "");
            break;

        case IDC_DRAWER_DENOMINATION_DOLLAR_EDIT:
            hard_dollar = 0;
            SetDlgItemText(IDC_DRAWER_DENOMINATION_DOLLAR_EDIT, "");
            break;

        case IDC_DRAWER_DENOMINATION_HARD_OTHER_EDIT:
            hard_other = 0;
            SetDlgItemText(IDC_DRAWER_DENOMINATION_HARD_OTHER_EDIT, "");
            break;

        case IDC_DRAWER_DENOMINATION_ONE_EDIT:
            soft_one = 0;
            SetDlgItemText(IDC_DRAWER_DENOMINATION_ONE_EDIT, "");
            break;

        case IDC_DRAWER_DENOMINATION_FIVE_EDIT:
            soft_five = 0;
            SetDlgItemText(IDC_DRAWER_DENOMINATION_FIVE_EDIT, "");
            break;

        case IDC_DRAWER_DENOMINATION_TEN_EDIT:
            soft_ten = 0;
            SetDlgItemText(IDC_DRAWER_DENOMINATION_TEN_EDIT, "");
            break;

        case IDC_DRAWER_DENOMINATION_TWENTY_EDIT:
            soft_twenty = 0;
            SetDlgItemText(IDC_DRAWER_DENOMINATION_TWENTY_EDIT, "");
            break;

        case IDC_DRAWER_DENOMINATION_FIFTY_EDIT:
            soft_fifty = 0;
            SetDlgItemText(IDC_DRAWER_DENOMINATION_TWENTY_EDIT, "");
            break;

        case IDC_DRAWER_DENOMINATION_HUNDRED_EDIT:
            soft_hundred = 0;
            SetDlgItemText(IDC_DRAWER_DENOMINATION_HUNDRED_EDIT, "");
            break;

        case IDC_DRAWER_DENOMINATION_SOFT_OTHER_EDIT:
            soft_other = 0;
            SetDlgItemText(IDC_DRAWER_DENOMINATION_SOFT_OTHER_EDIT, "");
            break;

        case IDC_DRAWER_TOTAL_AMOUNT_EDIT:
            total_amount = 0;
            SetDlgItemText(IDC_DRAWER_TOTAL_AMOUNT_EDIT, "");
            break;
    }
}

void CDrawerDenomCount::OnEnSetfocusDrawerDenominationPennyEdit()
{
    current_edit_focus = IDC_DRAWER_DENOMINATION_PENNY_EDIT;
}

void CDrawerDenomCount::OnEnSetfocusDrawerDenominationNickelEdit()
{
    current_edit_focus = IDC_DRAWER_DENOMINATION_NICKEL_EDIT;
}

void CDrawerDenomCount::OnEnSetfocusDrawerDenominationDimeEdit()
{
    current_edit_focus = IDC_DRAWER_DENOMINATION_DIME_EDIT;
}

void CDrawerDenomCount::OnEnSetfocusDrawerDenominationQuarterEdit()
{
    current_edit_focus = IDC_DRAWER_DENOMINATION_QUARTER_EDIT;
}

void CDrawerDenomCount::OnEnSetfocusDrawerDenominationHalfEdit()
{
    current_edit_focus = IDC_DRAWER_DENOMINATION_HALF_EDIT;
}

void CDrawerDenomCount::OnEnSetfocusDrawerDenominationDollarEdit()
{
    current_edit_focus = IDC_DRAWER_DENOMINATION_DOLLAR_EDIT;
}

void CDrawerDenomCount::OnEnSetfocusDrawerDenominationHardOtherEdit()
{
    current_edit_focus = IDC_DRAWER_DENOMINATION_HARD_OTHER_EDIT;
}

void CDrawerDenomCount::OnEnSetfocusDrawerDenominationOneEdit()
{
    current_edit_focus = IDC_DRAWER_DENOMINATION_ONE_EDIT;
}

void CDrawerDenomCount::OnEnSetfocusDrawerDenominationFiveEdit()
{
    current_edit_focus = IDC_DRAWER_DENOMINATION_FIVE_EDIT;
}

void CDrawerDenomCount::OnEnSetfocusDrawerDenominationTenEdit()
{
    current_edit_focus = IDC_DRAWER_DENOMINATION_TEN_EDIT;
}

void CDrawerDenomCount::OnEnSetfocusDrawerDenominationTwentyEdit()
{
    current_edit_focus = IDC_DRAWER_DENOMINATION_TWENTY_EDIT;
}

void CDrawerDenomCount::OnEnSetfocusDrawerDenominationFiftyEdit()
{
    current_edit_focus = IDC_DRAWER_DENOMINATION_FIFTY_EDIT;
}

void CDrawerDenomCount::OnEnSetfocusDrawerDenominationHundredEdit()
{
    current_edit_focus = IDC_DRAWER_DENOMINATION_HUNDRED_EDIT;
}

void CDrawerDenomCount::OnEnSetfocusDrawerDenominationSoftOtherEdit()
{
    current_edit_focus = IDC_DRAWER_DENOMINATION_SOFT_OTHER_EDIT;
}

void CDrawerDenomCount::OnEnSetfocusDrawerTotalAmountEdit()
{
    current_edit_focus = IDC_DRAWER_TOTAL_AMOUNT_EDIT;
}

void CDrawerDenomCount::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}
