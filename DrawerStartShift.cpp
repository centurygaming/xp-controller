/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    06/14/05    Initial Revision.
*/


// DrawerStartShift.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "DrawerStartShift.h"
#include ".\drawerstartshift.h"


// CDrawerStartShift dialog

IMPLEMENT_DYNAMIC(CDrawerStartShift, CDialog)
CDrawerStartShift::CDrawerStartShift(int *drawer_balance, CWnd* pParent /*=NULL*/)
	: CDialog(CDrawerStartShift::IDD, pParent)
{
    beginning_balance = drawer_balance;
    entered_balance = 0;
}

CDrawerStartShift::~CDrawerStartShift()
{
}

void CDrawerStartShift::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDrawerStartShift, CDialog)
	ON_BN_CLICKED(IDC_CASHIER_DRAWER_BALANCE_BUTTON1, OnBnClickedCashierDrawerBalanceButton1)
	ON_BN_CLICKED(IDC_CASHIER_DRAWER_BALANCE_BUTTON2, OnBnClickedCashierDrawerBalanceButton2)
	ON_BN_CLICKED(IDC_CASHIER_DRAWER_BALANCE_BUTTON3, OnBnClickedCashierDrawerBalanceButton3)
	ON_BN_CLICKED(IDC_CASHIER_DRAWER_BALANCE_BUTTON4, OnBnClickedCashierDrawerBalanceButton4)
	ON_BN_CLICKED(IDC_CASHIER_DRAWER_BALANCE_BUTTON5, OnBnClickedCashierDrawerBalanceButton5)
	ON_BN_CLICKED(IDC_CASHIER_DRAWER_BALANCE_BUTTON6, OnBnClickedCashierDrawerBalanceButton6)
	ON_BN_CLICKED(IDC_CASHIER_DRAWER_BALANCE_BUTTON7, OnBnClickedCashierDrawerBalanceButton7)
	ON_BN_CLICKED(IDC_CASHIER_DRAWER_BALANCE_BUTTON8, OnBnClickedCashierDrawerBalanceButton8)
	ON_BN_CLICKED(IDC_CASHIER_DRAWER_BALANCE_BUTTON9, OnBnClickedCashierDrawerBalanceButton9)
	ON_BN_CLICKED(IDC_CASHIER_DRAWER_BALANCE_BUTTON0, OnBnClickedCashierDrawerBalanceButton0)
	ON_BN_CLICKED(IDC_CASHIER_DRAWER_BALANCE_CLEAR_BUTTON, OnBnClickedCashierDrawerBalanceClearButton)
	ON_BN_CLICKED(IDC_CASHIER_DRAWER_BALANCE_ENTER_BUTTON, OnBnClickedCashierDrawerBalanceEnterButton)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


void CDrawerStartShift::ProcessButtonClick(BYTE button_press)
{
 CString edit_string;

    entered_balance = entered_balance * 10 + button_press;
    edit_string.Format("$%9.2f", (double)entered_balance / 100);
    SetDlgItemText(IDC_CASHIER_DRAWER_BALANCE_EDIT, edit_string);
}

void CDrawerStartShift::OnBnClickedCashierDrawerBalanceButton1()
{
    ProcessButtonClick(1);
}

void CDrawerStartShift::OnBnClickedCashierDrawerBalanceButton2()
{
    ProcessButtonClick(2);
}

void CDrawerStartShift::OnBnClickedCashierDrawerBalanceButton3()
{
    ProcessButtonClick(3);
}

void CDrawerStartShift::OnBnClickedCashierDrawerBalanceButton4()
{
    ProcessButtonClick(4);
}

void CDrawerStartShift::OnBnClickedCashierDrawerBalanceButton5()
{
    ProcessButtonClick(5);
}

void CDrawerStartShift::OnBnClickedCashierDrawerBalanceButton6()
{
    ProcessButtonClick(6);
}

void CDrawerStartShift::OnBnClickedCashierDrawerBalanceButton7()
{
    ProcessButtonClick(7);
}

void CDrawerStartShift::OnBnClickedCashierDrawerBalanceButton8()
{
    ProcessButtonClick(8);
}

void CDrawerStartShift::OnBnClickedCashierDrawerBalanceButton9()
{
    ProcessButtonClick(9);
}

void CDrawerStartShift::OnBnClickedCashierDrawerBalanceButton0()
{
    ProcessButtonClick(0);
}

void CDrawerStartShift::OnBnClickedCashierDrawerBalanceClearButton()
{
    entered_balance = 0;
    SetDlgItemText(IDC_CASHIER_DRAWER_BALANCE_EDIT, "");
}

void CDrawerStartShift::OnBnClickedCashierDrawerBalanceEnterButton()
{
 CString edit_string;

    GetDlgItemText(IDC_CASHIER_DRAWER_BALANCE_EDIT, edit_string);

    if (edit_string.GetLength() && entered_balance >= 0)
        *beginning_balance = entered_balance;
}

void CDrawerStartShift::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}
