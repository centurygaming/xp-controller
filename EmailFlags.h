/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    07/15/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"


// CEmailFlags dialog

class CEmailFlags : public CDialog
{
	DECLARE_DYNAMIC(CEmailFlags)

public:
	CEmailFlags(CString email_address, CWnd* pParent = NULL);   // standard constructor
	virtual ~CEmailFlags();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_TECHNICIAN_EMAIL_FLAGS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedEmailFlagsExitButton();
	afx_msg void OnBnClickedEmailFlagsUpdateButton();

// private variables
private:
    CString edit_email_address;

	CButton m_stratus_link_up;
	CButton m_stratus_link_down;
	CButton m_port_comm_problem;
	CButton m_port_no_response;
	CButton m_progressive_link_down;
	CButton m_million_dollar_ticket_draw;
	CButton m_player_card_stuck_sensor;
	CButton m_player_card_dead_reader;
	CButton m_player_card_abandoned;
};
