#if !defined(AFX_FIXEDBUTTON_H__21075390_DBE0_11D4_B599_00A024A73782__INCLUDED_)
#define AFX_FIXEDBUTTON_H__21075390_DBE0_11D4_B599_00A024A73782__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif

class CFixedButton : public CBitmapButton
{
// Construction
public:
	CFixedButton( void );

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFixedButton)
	protected:
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFixedButton( void );

	// Generated message map functions
protected:
	//{{AFX_MSG(CFixedButton)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	BOOL m_fSent;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif
