/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   03/08/13    Removed obsolete Location Collection code.
Robert Fuller   09/17/12    Make sure we are ignoring GB Interface devices when traversing through the game device linked list.
Robert Fuller   12/30/11    Make sure we are fetching SAS game pointers for a particular UCMC id when forcing, clearing, and displaying games.
Robert Fuller   08/03/11    Added code to support Location Collection features.
Robert Fuller   11/06/08    Added mechanism to start, force, and cancel all the games simultaneously.
*/

#pragma once
#include "afxwin.h"


// CGameCollectionState dialog

class CGameCollectionState : public CDialog
{
	DECLARE_DYNAMIC(CGameCollectionState)

public:
	CGameCollectionState(CWnd* pParent = NULL);   // standard constructor
	CGameCollectionState(bool manager_entry, CWnd* pParent = NULL);   // standard constructor
	virtual ~CGameCollectionState();

// Dialog Data
	enum { IDD = IDD_FORCE_COLLECTION_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

// public functions
public:
	afx_msg void OnBnClickedForceCollectionExitButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	virtual BOOL OnInitDialog();

	void ListGameInformation(CGameDevice *pDisplayGameDevice);

// public variables
public:
	bool destroy_dialog;
	afx_msg void OnBnClickedForceCollectionPreviousGameButton();
	afx_msg void OnBnClickedForceCollectionNextGameButton();
	afx_msg void OnBnClickedForceCollectionRefreshButton();
	afx_msg void OnBnClickedForceCollectionStartButton();
	afx_msg void OnBnClickedForceCollectionForceButton();
	afx_msg void OnBnClickedForceCollectionCancelButton();
	afx_msg void OnBnClickedStartAllButton();
	afx_msg void OnBnClickedForceAllButton();
	afx_msg void OnBnClickedCancelAllButton();

// private variables
private:
	CListBox m_game_listbox;
	CButton m_start_collection_button;
	CButton m_force_collection_button;
	CButton m_cancel_collection_button;
	CGameDevice *pDisplayGameDevice;
	bool list_game_info_initialized;

	CGameDevice* getNextNonGbInterfaceGameDevice(CGameDevice *pGameDevice);
};
