// GbProAsciiMsg.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "GbProAsciiMsg.h"


// CGbProAsciiMsg dialog

IMPLEMENT_DYNAMIC(CGbProAsciiMsg, CDialog)

CGbProAsciiMsg::CGbProAsciiMsg(CWnd* pParent /*=NULL*/)
	: CDialog(CGbProAsciiMsg::IDD, pParent)
{

}

CGbProAsciiMsg::~CGbProAsciiMsg()
{
}

void CGbProAsciiMsg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CGbProAsciiMsg, CDialog)
	ON_BN_CLICKED(IDC_GB_PRO_ASCII_EXIT_BUTTON, &CGbProAsciiMsg::OnBnClickedGbProAsciiExitButton)
	ON_BN_CLICKED(IDC_GB_PRO_ASCII_KEYBOARD_BUTTON, &CGbProAsciiMsg::OnBnClickedGbProAsciiKeyboardButton)
	ON_BN_CLICKED(IDC_GB_PRO_ASCII_SAVE_BUTTON, &CGbProAsciiMsg::OnBnClickedGbProAsciiSaveButton)
	ON_BN_CLICKED(IDC_GB_PRO_ASCII_TEST_POINTS_BUTTON, &CGbProAsciiMsg::OnBnClickedGbProAsciiTestPointsButton)
	ON_BN_CLICKED(IDC_GB_PRO_ASCII_TEST_CASH_BUTTON, &CGbProAsciiMsg::OnBnClickedGbProAsciiTestCashButton)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CGbProAsciiMsg message handlers

void CGbProAsciiMsg::OnBnClickedGbProAsciiExitButton()
{
	DestroyWindow();
}

void CGbProAsciiMsg::OnBnClickedGbProAsciiKeyboardButton()
{
	ShellExecute(NULL, "open", "osk.exe", NULL, NULL, SW_SHOWMAXIMIZED);
}

BOOL CGbProAsciiMsg::OnInitDialog()
{
 CString edit_string;
 GbProAsciiMachineMessagesFile file_ascii_messages;

	CDialog::OnInitDialog();

	if (GetFileAttributes(GB_PRO_ASCII_MESSAGES_FILE) == 0xFFFFFFFF ||
		!fileRead(GB_PRO_ASCII_MESSAGES_FILE, 0, &file_ascii_messages, sizeof(file_ascii_messages)))
	{
		memset(&file_ascii_messages, 0, sizeof(file_ascii_messages));
		sprintf_s(file_ascii_messages.points_text_line_one, sizeof(file_ascii_messages.points_text_line_one), GB_PRO_DISPLAY_LINE_ONE_DEFAULT);
		sprintf_s(file_ascii_messages.points_text_line_two, sizeof(file_ascii_messages.points_text_line_two), GB_PRO_DISPLAY_LINE_TWO_POINTS_DEFAULT);
		sprintf_s(file_ascii_messages.cash_text_line_one, sizeof(file_ascii_messages.cash_text_line_one), GB_PRO_DISPLAY_LINE_ONE_DEFAULT);
		sprintf_s(file_ascii_messages.cash_text_line_two, sizeof(file_ascii_messages.cash_text_line_two), GB_PRO_DISPLAY_LINE_TWO_CASH_DEFAULT);
	}

	edit_string = file_ascii_messages.points_text_line_one;
	SetDlgItemText(IDC_GB_PRO_ASCII_POINTS_LINE_ONE_EDIT, edit_string);
	edit_string = file_ascii_messages.points_text_line_two;
	SetDlgItemText(IDC_GB_PRO_ASCII_POINTS_LINE_TWO_EDIT, edit_string);

	edit_string = file_ascii_messages.cash_text_line_one;
	SetDlgItemText(IDC_GB_PRO_ASCII_CASH_LINE_ONE_EDIT, edit_string);
	edit_string = file_ascii_messages.cash_text_line_two;
	SetDlgItemText(IDC_GB_PRO_ASCII_CASH_LINE_TWO_EDIT, edit_string);

 return TRUE;
}

void CGbProAsciiMsg::OnBnClickedGbProAsciiSaveButton()
{
 #define STRING_LENGTH 30
 char points_line_one_string[STRING_LENGTH], points_line_two_string[STRING_LENGTH];
 char cash_line_one_string[STRING_LENGTH], cash_line_two_string[STRING_LENGTH];
 CString edit_string;
 GbProAsciiMachineMessagesFile file_ascii_messages;

	memset(points_line_one_string, 0, STRING_LENGTH);
	memset(points_line_two_string, 0, STRING_LENGTH);
	memset(cash_line_one_string, 0, STRING_LENGTH);
	memset(cash_line_two_string, 0, STRING_LENGTH);

	GetDlgItemText(IDC_GB_PRO_ASCII_POINTS_LINE_ONE_EDIT, edit_string);
	if (!edit_string.GetLength())
		sprintf_s(points_line_one_string, STRING_LENGTH, "'LINE NOT SET'");
	else if (edit_string.GetLength() < STRING_LENGTH)
		memcpy(points_line_one_string, edit_string.GetBuffer(0), edit_string.GetLength());
	else
		memcpy(points_line_one_string, edit_string.GetBuffer(0), STRING_LENGTH - 1);

	GetDlgItemText(IDC_GB_PRO_ASCII_POINTS_LINE_TWO_EDIT, edit_string);
	if (!edit_string.GetLength())
		sprintf_s(points_line_two_string, STRING_LENGTH, "'LINE NOT SET'");
	else if (edit_string.GetLength() < STRING_LENGTH)
		memcpy(points_line_two_string, edit_string.GetBuffer(0), edit_string.GetLength());
	else
		memcpy(points_line_two_string, edit_string.GetBuffer(0), STRING_LENGTH - 1);

	GetDlgItemText(IDC_GB_PRO_ASCII_CASH_LINE_ONE_EDIT, edit_string);
	if (!edit_string.GetLength())
		sprintf_s(cash_line_one_string, STRING_LENGTH, "'LINE NOT SET'");
	else if (edit_string.GetLength() < STRING_LENGTH)
		memcpy(cash_line_one_string, edit_string.GetBuffer(0), edit_string.GetLength());
	else
		memcpy(cash_line_one_string, edit_string.GetBuffer(0), STRING_LENGTH - 1);

	GetDlgItemText(IDC_GB_PRO_ASCII_CASH_LINE_TWO_EDIT, edit_string);
	if (!edit_string.GetLength())
		sprintf_s(cash_line_two_string, STRING_LENGTH, "'LINE NOT SET'");
	else if (edit_string.GetLength() < STRING_LENGTH)
		memcpy(cash_line_two_string, edit_string.GetBuffer(0), edit_string.GetLength());
	else
		memcpy(cash_line_two_string, edit_string.GetBuffer(0), STRING_LENGTH - 1);

	edit_string.Format("Points Message Line 1:\n     %s\nPoints Message Line 2:\n     %s\n"
		"Cash Message Line 1:\n     %s\nCash Message Line 2:\n     %s", points_line_one_string,
		points_line_two_string, cash_line_one_string, cash_line_two_string);

	EnableWindow(FALSE);
	MessageWindow(true, "SAVE DATA?", edit_string);
	while (theApp.message_window_queue.empty())
		theApp.OnIdle(0);
	EnableWindow();

	if (theApp.message_window_queue.front() == IDOK)
	{
		memset(&file_ascii_messages, 0, sizeof(file_ascii_messages));
		memcpy(file_ascii_messages.points_text_line_one, points_line_one_string, sizeof(file_ascii_messages.points_text_line_one));
		memcpy(file_ascii_messages.points_text_line_two, points_line_two_string, sizeof(file_ascii_messages.points_text_line_two));
		memcpy(file_ascii_messages.cash_text_line_one, cash_line_one_string, sizeof(file_ascii_messages.cash_text_line_one));
		memcpy(file_ascii_messages.cash_text_line_two, cash_line_two_string, sizeof(file_ascii_messages.cash_text_line_two));

		fileWrite(GB_PRO_ASCII_MESSAGES_FILE, 0, &file_ascii_messages, sizeof(file_ascii_messages));

		SetDlgItemText(IDC_GB_PRO_ASCII_POINTS_LINE_ONE_EDIT, points_line_one_string);
		SetDlgItemText(IDC_GB_PRO_ASCII_POINTS_LINE_TWO_EDIT, points_line_two_string);
		SetDlgItemText(IDC_GB_PRO_ASCII_CASH_LINE_ONE_EDIT, cash_line_one_string);
		SetDlgItemText(IDC_GB_PRO_ASCII_CASH_LINE_TWO_EDIT, cash_line_two_string);
	}
}

void CGbProAsciiMsg::OnBnClickedGbProAsciiTestPointsButton()
{
 int point_value;
 CString edit_string;
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl;

	if (theApp.pGbProManager)
	{
		GetDlgItemText(IDC_GB_PRO_ASCII_TEST_POINTS_EDIT, edit_string);

		if (edit_string.GetLength())
			point_value = atoi(edit_string);
		else
			point_value = 0;

		if (point_value > 0)
		{
			pGameCommControl = theApp.pHeadGameCommControl;

			while (pGameCommControl)
			{
				pGameDevice = pGameCommControl->pHeadGameDevice;

				while (pGameDevice)
				{
					pGameCommControl->SendMachineAsciiMessage(pGameDevice, (DWORD)point_value, 0, 0);
					pGameDevice = pGameDevice->pNextGame;
				}

				pGameCommControl = pGameCommControl->pNextGameCommControl;
			}
		}
	}
}

void CGbProAsciiMsg::OnBnClickedGbProAsciiTestCashButton()
{
 int cash_amount;
 CString edit_string;
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl;

	if (theApp.pGbProManager)
	{
		GetDlgItemText(IDC_GB_PRO_ASCII_TEST_CASH_EDIT, edit_string);

		if (edit_string.GetLength())
			cash_amount = atoi(edit_string);
		else
			cash_amount = 0;

		if (cash_amount > 0)
		{
			pGameCommControl = theApp.pHeadGameCommControl;

			while (pGameCommControl)
			{
				pGameDevice = pGameCommControl->pHeadGameDevice;

				while (pGameDevice)
				{
					pGameCommControl->SendMachineAsciiMessage(pGameDevice, 0, (DWORD)cash_amount, 0);
					pGameDevice = pGameDevice->pNextGame;
				}

				pGameCommControl = pGameCommControl->pNextGameCommControl;
			}
		}
	}
}

void CGbProAsciiMsg::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default

	CDialog::OnMouseMove(nFlags, point);
}
