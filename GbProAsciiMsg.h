#pragma once


// CGbProAsciiMsg dialog

class CGbProAsciiMsg : public CDialog
{
	DECLARE_DYNAMIC(CGbProAsciiMsg)

public:
	CGbProAsciiMsg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CGbProAsciiMsg();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_GB_PRO_MACHINE_ASCII_MSGS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedGbProAsciiExitButton();
	afx_msg void OnBnClickedGbProAsciiKeyboardButton();
	afx_msg void OnBnClickedGbProAsciiSaveButton();
	afx_msg void OnBnClickedGbProAsciiTestPointsButton();
	afx_msg void OnBnClickedGbProAsciiTestCashButton();
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};
