/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	07/03/13	Save important Stratus TX messages to a file on disk and do not purge unless Stratus returns an ack.
Dave Elquist    06/24/05    Initial Revision.
*/


// GbProManagerDialog.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include ".\gbpromanagerdialog.h"


// CGbProManagerDialog dialog

IMPLEMENT_DYNAMIC(CGbProManagerDialog, CDialog)
CGbProManagerDialog::CGbProManagerDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CGbProManagerDialog::IDD, pParent)
{
    player_account = 0;
}

CGbProManagerDialog::~CGbProManagerDialog()
{
}

void CGbProManagerDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, m_player_points);
	DDX_Control(pDX, IDC_GB_PRO_GENERATE_MILLION_DOLLAR_TICKET_BUTTON, m_million_dollar_ticket_button);
}


BEGIN_MESSAGE_MAP(CGbProManagerDialog, CDialog)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_EXIT_BUTTON, OnBnClickedManagerGbProExitButton)
	ON_BN_CLICKED(IDC_GB_PRO_GENERATE_MILLION_DOLLAR_TICKET_BUTTON, OnBnClickedGbProGenerateMillionDollarTicketButton)
	ON_BN_CLICKED(IDC_GB_PRO_PLAYER_POINTS_BUTTON, OnBnClickedGbProPlayerPointsButton)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON1, OnBnClickedManagerGbProAccountButton1)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON2, OnBnClickedManagerGbProAccountButton2)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON3, OnBnClickedManagerGbProAccountButton3)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON4, OnBnClickedManagerGbProAccountButton4)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON5, OnBnClickedManagerGbProAccountButton5)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON6, OnBnClickedManagerGbProAccountButton6)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON7, OnBnClickedManagerGbProAccountButton7)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON8, OnBnClickedManagerGbProAccountButton8)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON9, OnBnClickedManagerGbProAccountButton9)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_ACCOUNT_BUTTON0, OnBnClickedManagerGbProAccountButton0)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_ACCOUNT_CLEAR_BUTTON, OnBnClickedManagerGbProAccountClearButton)
END_MESSAGE_MAP()


// CGbProManagerDialog message handlers

void CGbProManagerDialog::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

BOOL CGbProManagerDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

    if (!theApp.pGbProManager || !theApp.pGbProManager->memory_gb_pro_settings.million_dollar_ticket_server_ip_address)
        m_million_dollar_ticket_button.DestroyWindow();

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CGbProManagerDialog::OnBnClickedManagerGbProExitButton()
{
    DestroyWindow();
}

void CGbProManagerDialog::OnBnClickedGbProGenerateMillionDollarTicketButton()
{
}

void CGbProManagerDialog::OnBnClickedGbProPlayerPointsButton()
{
#if 0
??????????????
 int string_value, current_day_of_year;
 DWORD points_to_award;
 time_t convert_current_time;
 struct tm *time_struct = NULL;
 CString prt_buf;
 FileGbProManagerPoints file_manager_points;
 FileGbProManagerSettings file_manager_settings;
 StratusMessageFormat stratus_msg;

    if (!player_account)
        MessageWindow(false, "GB PRO POINTS", "ENTER PLAYER ACCOUNT");
    else
    {
        string_value = m_player_points.GetCurSel();

        switch (string_value)
        {
            case 0:
                points_to_award = 1000;
                break;

            case 1:
                points_to_award = 5000;
                break;

            case 2:
                points_to_award = 10000;
                break;

            default:
                MessageWindow(false, "GB PRO POINTS", "SELECT NUMBER OF POINTS\nTO GIVE TO PLAYER.");
                return;
        }

        if (!fileRead(GB_PRO_SETTINGS_INI, 0, &file_manager_settings, sizeof(file_manager_settings)))
        {
            prt_buf.Format("UNABLE TO READ FILE:\n%s", GB_PRO_SETTINGS_INI);
            MessageWindow(false, "GB PRO POINTS", prt_buf);
        }
        else
        {
            // check time
            convert_current_time = time(NULL);
            gmtime_s(time_struct, &convert_current_time);
            if (!time_struct)
                MessageWindow(false, "GB PRO POINTS", "ERROR CONVERTING CURRENT TIME STAMP\nPLEASE TRY AGAIN");
            else
            {
                current_day_of_year = time_struct->tm_yday;
				time_struct = NULL;
                gmtime_s(time_struct, &file_manager_settings.memory.last_time_points_awarded_reset);
                if (!time_struct)
                    MessageWindow(false, "GB PRO POINTS", "ERROR CONVERTING FILE TIME STAMP\nPLEASE TRY AGAIN");
                else
                {
                    if (time_struct->tm_yday != current_day_of_year)
                    {
                        file_manager_settings.memory.points_awarded = 0;
                        file_manager_settings.memory.last_time_points_awarded_reset = convert_current_time;
                    }

                    file_manager_settings.memory.points_awarded += points_to_award;
                    if (file_manager_settings.memory.points_awarded > file_manager_settings.memory.maximum_points_award_per_day)
                        MessageWindow(false, "GB PRO POINTS", "CANNOT GIVE GB POINTS\nDAILY MAXIMUM EXCEEDED");
                    else
                    {
                        if (theApp.stratusSocket.last_time_check)
                           MessageWindow(false, "GB PRO POINTS", "CANNOT GIVE GB POINTS\nSYSTEM CONNECTION DOWN");
                        else
                        {
                            if (!fileWrite(GB_PRO_SETTINGS_INI, 0, &file_manager_settings, sizeof(file_manager_settings)))
                               MessageWindow(false, "GB PRO POINTS", "CANNOT GIVE GB POINTS\nERROR WRITING FILE");
                            else
                            {
                                theApp.printer_busy = true;
                                theApp.PrinterHeader(0);
                                prt_buf.Format("PLAYER ACCOUNT\n    %u\nMANAGER ID\n    %u\nGB POINTS\n    %u\n\n",
                                    player_account, theApp.current_user.user.id, points_to_award);
                                theApp.PrinterData(prt_buf.GetBuffer(0));
                                theApp.printer_busy = false;

                                file_manager_points.memory.player_account = player_account;
                                file_manager_points.memory.gb_points = points_to_award;
                                file_manager_points.memory.system_user_id = theApp.current_user.user.id;
                                file_manager_points.memory.time_stamp = time(NULL);
                                fileWrite(GB_PRO_MANAGER_POINTS_FILE, -1, &file_manager_points, sizeof(file_manager_points));

                                endian4(file_manager_points.memory.player_account);
                                endian4(file_manager_points.memory.gb_points);

                                memset(&stratus_msg, 0, sizeof(stratus_msg));
                                stratus_msg.task_code = MPI_TASK_PLAYER_ACCOUNTING;
                                stratus_msg.send_code = MPI_GB_PRO_POINTS;
                                stratus_msg.property_id = theApp.memory_location_config.property_id;
                                stratus_msg.club_code = theApp.memory_location_config.club_code;
                                stratus_msg.customer_id = theApp.memory_location_config.customer_id;
                                stratus_msg.message_length = 10 + STRATUS_HEADER_SIZE;  // +2 bytes for fractional points
                                memcpy(stratus_msg.data, &file_manager_points.memory.player_account, 4);
                                memcpy(&stratus_msg.data[4], &file_manager_points.memory.gb_points, 4);
                                theApp.QueueStratusTxMessage(&stratus_msg, FALSE);

                                if (theApp.current_user.user.id / 10000 == MANAGER)
                                    DestroyWindow();
                            }
                        }
                    }
                }
            }
        }
    }
#endif
}

void CGbProManagerDialog::ProcessButtonClick(BYTE button_press)
{
 CString edit_string;

    player_account = player_account * 10 + button_press;
    edit_string.Format("%u", player_account);
    SetDlgItemText(IDC_GB_PRO_PLAYER_ACCOUNT_EDIT, edit_string);
}

void CGbProManagerDialog::OnBnClickedManagerGbProAccountButton1()
{
    ProcessButtonClick(1);
}

void CGbProManagerDialog::OnBnClickedManagerGbProAccountButton2()
{
    ProcessButtonClick(2);
}

void CGbProManagerDialog::OnBnClickedManagerGbProAccountButton3()
{
    ProcessButtonClick(3);
}

void CGbProManagerDialog::OnBnClickedManagerGbProAccountButton4()
{
    ProcessButtonClick(4);
}

void CGbProManagerDialog::OnBnClickedManagerGbProAccountButton5()
{
    ProcessButtonClick(5);
}

void CGbProManagerDialog::OnBnClickedManagerGbProAccountButton6()
{
    ProcessButtonClick(6);
}

void CGbProManagerDialog::OnBnClickedManagerGbProAccountButton7()
{
    ProcessButtonClick(7);
}

void CGbProManagerDialog::OnBnClickedManagerGbProAccountButton8()
{
    ProcessButtonClick(8);
}

void CGbProManagerDialog::OnBnClickedManagerGbProAccountButton9()
{
    ProcessButtonClick(9);
}

void CGbProManagerDialog::OnBnClickedManagerGbProAccountButton0()
{
    ProcessButtonClick(0);
}

void CGbProManagerDialog::OnBnClickedManagerGbProAccountClearButton()
{
    player_account = 0;
    SetDlgItemText(IDC_GB_PRO_PLAYER_ACCOUNT_EDIT, "");
}
