/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	12/04/13	Allow manager awarded points on the XP Controller if there is not an IDC ip address or game tender client available.
Robert Fuller   07/24/08    Initial Revision.
*/


// ManagerAwardPoints.cpp : implementation file
//
#define _CRT_RAND_S

#include "stdafx.h"
#include "xp_controller.h"
#include "ManagerAwardPoints.h"

// CManagerAwardPoints dialog

IMPLEMENT_DYNAMIC(CManagerAwardPoints, CDialog)
CManagerAwardPoints::CManagerAwardPoints(CWnd* pParent /*=NULL*/)
	: CDialog(CManagerAwardPoints::IDD, pParent)
{
    current_edit_focus  = 0;
    member_id = 0;

	VERIFY(keypad0.LoadBitmaps(_T("SKEYPAD0U"), _T("SKEYPAD0D")));
	VERIFY(keypad1.LoadBitmaps(_T("SKEYPAD1U"), _T("SKEYPAD1D")));
	VERIFY(keypad2.LoadBitmaps(_T("SKEYPAD2U"), _T("SKEYPAD2D")));
	VERIFY(keypad3.LoadBitmaps(_T("SKEYPAD3U"), _T("SKEYPAD3D")));
	VERIFY(keypad4.LoadBitmaps(_T("SKEYPAD4U"), _T("SKEYPAD4D")));
	VERIFY(keypad5.LoadBitmaps(_T("SKEYPAD5U"), _T("SKEYPAD5D")));
	VERIFY(keypad6.LoadBitmaps(_T("SKEYPAD6U"), _T("SKEYPAD6D")));
	VERIFY(keypad7.LoadBitmaps(_T("SKEYPAD7U"), _T("SKEYPAD7D")));
	VERIFY(keypad8.LoadBitmaps(_T("SKEYPAD8U"), _T("SKEYPAD8D")));
	VERIFY(keypad9.LoadBitmaps(_T("SKEYPAD9U"), _T("SKEYPAD9D")));
	VERIFY(keypadC.LoadBitmaps(_T("SCLEARU"), _T("SCLEARD")));

	VERIFY(exit.LoadBitmaps(_T("SEXITU"), _T("SEXITD")));

	VERIFY(award_5000_points_button.LoadBitmaps(_T("SFIVEKU"), _T("SFIVEKD")));
	VERIFY(award_10000_points_button.LoadBitmaps(_T("STENKU"), _T("STENKD")));
	VERIFY(award_25000_points_button.LoadBitmaps(_T("STWOFIVEKU"), _T("STWOFIVEKD")));

}

CManagerAwardPoints::~CManagerAwardPoints()
{
}

void CManagerAwardPoints::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MANAGER_MEMBER_NUMBER_EDIT, member_id_edit);
}


BEGIN_MESSAGE_MAP(CManagerAwardPoints, CDialog)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_EXIT_MANAGER_BUTTON, OnBnClickedExitManagerButton)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_MANAGER_5000_POINTS_BUTTON, OnBnClickedManagerAward5000PointsButton)
	ON_BN_CLICKED(IDC_MANAGER_10000_POINTS_BUTTON, OnBnClickedManagerAward10000PointsButton)
	ON_BN_CLICKED(IDC_MANAGER_25000_POINTS_BUTTON, OnBnClickedManagerAward25000PointsButton)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON1, OnBnClickedManagerMenuButton1)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON2, OnBnClickedManagerMenuButton2)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON3, OnBnClickedManagerMenuButton3)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON4, OnBnClickedManagerMenuButton4)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON5, OnBnClickedManagerMenuButton5)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON6, OnBnClickedManagerMenuButton6)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON7, OnBnClickedManagerMenuButton7)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON8, OnBnClickedManagerMenuButton8)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON9, OnBnClickedManagerMenuButton9)
	ON_BN_CLICKED(IDC_MANAGER_MENU_BUTTON0, OnBnClickedManagerMenuButton0)
	ON_BN_CLICKED(IDC_MANAGER_MENU_CLEAR_BUTTON, OnBnClickedManagerMenuClearButton)
END_MESSAGE_MAP()


void CManagerAwardPoints::OnBnClickedExitManagerButton()
{
    theApp.current_dialog_return = IDD_MANAGER_MENU_DIALOG;
    DestroyWindow();
}

void CManagerAwardPoints::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CManagerAwardPoints::OnBnClickedManagerAward5000PointsButton()
{

    GbProPointsTicketsCash gb_pro_points_tickets_cash;
    CString prt_buf;

    memset(&gb_pro_points_tickets_cash, 0, sizeof(gb_pro_points_tickets_cash));

    if (member_id)
    {
        memset(&gb_pro_points_tickets_cash, 0, sizeof(gb_pro_points_tickets_cash));
        gb_pro_points_tickets_cash.player_account = member_id;
        gb_pro_points_tickets_cash.bonus_points = 5000;
		gb_pro_points_tickets_cash.promotion_id = 0x4D; // manager awarded points promo id
        // generate a transaction_id
		unsigned int transaction_id;
        rand_s((unsigned int*)&transaction_id);
		gb_pro_points_tickets_cash.transaction_id = transaction_id;

        CGameCommControl temp_game_comm_control;
        temp_game_comm_control.ProcessGbProPointsTicketsCash(&gb_pro_points_tickets_cash, 0);

	    theApp.PrinterData(" \n\n");
	    theApp.PrinterData(" \n_________________________________________\n");
        theApp.PrinterId();
        theApp.PrinterTitle(" ");
	    theApp.PrinterData(" \nMANAGER AWARDED 5,000 POINTS\n");
   		prt_buf.Format(" \nTO MEMBER ACCT: %u\n", member_id);
   		theApp.PrinterData(prt_buf.GetBuffer(0));
	    theApp.PrinterData(" \n_____________________________\n");



        keypad0.DestroyWindow();
        keypad1.DestroyWindow();
        keypad2.DestroyWindow();
        keypad3.DestroyWindow();
        keypad4.DestroyWindow();
        keypad5.DestroyWindow();
        keypad6.DestroyWindow();
        keypad7.DestroyWindow();
        keypad8.DestroyWindow();
        keypad9.DestroyWindow();
        keypadC.DestroyWindow();

        award_5000_points_button.DestroyWindow();
        award_10000_points_button.DestroyWindow();
        award_25000_points_button.DestroyWindow();
        member_id_edit.DestroyWindow();

        CWnd* item;

        item = 0;
        item = GetDlgItem(IDC_AWARD_POINTS);
        if (item)
            item->ShowWindow(SW_HIDE);
        item = 0;
        item = GetDlgItem(IDC_MEMBERNUM);
        if (item)
            item->ShowWindow(SW_HIDE);

        SetDlgItemText(IDC_AWARD_POINTS_TITLE_STATIC, "5,000 POINTS SUCCESSFULLY AWARDED");
    }

}

void CManagerAwardPoints::OnBnClickedManagerAward10000PointsButton()
{

    GbProPointsTicketsCash gb_pro_points_tickets_cash;
    CString prt_buf;

    memset(&gb_pro_points_tickets_cash, 0, sizeof(gb_pro_points_tickets_cash));

    if (member_id)
    {
        memset(&gb_pro_points_tickets_cash, 0, sizeof(gb_pro_points_tickets_cash));
        gb_pro_points_tickets_cash.player_account = member_id;
        gb_pro_points_tickets_cash.bonus_points = 10000;
		gb_pro_points_tickets_cash.promotion_id = 0x4D; // manager awarded points promo id
        // generate a transaction_id
		unsigned int transaction_id;
        rand_s((unsigned int*)&transaction_id);
		gb_pro_points_tickets_cash.transaction_id = transaction_id;
        CGameCommControl temp_game_comm_control;
        temp_game_comm_control.ProcessGbProPointsTicketsCash(&gb_pro_points_tickets_cash, 0);

        keypad0.DestroyWindow();
        keypad1.DestroyWindow();
        keypad2.DestroyWindow();
        keypad3.DestroyWindow();
        keypad4.DestroyWindow();
        keypad5.DestroyWindow();
        keypad6.DestroyWindow();
        keypad7.DestroyWindow();
        keypad8.DestroyWindow();
        keypad9.DestroyWindow();
        keypadC.DestroyWindow();

	    theApp.PrinterData(" \n\n");
	    theApp.PrinterData(" \n_________________________________________\n");
        theApp.PrinterId();
        theApp.PrinterTitle(" ");
	    theApp.PrinterData(" \nMANAGER AWARDED 10,000 POINTS\n");
   		prt_buf.Format(" \nTO MEMBER ACCT: %u\n", member_id);
   		theApp.PrinterData(prt_buf.GetBuffer(0));
	    theApp.PrinterData(" \n_____________________________\n");

        award_5000_points_button.DestroyWindow();
        award_10000_points_button.DestroyWindow();
        award_25000_points_button.DestroyWindow();
        member_id_edit.DestroyWindow();

        CWnd* item;

        item = 0;
        item = GetDlgItem(IDC_AWARD_POINTS);
        if (item)
            item->ShowWindow(SW_HIDE);
        item = 0;
        item = GetDlgItem(IDC_MEMBERNUM);
        if (item)
            item->ShowWindow(SW_HIDE);

        SetDlgItemText(IDC_AWARD_POINTS_TITLE_STATIC, "10,000 POINTS SUCCESSFULLY AWARDED");
    }

}

void CManagerAwardPoints::OnBnClickedManagerAward25000PointsButton()
{

    GbProPointsTicketsCash gb_pro_points_tickets_cash;
    CString prt_buf;

    memset(&gb_pro_points_tickets_cash, 0, sizeof(gb_pro_points_tickets_cash));

    if (member_id)
    {
        memset(&gb_pro_points_tickets_cash, 0, sizeof(gb_pro_points_tickets_cash));
        gb_pro_points_tickets_cash.player_account = member_id;
        gb_pro_points_tickets_cash.bonus_points = 25000;
		gb_pro_points_tickets_cash.promotion_id = 0x4D; // manager awarded points promo id
        // generate a transaction_id
		unsigned int transaction_id;
        rand_s((unsigned int*)&transaction_id);
		gb_pro_points_tickets_cash.transaction_id = transaction_id;

        CGameCommControl temp_game_comm_control;
        temp_game_comm_control.ProcessGbProPointsTicketsCash(&gb_pro_points_tickets_cash, 0);

        keypad0.DestroyWindow();
        keypad1.DestroyWindow();
        keypad2.DestroyWindow();
        keypad3.DestroyWindow();
        keypad4.DestroyWindow();
        keypad5.DestroyWindow();
        keypad6.DestroyWindow();
        keypad7.DestroyWindow();
        keypad8.DestroyWindow();
        keypad9.DestroyWindow();
        keypadC.DestroyWindow();

	    theApp.PrinterData(" \n\n");
	    theApp.PrinterData(" \n_________________________________________\n");
        theApp.PrinterId();
        theApp.PrinterTitle(" ");
	    theApp.PrinterData(" \nMANAGER AWARDED 25,000 POINTS\n");
   		prt_buf.Format(" \nTO MEMBER ACCT: %u\n", member_id);
   		theApp.PrinterData(prt_buf.GetBuffer(0));
	    theApp.PrinterData(" \n_____________________________\n");

        award_5000_points_button.DestroyWindow();
        award_10000_points_button.DestroyWindow();
        award_25000_points_button.DestroyWindow();
        member_id_edit.DestroyWindow();

        CWnd* item;

        item = 0;
        item = GetDlgItem(IDC_AWARD_POINTS);
        if (item)
            item->ShowWindow(SW_HIDE);
        item = 0;
        item = GetDlgItem(IDC_MEMBERNUM);
        if (item)
            item->ShowWindow(SW_HIDE);

        SetDlgItemText(IDC_AWARD_POINTS_TITLE_STATIC, "25,000 POINTS SUCCESSFULLY AWARDED");
    }

}


void CManagerAwardPoints::ProcessButtonClick(BYTE button_press)
{
 CString edit_string;

//    else if (current_edit_focus == IDC_MANAGER_MEMBER_NUMBER_EDIT)
    {
        member_id = member_id * 10 + button_press;
        if (member_id > 999999999)
            member_id = 0;
        edit_string.Format("%u", member_id);
        SetDlgItemText(IDC_MANAGER_MEMBER_NUMBER_EDIT, edit_string);
    }
}

void CManagerAwardPoints::OnBnClickedManagerMenuButton1()
{
    ProcessButtonClick(1);
}

void CManagerAwardPoints::OnBnClickedManagerMenuButton2()
{
    ProcessButtonClick(2);
}

void CManagerAwardPoints::OnBnClickedManagerMenuButton3()
{
    ProcessButtonClick(3);
}

void CManagerAwardPoints::OnBnClickedManagerMenuButton4()
{
    ProcessButtonClick(4);
}

void CManagerAwardPoints::OnBnClickedManagerMenuButton5()
{
    ProcessButtonClick(5);
}

void CManagerAwardPoints::OnBnClickedManagerMenuButton6()
{
    ProcessButtonClick(6);
}

void CManagerAwardPoints::OnBnClickedManagerMenuButton7()
{
    ProcessButtonClick(7);
}

void CManagerAwardPoints::OnBnClickedManagerMenuButton8()
{
    ProcessButtonClick(8);
}

void CManagerAwardPoints::OnBnClickedManagerMenuButton9()
{
    ProcessButtonClick(9);
}

void CManagerAwardPoints::OnBnClickedManagerMenuButton0()
{
    ProcessButtonClick(0);
}

void CManagerAwardPoints::OnBnClickedManagerMenuClearButton()
{
//    else if (current_edit_focus == IDC_MANAGER_MEMBER_NUMBER_EDIT)
    {
        member_id = 0;
        SetDlgItemText(IDC_MANAGER_MEMBER_NUMBER_EDIT, "");
    }
}

BOOL CManagerAwardPoints::OnInitDialog()
{
	CDialog::OnInitDialog();

//    SetDialogBkColor(RGB(255, 0, 0), RGB(0, 255, 0));

    VERIFY(keypad0.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON0, this));
    keypad0.SizeToContent();
    VERIFY(keypad1.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON1, this));
    keypad1.SizeToContent();
    VERIFY(keypad2.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON2, this));
    keypad2.SizeToContent();
    VERIFY(keypad3.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON3, this));
    keypad3.SizeToContent();
    VERIFY(keypad4.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON4, this));
    keypad4.SizeToContent();
    VERIFY(keypad5.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON5, this));
    keypad5.SizeToContent();
    VERIFY(keypad6.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON6, this));
    keypad6.SizeToContent();
    VERIFY(keypad7.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON7, this));
    keypad7.SizeToContent();
    VERIFY(keypad8.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON8, this));
    keypad8.SizeToContent();
    VERIFY(keypad9.SubclassDlgItem(IDC_MANAGER_MENU_BUTTON9, this));
    keypad9.SizeToContent();
    VERIFY(keypadC.SubclassDlgItem(IDC_MANAGER_MENU_CLEAR_BUTTON, this));
    keypadC.SizeToContent();

    VERIFY(exit.SubclassDlgItem(IDC_EXIT_MANAGER_BUTTON, this));
    exit.SizeToContent();

    VERIFY(award_5000_points_button.SubclassDlgItem(IDC_MANAGER_5000_POINTS_BUTTON, this));
    award_5000_points_button.SizeToContent();
    VERIFY(award_10000_points_button.SubclassDlgItem(IDC_MANAGER_10000_POINTS_BUTTON, this));
    award_10000_points_button.SizeToContent();
    VERIFY(award_25000_points_button.SubclassDlgItem(IDC_MANAGER_25000_POINTS_BUTTON, this));
    award_25000_points_button.SizeToContent();

    if (theApp.memory_settings_ini.masters_ip_address)
    {
        keypad0.DestroyWindow();
        keypad1.DestroyWindow();
        keypad2.DestroyWindow();
        keypad3.DestroyWindow();
        keypad4.DestroyWindow();
        keypad5.DestroyWindow();
        keypad6.DestroyWindow();
        keypad7.DestroyWindow();
        keypad8.DestroyWindow();
        keypad9.DestroyWindow();
        keypadC.DestroyWindow();

        award_5000_points_button.DestroyWindow();
        award_10000_points_button.DestroyWindow();
        award_25000_points_button.DestroyWindow();
        member_id_edit.DestroyWindow();


        CWnd* item;

        item = 0;
        item = GetDlgItem(IDC_AWARD_POINTS);
        if (item)
            item->ShowWindow(SW_HIDE);
        item = 0;
        item = GetDlgItem(IDC_MEMBERNUM);
        if (item)
            item->ShowWindow(SW_HIDE);

    }

 return TRUE;
}

void CManagerAwardPoints::OnPaint() 
{
	CRect rect;
	GetClientRect(&rect);
	CPaintDC dc(this); // device context for painting

	CDialog::OnPaint();

    dc.FillSolidRect(&rect, 0x00000000);
}
