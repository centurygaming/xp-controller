/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	05/26/08	Removed newlines at end of strings shown in screen edit boxes.
Dave Elquist    06/24/05    Initial Revision.
*/


// ManagerGbProReports.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include ".\managergbproreports.h"


// CManagerGbProReports dialog

IMPLEMENT_DYNAMIC(CManagerGbProReports, CDialog)
CManagerGbProReports::CManagerGbProReports(CWnd* pParent /*=NULL*/)
	: CDialog(CManagerGbProReports::IDD, pParent)
{
    button_press = 0;
}

CManagerGbProReports::~CManagerGbProReports()
{
}

void CManagerGbProReports::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MANAGER_GB_PRO_REPORTS_TICKETS_BUTTON, m_manager_tickets_button);
	DDX_Control(pDX, IDC_MANAGER_GB_PRO_REPORTS_GAME_TICKETS_BUTTON, m_manager_game_tickets_button);
	DDX_Control(pDX, IDC_MANAGER_GB_PRO_REPORTS_LIST, m_manager_listbox);
}


BEGIN_MESSAGE_MAP(CManagerGbProReports, CDialog)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_REPORTS_POINTS_BUTTON, OnBnClickedManagerGbProReportsPointsButton)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_REPORTS_TICKETS_BUTTON, OnBnClickedManagerGbProReportsTicketsButton)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_REPORTS_GAME_POINTS_BUTTON, OnBnClickedManagerGbProReportsGamePointsButton)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_REPORTS_GAME_TICKETS_BUTTON, OnBnClickedManagerGbProReportsGameTicketsButton)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_REPORTS_LIST_ALL_BUTTON, OnBnClickedManagerGbProReportsListAllButton)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_REPORTS_PRINT_BUTTON, OnBnClickedManagerGbProReportsPrintButton)
	ON_BN_CLICKED(IDC_MANAGER_GB_PRO_REPORTS_EXIT_BUTTON, &CManagerGbProReports::OnBnClickedManagerGbProReportsExitButton)
END_MESSAGE_MAP()


// CManagerGbProReports message handlers

void CManagerGbProReports::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CManagerGbProReports::OnBnClickedManagerGbProReportsPointsButton()
{
    OnBnClickedManagerGbProReportsPointsButton(false, true);
}

void CManagerGbProReports::OnBnClickedManagerGbProReportsPointsButton(bool print_data, bool clear_window)
{
 DWORD file_index = 0;
 CString prt_buf;
 FileGbProManagerPoints file_manager_points;

    button_press = IDC_MANAGER_GB_PRO_REPORTS_POINTS_BUTTON;
    if (clear_window)
        m_manager_listbox.ResetContent();

    if (print_data)
    {
        theApp.printer_busy = true;
        theApp.PrinterHeader(0);
    }

    prt_buf = "MANAGER POINTS";
    m_manager_listbox.AddString(prt_buf);
    if (print_data)
        theApp.PrinterData(prt_buf.GetBuffer(0));

    while (fileRead(GB_PRO_MANAGER_POINTS_FILE, file_index, &file_manager_points, sizeof(file_manager_points)))
    {
        prt_buf.Format("%s  ID %u", convertTimeStamp(file_manager_points.memory.time_stamp), file_manager_points.memory.system_user_id);
        m_manager_listbox.AddString(prt_buf);
        if (print_data)
            theApp.PrinterData(prt_buf.GetBuffer(0));

        prt_buf.Format("ACCT: %u, PTS: %u", file_manager_points.memory.player_account, file_manager_points.memory.gb_points);
        m_manager_listbox.AddString(prt_buf);
        if (print_data)
            theApp.PrinterData(prt_buf.GetBuffer(0));

        file_index++;
    }

    if (!file_index)
        prt_buf = "NO RECORDS FOUND";
    else
        prt_buf = "";

    m_manager_listbox.AddString(prt_buf);
    if (print_data)
    {
        theApp.PrinterData(prt_buf.GetBuffer(0));
        theApp.printer_busy = false;
    }
}

void CManagerGbProReports::OnBnClickedManagerGbProReportsTicketsButton()
{
    OnBnClickedManagerGbProReportsTicketsButton(false, true);
}

void CManagerGbProReports::OnBnClickedManagerGbProReportsTicketsButton(bool print_data, bool clear_window)
{
 DWORD file_index = 0;
 CString prt_buf;
 FileGbProManagerTickets file_manager_ticket;

    button_press = IDC_MANAGER_GB_PRO_REPORTS_TICKETS_BUTTON;
    if (clear_window)
        m_manager_listbox.ResetContent();

    if (print_data)
    {
        theApp.printer_busy = true;
        theApp.PrinterHeader(0);
    }

    prt_buf = "MANAGER TICKET";
    m_manager_listbox.AddString(prt_buf);
    if (print_data)
        theApp.PrinterData(prt_buf.GetBuffer(0));

    while (fileRead(GB_PRO_MANAGER_TICKETS_FILE, file_index, &file_manager_ticket, sizeof(file_manager_ticket)))
    {
        prt_buf.Format("%s  ID %u\n", convertTimeStamp(file_manager_ticket.memory.time_stamp), file_manager_ticket.memory.system_user_id);
        m_manager_listbox.AddString(prt_buf);
        if (print_data)
            theApp.PrinterData(prt_buf.GetBuffer(0));

        file_index++;
    }

    if (!file_index)
        prt_buf = "NO RECORDS FOUND";
    else
        prt_buf = "";

    m_manager_listbox.AddString(prt_buf);
    if (print_data)
    {
        theApp.PrinterData(prt_buf.GetBuffer(0));
        theApp.printer_busy = false;
    }
}

void CManagerGbProReports::OnBnClickedManagerGbProReportsGamePointsButton()
{
    OnBnClickedManagerGbProReportsGamePointsButton(false, true);
}

void CManagerGbProReports::OnBnClickedManagerGbProReportsGamePointsButton(bool print_data, bool clear_window) //?????? redo reporting function
{
 DWORD file_index = 0;
 CString prt_buf;
 GbProPointsAndTicketsFile file_game_points_tickets;

    button_press = IDC_MANAGER_GB_PRO_REPORTS_GAME_POINTS_BUTTON;
    if (clear_window)
        m_manager_listbox.ResetContent();

    if (print_data)
    {
        theApp.printer_busy = true;
        theApp.PrinterHeader(0);
    }

    prt_buf = "GAME POINTS";
    m_manager_listbox.AddString(prt_buf);
    if (print_data)
        theApp.PrinterData(prt_buf.GetBuffer(0));

    while (fileRead(GB_PRO_POINTS_TICKETS_FILE, file_index, &file_game_points_tickets, sizeof(file_game_points_tickets)))
    {
        prt_buf.Format("%s  MACHINE %u\nACCT %u, PTS %u", convertTimeStamp(file_game_points_tickets.time_stamp),
            file_game_points_tickets.ucmc_id, file_game_points_tickets.memory.player_account,
            file_game_points_tickets.memory.bonus_points);
        m_manager_listbox.AddString(prt_buf);
        if (print_data)
            theApp.PrinterData(prt_buf.GetBuffer(0));

        file_index++;
    }

    prt_buf = "";
    m_manager_listbox.AddString(prt_buf);
    if (print_data)
    {
        theApp.PrinterData(prt_buf.GetBuffer(0));
        theApp.printer_busy = false;
    }
}

void CManagerGbProReports::OnBnClickedManagerGbProReportsGameTicketsButton()
{
    OnBnClickedManagerGbProReportsGameTicketsButton(false, true);
}

void CManagerGbProReports::OnBnClickedManagerGbProReportsGameTicketsButton(bool print_data, bool clear_window) //?????? redo reporting function
{
 DWORD file_index = 0;
 CString prt_buf;
 GbProPointsAndTicketsFile file_game_points_tickets;

    button_press = IDC_MANAGER_GB_PRO_REPORTS_GAME_TICKETS_BUTTON;
    if (clear_window)
        m_manager_listbox.ResetContent();

    if (print_data)
    {
        theApp.printer_busy = true;
        theApp.PrinterHeader(0);
    }

    prt_buf = "GAME TICKETS";
    m_manager_listbox.AddString(prt_buf);
    if (print_data)
        theApp.PrinterData(prt_buf.GetBuffer(0));

    while (fileRead(GB_PRO_POINTS_TICKETS_FILE, file_index, &file_game_points_tickets, sizeof(file_game_points_tickets)))
    {
        prt_buf.Format("%s  MACHINE %u, TKTS %u", convertTimeStamp(file_game_points_tickets.time_stamp),
            file_game_points_tickets.ucmc_id, file_game_points_tickets.memory.print_drawing_ticket);
        m_manager_listbox.AddString(prt_buf);
        if (print_data)
            theApp.PrinterData(prt_buf.GetBuffer(0));

        file_index++;
    }

    prt_buf = "";
    m_manager_listbox.AddString(prt_buf);
    if (print_data)
    {
        theApp.PrinterData(prt_buf.GetBuffer(0));
        theApp.printer_busy = false;
    }
}

void CManagerGbProReports::OnBnClickedManagerGbProReportsListAllButton()
{
    button_press = IDC_MANAGER_GB_PRO_REPORTS_LIST_ALL_BUTTON;
    m_manager_listbox.ResetContent();
    OnBnClickedManagerGbProReportsPointsButton(false, false);
    OnBnClickedManagerGbProReportsTicketsButton(false, false);
    OnBnClickedManagerGbProReportsGamePointsButton(false, false);
    OnBnClickedManagerGbProReportsGameTicketsButton(false, false);
}

void CManagerGbProReports::OnBnClickedManagerGbProReportsPrintButton()
{
    if (button_press == IDC_MANAGER_GB_PRO_REPORTS_POINTS_BUTTON)
        OnBnClickedManagerGbProReportsPointsButton(true, true);
    else if (button_press == IDC_MANAGER_GB_PRO_REPORTS_TICKETS_BUTTON)
        OnBnClickedManagerGbProReportsTicketsButton(true, true);
    else if (button_press == IDC_MANAGER_GB_PRO_REPORTS_GAME_POINTS_BUTTON)
        OnBnClickedManagerGbProReportsGamePointsButton(true, true);
    else if (button_press == IDC_MANAGER_GB_PRO_REPORTS_GAME_TICKETS_BUTTON)
        OnBnClickedManagerGbProReportsGameTicketsButton(true, true);
    else if (button_press == IDC_MANAGER_GB_PRO_REPORTS_LIST_ALL_BUTTON)
    {
        m_manager_listbox.ResetContent();
        OnBnClickedManagerGbProReportsPointsButton(true, false);
        OnBnClickedManagerGbProReportsTicketsButton(true, false);
        OnBnClickedManagerGbProReportsGamePointsButton(true, false);
        OnBnClickedManagerGbProReportsGameTicketsButton(true, false);
    }
}

BOOL CManagerGbProReports::OnInitDialog()
{
	CDialog::OnInitDialog();

    if (!theApp.pGbProManager->memory_gb_pro_settings.million_dollar_ticket_server_ip_address ||
        !theApp.pGbProManager->memory_gb_pro_settings.million_dollar_ticket_server_tcp_port)
    {
        m_manager_tickets_button.DestroyWindow();
        m_manager_game_tickets_button.DestroyWindow();
    }

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CManagerGbProReports::OnBnClickedManagerGbProReportsExitButton()
{
	DestroyWindow();
}
