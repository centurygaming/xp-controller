/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    06/24/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"


// CManagerGbProReports dialog

class CManagerGbProReports : public CDialog
{
	DECLARE_DYNAMIC(CManagerGbProReports)

public:
	CManagerGbProReports(CWnd* pParent = NULL);   // standard constructor
	virtual ~CManagerGbProReports();

// Dialog Data
	enum { IDD = IDD_MANAGER_GB_PRO_REPORTS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedManagerGbProReportsPointsButton();
	afx_msg void OnBnClickedManagerGbProReportsTicketsButton();
	afx_msg void OnBnClickedManagerGbProReportsGamePointsButton();
	afx_msg void OnBnClickedManagerGbProReportsGameTicketsButton();
	afx_msg void OnBnClickedManagerGbProReportsListAllButton();
	afx_msg void OnBnClickedManagerGbProReportsPrintButton();
	afx_msg void OnBnClickedManagerGbProReportsExitButton();
	virtual BOOL OnInitDialog();

// private functions
private:
    void OnBnClickedManagerGbProReportsPointsButton(bool print_data, bool clear_window);
	void OnBnClickedManagerGbProReportsTicketsButton(bool print_data, bool clear_window);
	void OnBnClickedManagerGbProReportsGamePointsButton(bool print_data, bool clear_window);
	void OnBnClickedManagerGbProReportsGameTicketsButton(bool print_data, bool clear_window);

// private variables
private:
	CButton m_manager_tickets_button;
	CButton m_manager_game_tickets_button;

    DWORD button_press;
	CListBox m_manager_listbox;
};
