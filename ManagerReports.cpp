/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   03/08/13    Removed obsolete Location Collection code.
Robert Fuller   06/01/12    Send Stratus message when starting or ending a cashier shift
Robert Fuller   02/07/12    Make sure GB Advantage is configured on before accessing the class pointer potentially uninitialized class pointer
Robert Fuller   10/03/11    Print the sas id address as opposed to the irrelevant mux id address for SAS direct connect games.
Robert Fuller   08/03/11    Added code to support Location Collection features.
Robert Fuller   08/09/10    Added mods to support the XP/SAS direct connect feature.
Robert Fuller   07/27/09    Hide a number of buttons in the manager reports screen for slave controllers.
Robert Fuller   07/15/09    Increased the number of shifts to print on the shift report from 4 to 21.
Robert Fuller   06/09/09    Added code for custom picture buttons.
Dave Elquist    07/15/05    Added GB Pro support.
Dave Elquist    06/09/05    Initial Revision.
*/


// ManagerReports.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "ManagerReports.h"
#include ".\managerreports.h"


// CManagerReports dialog

IMPLEMENT_DYNAMIC(CManagerReports, CDialog)
CManagerReports::CManagerReports(CWnd* pParent /*=NULL*/)
	: CDialog(CManagerReports::IDD, pParent)
{
    pManagerGbProReports = NULL;

	VERIFY(m_manager_netwin_button.LoadBitmaps(_T("MNETWINU"), _T("MNETWIND")));
	VERIFY(m_manager_machines_button.LoadBitmaps(_T("MMACHINESU"), _T("MMACHINESD")));
	VERIFY(m_cashier_ids_button.LoadBitmaps(_T("MCASHIERIDSU"), _T("MCASHIERIDSD")));
	VERIFY(m_redeem_button.LoadBitmaps(_T("MREDEEMU"), _T("MREDEEMD")));
//	VERIFY(m_cashier_shift_all_button.LoadBitmaps(_T("MALLU"), _T("MALLD")));
	VERIFY(tickets_button.LoadBitmaps(_T("MTICKETSU"), _T("MTICKETSD")));
	VERIFY(dispenser_button.LoadBitmaps(_T("MDISPENSERU"), _T("MDISPENSERD")));
	VERIFY(irsw2g_button.LoadBitmaps(_T("MIRSW2GU"), _T("MIRSW2GD")));
	VERIFY(adjust_button.LoadBitmaps(_T("MADJUSTU"), _T("MADJUSTD")));
	VERIFY(exit_button.LoadBitmaps(_T("CEXITU"), _T("CEXITD")));

	VERIFY(m_cashier_4shifts_button.LoadBitmaps(_T("4SHIFTSU"), _T("4SHIFTSD")));
	VERIFY(m_cashier_14shifts_button.LoadBitmaps(_T("14SHIFTSU"), _T("14SHIFTSD")));
	VERIFY(m_cashier_21shifts_button.LoadBitmaps(_T("21SHIFTSU"), _T("21SHIFTSD")));

}

CManagerReports::~CManagerReports()
{
    if (pManagerGbProReports)
    {
        pManagerGbProReports->DestroyWindow();
        delete pManagerGbProReports;
    }
}

void CManagerReports::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MANAGER_REPORT_SHIFT_GROUPBOX, m_cashier_shift_groupbox);
//	DDX_Control(pDX, IDC_MANAGER_REPORT_CASHIER_SHIFT_CURRENT_BUTTON, m_cashier_shift_current_button);
	DDX_Control(pDX, IDC_MANAGER_REPORT_CASHIER_ID_STATIC, m_cashier_shift_text);
	DDX_Control(pDX, IDC_COMBO1, m_cashier_shift_combo_box);
	DDX_Control(pDX, IDC_MANAGER_REPORT_GB_PRO_BUTTON, m_manager_reports_gb_pro_button);
}


BEGIN_MESSAGE_MAP(CManagerReports, CDialog)
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_MANAGER_REPORT_TICKETS_BUTTON, OnBnClickedManagerReportTicketsButton)
	ON_BN_CLICKED(IDC_MANAGER_REPORT_DISPENSER_BUTTON, OnBnClickedManagerReportDispenserButton)
	ON_BN_CLICKED(IDC_MANAGER_REPORT_NETWIN_BUTTON, OnBnClickedManagerReportNetwinButton)
	ON_BN_CLICKED(IDC_MANAGER_REPORT_ADJUSTMENTS_BUTTON, OnBnClickedManagerReportAdjustmentsButton)
	ON_BN_CLICKED(IDC_MANAGER_REPORT_W2G_BUTTON, OnBnClickedManagerReportW2gButton)
	ON_BN_CLICKED(IDC_MANAGER_REPORT_MACHINES_BUTTON, OnBnClickedManagerReportMachinesButton)
	ON_BN_CLICKED(IDC_MANAGER_REPORT_CASHIER_IDS_BUTTON, OnBnClickedManagerReportCashierIdsButton)
	ON_BN_CLICKED(IDC_MANAGER_REPORT_REDEEM_BUTTON, OnBnClickedManagerReportRedeemButton)
	ON_BN_CLICKED(IDC_MANAGER_REPORT_CASHIER_SHIFT_CURRENT_BUTTON, OnBnClickedManagerReportCashierShiftCurrentButton)
	ON_BN_CLICKED(IDC_MANAGER_REPORT_CASHIER_SHIFT_4_BUTTON, OnBnClickedManagerReportCashier4ShiftsButton)
	ON_BN_CLICKED(IDC_MANAGER_REPORT_CASHIER_SHIFT_14_BUTTON, OnBnClickedManagerReportCashier14ShiftsButton)
	ON_BN_CLICKED(IDC_MANAGER_REPORT_CASHIER_SHIFT_21_BUTTON, OnBnClickedManagerReportCashier21ShiftsButton)
	ON_BN_CLICKED(IDC_MANAGER_REPORTS_EXIT_BUTTON, OnBnClickedManagerReportsExitButton)
	ON_BN_CLICKED(IDC_MANAGER_REPORT_GB_PRO_BUTTON, OnBnClickedManagerReportGbProButton)
END_MESSAGE_MAP()


// CManagerReports message handlers

void CManagerReports::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CManagerReports::OnBnClickedManagerReportTicketsButton()
{
    theApp.printer_busy = true;
    theApp.DropTicketsPrint(0);
    theApp.printer_busy = false;
}

void CManagerReports::OnBnClickedManagerReportDispenserButton()
{
    theApp.printer_busy = true;
    theApp.PrintDispenserInfo();
    theApp.printer_busy = false;
}

void CManagerReports::OnBnClickedManagerReportNetwinButton()
{
    theApp.GetCustomerIdSelection(1);

    if (theApp.get_customer_id_selection)
    {
        EnableWindow(false);
        theApp.GetNetwinReport(true, theApp.get_customer_id_selection, true);
        EnableWindow();
    }
}

void CManagerReports::OnBnClickedManagerReportAdjustmentsButton()
{
    theApp.printer_busy = true;
    theApp.CashAdjustmentReport();
    theApp.printer_busy = false;
}

void CManagerReports::OnBnClickedManagerReportW2gButton()
{
    theApp.printer_busy = true;
    theApp.PrintW2gTickets(0);
    theApp.printer_busy = false;
}

void CManagerReports::OnBnClickedManagerReportMachinesButton()
{
 char append_string[3];
 WORD total_games = 0, total_port_games, total_sas_games;
 CString prt_buf;
 CGameCommControl *pGameCommControl = theApp.pHeadGameCommControl;
 CGameDevice *pGameDevice;

    theApp.printer_busy = true;
    theApp.PrinterTitle("MACHINE LOG");
    theApp.PrinterData("\n");
    theApp.PrinterData("\n*=block remote resets");
    theApp.PrinterData("\n#=sound for game lock\n");

    while (pGameCommControl)
    {
        if (!pGameCommControl->sas_com)
        {
            total_port_games = 0;

            pGameDevice = pGameCommControl->pHeadGameDevice;
            prt_buf.Format("\nSerial Port: %u\n", pGameCommControl->port_info.port_number);
            theApp.PrinterData(prt_buf.GetBuffer(0));

            if (pGameDevice)
            {
                prt_buf = "\n  MUX   UCMC ID\n --------------\n";
                theApp.PrinterData(prt_buf.GetBuffer(0));
            }

            while (pGameDevice)
            {
                if (pGameDevice->memory_game_config.version != GB_INTERFACE_VERSION)
                {
                    total_port_games++;
                    total_games++;

                    if (pGameDevice->memory_game_config.block_remote_game_unlocks)
                        append_string[0] = '*';
                    else
                        append_string[0] = ' ';

                    if (pGameDevice->memory_game_config.sound_for_game_lock)
                        append_string[1] = '#';
                    else
                        append_string[1] = ' ';

                    append_string[2] = 0;

                    prt_buf.Format("   %2.2ld   %6ld%s\n", pGameDevice->memory_game_config.mux_id,
                        pGameDevice->memory_game_config.ucmc_id, append_string);
                    theApp.PrinterData(prt_buf.GetBuffer(0));

                }
                pGameDevice = pGameDevice->pNextGame;
            }

            if (total_port_games)
            {
                prt_buf.Format(" TOTAL MACHINES ON PORT: %u", total_port_games);
                theApp.PrinterData(prt_buf.GetBuffer(0));
            }
            else
                theApp.PrinterData(" ** NO MACHINES ON PORT **\n");

            theApp.PrinterData(PRINTER_FORM_FEED);
        }
        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }

    total_sas_games = 0;

    pGameCommControl = theApp.pHeadGameCommControl;

    prt_buf = "\n XP SAS Direct Connect Games \n";
    theApp.PrinterData(prt_buf.GetBuffer(0));

    prt_buf = "\n SAS ID   UCMC ID\n --------------\n";
    theApp.PrinterData(prt_buf.GetBuffer(0));

    while (pGameCommControl)
    {
        if (pGameCommControl->sas_com)
        {
            total_sas_games++;
            total_games++;

            pGameDevice = pGameCommControl->pHeadGameDevice;
//            prt_buf.Format("\nSerial Port: %u\n", pGameCommControl->port_info.port_number);
//            theApp.PrinterData(prt_buf.GetBuffer(0));

            if (pGameDevice)
            {

                if (pGameDevice->memory_game_config.block_remote_game_unlocks)
                    append_string[0] = '*';
                else
                    append_string[0] = ' ';

                if (pGameDevice->memory_game_config.sound_for_game_lock)
                    append_string[1] = '#';
                else
                    append_string[1] = ' ';

                append_string[2] = 0;

                prt_buf.Format("   %2.2ld      %6ld%s\n", pGameDevice->memory_game_config.sas_id,
                    pGameDevice->memory_game_config.ucmc_id, append_string);
                theApp.PrinterData(prt_buf.GetBuffer(0));

//                theApp.PrinterData(PRINTER_FORM_FEED);
            }
        }
        pGameCommControl = pGameCommControl->pNextGameCommControl;
    }

    if (total_sas_games)
    {
        prt_buf.Format(" TOTAL SAS GAMES: %u", total_sas_games);
        theApp.PrinterData(prt_buf.GetBuffer(0));
    }
    else
        theApp.PrinterData(" ** NO SAS GAMES ON PORT **\n");

    theApp.PrinterData(PRINTER_FORM_FEED);

    prt_buf = "\n\n";
    theApp.PrinterData(prt_buf.GetBuffer(0));


    if (total_games)
    {
        prt_buf.Format(" TOTAL MACHINES: %u\n\n", total_games);
        theApp.PrinterData(prt_buf.GetBuffer(0));
    }
    else
        theApp.PrinterData(" ** NO MACHINES **\n");

    theApp.PrinterData(PRINTER_FORM_FEED);
    theApp.printer_busy = false;
}

void CManagerReports::OnBnClickedManagerReportCashierIdsButton()
{
 bool cashier_found = false;
 DWORD file_index = 0;
 CString prt_buf;
 CashierAndManagerUserFile cashier_user;

    theApp.printer_busy = true;
    theApp.PrinterTitle("CASHIER IDS");
    theApp.PrinterData("\nCASHIER\n-------\n");

    while (fileRead(LOCATION_IDS, file_index, &cashier_user, sizeof(cashier_user)))
    {
        if (cashier_user.user.id / 10000 == CASHIER)
        {
            cashier_found = true;
            prt_buf.Format(" %u: %s %s\n", cashier_user.user.id, cashier_user.user.first_name, cashier_user.user.last_name);
            theApp.PrinterData(prt_buf.GetBuffer(0));
        }

        file_index++;
    }

    if (!cashier_found)
        theApp.PrinterData(" ** NO CASHIER IDS **\n");

    theApp.PrinterData(PRINTER_FORM_FEED);
    theApp.printer_busy = false;
}

void CManagerReports::OnBnClickedManagerReportRedeemButton()
{
    theApp.GetCustomerIdSelection(2);

    if (theApp.get_customer_id_selection)
    {
        theApp.printer_busy = true;
        theApp.GameRedeemReport(theApp.get_customer_id_selection);
        theApp.printer_busy = false;
    }
}

void CManagerReports::OnBnClickedManagerReportCashierShiftCurrentButton()
{
 int item_selected = m_cashier_shift_combo_box.GetCurSel();
 CString edit_string;

    if (item_selected != CB_ERR)
    {
        m_cashier_shift_combo_box.GetLBText(item_selected, edit_string);
        item_selected = atoi(edit_string);

        if (item_selected / 10000 == CASHIER)
        {
            theApp.printer_busy = true;
            theApp.PrintCashierShift(item_selected, 1, false, false);
            theApp.printer_busy = false;
        }
    }
}

void CManagerReports::OnBnClickedManagerReportCashier4ShiftsButton()
{
    theApp.PrintCashierShift(0, 4, false, false);
}

void CManagerReports::OnBnClickedManagerReportCashier14ShiftsButton()
{
    theApp.PrintCashierShift(0, 14, false, false);
}

void CManagerReports::OnBnClickedManagerReportCashier21ShiftsButton()
{
    theApp.PrintCashierShift(0, 21, false, false);
}

BOOL CManagerReports::OnInitDialog()
{
 DWORD file_index;
 CString edit_string;
 CashierAndManagerUserFile cashier_user;

	CDialog::OnInitDialog();


    VERIFY(m_manager_netwin_button.SubclassDlgItem(IDC_MANAGER_REPORT_NETWIN_BUTTON, this));
    m_manager_netwin_button.SizeToContent();
    VERIFY(m_manager_machines_button.SubclassDlgItem(IDC_MANAGER_REPORT_MACHINES_BUTTON, this));
    m_manager_machines_button.SizeToContent();
    VERIFY(m_cashier_ids_button.SubclassDlgItem(IDC_MANAGER_REPORT_CASHIER_IDS_BUTTON, this));
    m_cashier_ids_button.SizeToContent();
    VERIFY(m_redeem_button.SubclassDlgItem(IDC_MANAGER_REPORT_REDEEM_BUTTON, this));
    m_redeem_button.SizeToContent();
//    VERIFY(m_cashier_shift_all_button.SubclassDlgItem(IDC_MANAGER_REPORT_CASHIER_SHIFT_4_BUTTON, this));
//    m_cashier_shift_all_button.SizeToContent();
    VERIFY(tickets_button.SubclassDlgItem(IDC_MANAGER_REPORT_TICKETS_BUTTON, this));
    tickets_button.SizeToContent();
    VERIFY(dispenser_button.SubclassDlgItem(IDC_MANAGER_REPORT_DISPENSER_BUTTON, this));
    dispenser_button.SizeToContent();
    VERIFY(irsw2g_button.SubclassDlgItem(IDC_MANAGER_REPORT_W2G_BUTTON, this));
    irsw2g_button.SizeToContent();
    VERIFY(adjust_button.SubclassDlgItem(IDC_MANAGER_REPORT_ADJUSTMENTS_BUTTON, this));
    adjust_button.SizeToContent();
    VERIFY(exit_button.SubclassDlgItem(IDC_MANAGER_REPORTS_EXIT_BUTTON, this));
    exit_button.SizeToContent();


    VERIFY(m_cashier_4shifts_button.SubclassDlgItem(IDC_MANAGER_REPORT_CASHIER_SHIFT_4_BUTTON, this));
    m_cashier_4shifts_button.SizeToContent();
    VERIFY(m_cashier_14shifts_button.SubclassDlgItem(IDC_MANAGER_REPORT_CASHIER_SHIFT_14_BUTTON, this));
    m_cashier_14shifts_button.SizeToContent();
    VERIFY(m_cashier_21shifts_button.SubclassDlgItem(IDC_MANAGER_REPORT_CASHIER_SHIFT_21_BUTTON, this));
    m_cashier_21shifts_button.SizeToContent();


    if (!theApp.pGbProManager)
        m_manager_reports_gb_pro_button.DestroyWindow();

    if (theApp.memory_settings_ini.masters_ip_address)
    {
        m_manager_netwin_button.DestroyWindow();
        m_manager_machines_button.DestroyWindow();
        m_cashier_ids_button.DestroyWindow();
        m_redeem_button.DestroyWindow();
        m_cashier_4shifts_button.DestroyWindow();
        m_cashier_14shifts_button.DestroyWindow();
        m_cashier_21shifts_button.DestroyWindow();
        tickets_button.DestroyWindow();
        irsw2g_button.DestroyWindow();
        m_redeem_button.DestroyWindow();

        CWnd* item;

        item = 0;
        item = GetDlgItem(IDC_MANAGER_REPORT_SHIFT_GROUPBOX);
        if (item)
            item->ShowWindow(SW_HIDE);
    }

   	if (!theApp.memory_settings_ini.force_cashier_shifts)
    {
        m_cashier_shift_groupbox.DestroyWindow();
        m_cashier_shift_current_button.DestroyWindow();
//        m_cashier_shift_all_button.DestroyWindow();
        m_cashier_shift_text.DestroyWindow();
        m_cashier_shift_combo_box.DestroyWindow();
    }
    else
    {
        file_index = 0;

        while (fileRead(LOCATION_IDS, file_index, &cashier_user, sizeof(cashier_user)))
        {
            if (cashier_user.user.id / 10000 == CASHIER)
            {
                edit_string.Format("%u", cashier_user.user.id);
                m_cashier_shift_combo_box.AddString(edit_string);
            }

            file_index++;
        }
    }

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CManagerReports::OnPaint() 
{
	CRect rect;
	GetClientRect(&rect);
	CPaintDC dc(this); // device context for painting

	CDialog::OnPaint();

    dc.FillSolidRect(&rect, 0x00000000);
}

void CManagerReports::OnBnClickedManagerReportsExitButton()
{
	DestroyWindow();
}

void CManagerReports::OnBnClickedManagerReportGbProButton()
{
    if (theApp.memory_settings_ini.activate_gb_advantage_support && theApp.pGbProManager)
    {
        if (pManagerGbProReports)
        {
            pManagerGbProReports->DestroyWindow();
            delete pManagerGbProReports;
        }

        pManagerGbProReports = new CManagerGbProReports;
        pManagerGbProReports->Create(IDD_MANAGER_GB_PRO_REPORTS_DIALOG);
    }
}
