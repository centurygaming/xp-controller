/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   07/15/09    Increased the number of shifts to print on the shift report from 4 to 21.
Robert Fuller   06/09/09    Added code for custom picture buttons.
Dave Elquist    07/15/05    Added GB Pro support.
Dave Elquist    06/09/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"
#include "ManagerGbProReports.h"
#include "FixedButton.h"


// CManagerReports dialog

class CManagerReports : public CDialog
{
	DECLARE_DYNAMIC(CManagerReports)

public:
	CManagerReports(CWnd* pParent = NULL);   // standard constructor
	virtual ~CManagerReports();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_MANAGER_REPORTS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnPaint();

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedManagerReportTicketsButton();
	afx_msg void OnBnClickedManagerReportDispenserButton();
	afx_msg void OnBnClickedManagerReportNetwinButton();
	afx_msg void OnBnClickedManagerReportAdjustmentsButton();
	afx_msg void OnBnClickedManagerReportW2gButton();
	afx_msg void OnBnClickedManagerReportMachinesButton();
	afx_msg void OnBnClickedManagerReportCashierIdsButton();
	afx_msg void OnBnClickedManagerReportRedeemButton();
	afx_msg void OnBnClickedManagerReportCashierShiftCurrentButton();
	afx_msg void OnBnClickedManagerReportCashier4ShiftsButton();
	afx_msg void OnBnClickedManagerReportCashier14ShiftsButton();
	afx_msg void OnBnClickedManagerReportCashier21ShiftsButton();
	afx_msg void OnBnClickedManagerReportsExitButton();
	afx_msg void OnBnClickedManagerReportGbProButton();

private:
	CFixedButton m_manager_netwin_button;
	CFixedButton m_manager_machines_button;
	CFixedButton m_cashier_ids_button;
	CFixedButton m_redeem_button;
	CFixedButton m_cashier_shift_all_button;
	CFixedButton tickets_button;
	CFixedButton dispenser_button;
	CFixedButton irsw2g_button;
	CFixedButton adjust_button;
	CFixedButton exit_button;
	CFixedButton m_cashier_4shifts_button;
	CFixedButton m_cashier_14shifts_button;
	CFixedButton m_cashier_21shifts_button;

	CButton m_cashier_shift_current_button;
	CButton m_manager_reports_gb_pro_button;

	CStatic m_cashier_shift_groupbox;
	CStatic m_cashier_shift_text;

	CComboBox m_cashier_shift_combo_box;

    CManagerGbProReports *pManagerGbProReports;
};
