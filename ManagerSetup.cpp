/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   07/27/09    Fixed the test game lock sound feature.
Robert Fuller   07/15/09    Increased the number of shifts to print on the shift report from 4 to 21.
Dave Elquist    07/15/05    Changed dispenser(lat) message from the Stratus.
Dave Elquist    06/09/05    Initial Revision.
*/


// ManagerSetup.cpp : implementation file
//

#include "stdafx.h"
#include "Mmsystem.h"
#include "xp_controller.h"
#include ".\managersetup.h"


// CManagerSetup dialog

IMPLEMENT_DYNAMIC(CManagerSetup, CDialog)
CManagerSetup::CManagerSetup(CWnd* pParent /*=NULL*/)
	: CDialog(CManagerSetup::IDD, pParent)
{
}

CManagerSetup::~CManagerSetup()
{
}

void CManagerSetup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MANAGER_FORCE_CASHIER_SHIFTS_CHECK, m_force_cashier_shifts_check);
	DDX_Control(pDX, IDC_MANAGER_PRINT_DRINK_COMPS_CHECK, m_print_drink_comps_check);
	DDX_Control(pDX, IDC_MANAGER_PLAYER_TIER_CHECK, m_print_player_tier_level_check);
	DDX_Control(pDX, IDC_MANAGER_SOUND_FOR_GAMELOCKS_TEST_BUTTON, m_sound_for_gamelock_test_button);
	DDX_Control(pDX, IDC_MANAGER_SETUP_CASHIER_SHIFT_PRINTING_CHECK, m_allow_cashier_to_print_shift_check);
	DDX_Control(pDX, IDC_MANAGER_PRINT_FULL_SHIFT_REPORT, m_print_full_shift_report);
    
}


BEGIN_MESSAGE_MAP(CManagerSetup, CDialog)
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_MANAGER_SOUND_FOR_GAMELOCKS_TEST_BUTTON, OnBnClickedManagerSoundForGamelocksTestButton)
	ON_BN_CLICKED(IDC_MANAGER_SETUP_EXIT_BUTTON, OnBnClickedManagerSetupExitButton)
	ON_BN_CLICKED(IDC_MANAGER_PLAYER_TIER_CHECK, OnBnClickedManagerPlayerTierCheck)
	ON_BN_CLICKED(IDC_MANAGER_PRINT_DRINK_COMPS_CHECK, OnBnClickedManagerPrintDrinkCompsCheck)
	ON_BN_CLICKED(IDC_MANAGER_FORCE_CASHIER_SHIFTS_CHECK, OnBnClickedManagerForceCashierShiftsCheck)
	ON_BN_CLICKED(IDC_MANAGER_SETUP_CASHIER_SHIFT_PRINTING_CHECK, OnBnClickedManagerSetupCashierShiftPrintingCheck)
	ON_BN_CLICKED(IDC_MANAGER_PRINT_FULL_SHIFT_REPORT, OnBnClickedManagerPrintFullShiftReportCheck)
END_MESSAGE_MAP()


// CManagerSetup message handlers

void CManagerSetup::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CManagerSetup::OnBnClickedManagerSoundForGamelocksTestButton()
{
//    theApp.sound_game_lock = 1;
    PlaySound(GAME_LOCK_SOUND, NULL, SND_FILENAME | SND_ASYNC);
}

void CManagerSetup::OnBnClickedManagerSetupExitButton()
{
	DestroyWindow();
}

void CManagerSetup::OnBnClickedManagerPlayerTierCheck()
{
    if (m_print_player_tier_level_check.GetCheck() == BST_CHECKED)
        theApp.memory_settings_ini.print_account_tier_level = 1;
    else if (m_print_player_tier_level_check.GetCheck() == BST_UNCHECKED)
        theApp.memory_settings_ini.print_account_tier_level = 0;

    fileWrite(MAIN_SETTINGS_INI, 0, &theApp.memory_settings_ini, sizeof(theApp.memory_settings_ini));
}

void CManagerSetup::OnBnClickedManagerPrintDrinkCompsCheck()
{
    if (m_print_drink_comps_check.GetCheck() == BST_CHECKED)
        theApp.memory_settings_ini.print_drink_comps = 1;
    else if (m_print_drink_comps_check.GetCheck() == BST_UNCHECKED)
        theApp.memory_settings_ini.print_drink_comps = 0;

    fileWrite(MAIN_SETTINGS_INI, 0, &theApp.memory_settings_ini, sizeof(theApp.memory_settings_ini));
}

void CManagerSetup::OnBnClickedManagerForceCashierShiftsCheck()
{
    if (m_force_cashier_shifts_check.GetCheck() == BST_CHECKED)
    {
        m_allow_cashier_to_print_shift_check.SetCheck(BST_CHECKED);
        theApp.memory_settings_ini.force_cashier_shifts = 1;
    }
    else if (m_force_cashier_shifts_check.GetCheck() == BST_UNCHECKED)
    {
        m_allow_cashier_to_print_shift_check.SetCheck(BST_UNCHECKED);
        theApp.memory_settings_ini.force_cashier_shifts = 0;
    }

    fileWrite(MAIN_SETTINGS_INI, 0, &theApp.memory_settings_ini, sizeof(theApp.memory_settings_ini));
}

BOOL CManagerSetup::OnInitDialog()
{
	CDialog::OnInitDialog();

    if (theApp.memory_settings_ini.masters_ip_address)
    {
        m_print_drink_comps_check.DestroyWindow();
        m_print_player_tier_level_check.DestroyWindow();
        m_sound_for_gamelock_test_button.DestroyWindow();
    }
    else
    {
        if (theApp.memory_settings_ini.print_account_tier_level)
            m_print_player_tier_level_check.SetCheck(BST_CHECKED);

        if (theApp.memory_settings_ini.print_drink_comps)
            m_print_drink_comps_check.SetCheck(BST_CHECKED);


        if (theApp.memory_settings_ini.print_full_shift_report)
            m_print_full_shift_report.SetCheck(BST_CHECKED);
    }

    if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CASH_DRAWER)
        m_force_cashier_shifts_check.DestroyWindow();
    else
    {
        if (theApp.memory_settings_ini.force_cashier_shifts)
        {
            m_force_cashier_shifts_check.SetCheck(BST_CHECKED);

            if (theApp.memory_settings_ini.force_cashier_shifts == 1)
                m_allow_cashier_to_print_shift_check.SetCheck(BST_CHECKED);
        }
    }

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CManagerSetup::OnPaint() 
{
	CRect rect;
	GetClientRect(&rect);
	CPaintDC dc(this); // device context for painting

	CDialog::OnPaint();

//    dc.FillSolidRect(&rect, 0x00000000);
}

void CManagerSetup::OnBnClickedManagerSetupCashierShiftPrintingCheck()
{
    m_force_cashier_shifts_check.SetCheck(BST_CHECKED);

    if (m_allow_cashier_to_print_shift_check.GetCheck() == BST_CHECKED)
        theApp.memory_settings_ini.force_cashier_shifts = 1;
    else if (m_allow_cashier_to_print_shift_check.GetCheck() == BST_UNCHECKED)
        theApp.memory_settings_ini.force_cashier_shifts = 2;

    fileWrite(MAIN_SETTINGS_INI, 0, &theApp.memory_settings_ini, sizeof(theApp.memory_settings_ini));
}

void CManagerSetup::OnBnClickedManagerPrintFullShiftReportCheck()
{
    if (m_print_full_shift_report.GetCheck() == BST_CHECKED)
        theApp.memory_settings_ini.print_full_shift_report = 1;
    else if (m_print_full_shift_report.GetCheck() == BST_UNCHECKED)
        theApp.memory_settings_ini.print_full_shift_report = 0;

    fileWrite(MAIN_SETTINGS_INI, 0, &theApp.memory_settings_ini, sizeof(theApp.memory_settings_ini));
}
