/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	12/04/13	Launch game tender in the main menu after being in the screen 30 seconds.
Robert Fuller   07/02/13    Added code to launch the old GB Trax application if there is no IDC and the GameTenderClient.exe file is not present on the controller hardware.
Robert Fuller   05/02/13    Don't display the Quick Enroll button if the XPC is not configured to communicate with the Stratus or the Quick Enroll application is not present on the XPC disk.
Robert Fuller   03/08/13    Removed obsolete Location Collection code.
Robert Fuller   03/08/12    Make sure the configurable IDC ip address and port are set before showing the Game Tender button on controller
Robert Fuller   09/17/12    Added quick enroll button to cashier menu.
Robert Fuller   06/01/12    Send Stratus message when starting or ending a cashier shift
Robert Fuller   10/03/11    Print the sas id address as opposed to the irrelevant mux id address for SAS direct connect games.
Robert Fuller   08/03/11    Added code to support Location Collection features.
Robert Fuller   04/26/10    Added additional customer ids to the configuration settings. Get netwin data for all customer ids when calculating shift report data.
Robert Fuller   09/03/09    Added software mods to support running the XP Controller in stand alone mode which allows locations to operate with basic features without connecting to the main frame.
Robert Fuller   07/15/09    Increased the number of shifts to print on the shift report from 4 to 21.
Robert Fuller   06/09/09    Added code for custom picture buttons.
Robert Fuller   05/15/09    Save drink comp info in the cashier shift data when ending a shift.
Robert Fuller   04/08/09    Change shift paradigm to only allow a single shift per login id to be active at a time.
Robert Fuller   01/27/09    Fill the game lock structure with the ticke id.
Robert Fuller   09/25/08    Send cashier to the logon screen after they end the shift.
Robert Fuller   09/25/08    Changed the way we keep track of the game comps for each cashiers shift.
Robert Fuller   09/22/08    Initialized the previous drop and handle shift variables by getting netwin data when a shift begins.
Robert Fuller   09/08/08    Added code to print the handle on the shift report.
Robert Fuller   01/22/08    Send the first/last name of who is logged into XP Controller when starting GB Trax.
Robert Fuller   12/11/07    Delete the right shift report file when printing shift reports.
Dave Elquist    07/15/05    Added Marker support. Added GB Pro support.
Dave Elquist    05/16/05    Initial Revision.
*/


// MenuCashier.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include ".\menucashier.h"
#include "TimerWindow.h"
#include "DrawerDenomCount.h"


#define TIMEOUT_TO_GAME_TENDER_MSECS 30 * 1000


// CMenuCashier dialog

IMPLEMENT_DYNAMIC(CMenuCashier, CDialog)
CMenuCashier::CMenuCashier(CWnd* pParent /*=NULL*/)
	: CDialog(CMenuCashier::IDD, pParent)
{
    pGameLockDialog         = NULL;
    pCashierReports         = NULL;
	pCashierGbPro           = NULL;

	VERIFY(m_pay_button.LoadBitmaps(_T("CPAYU"), _T("CPAYD")));
	VERIFY(m_end_shift_button.LoadBitmaps(_T("CENDSHIFTU"), _T("CENDSHIFTD")));
	VERIFY(m_cashier_vlora_button.LoadBitmaps(_T("CGBTRAXU"), _T("CGBTRAXD")));
	VERIFY(m_reports_button.LoadBitmaps(_T("CREPORTSU"), _T("CREPORTSD")));
	VERIFY(m_quick_enroll_button.LoadBitmaps(_T("CQUICKENROLLU"), _T("CQUICKENROLLD")));
	VERIFY(m_exit_button.LoadBitmaps(_T("CEXITU"), _T("CEXITD")));
	VERIFY(m_cashier_gametender_button.LoadBitmaps(_T("CGAMETENDERU"), _T("CGAMETENDERD")));
}

CMenuCashier::~CMenuCashier()
{
    if (pGameLockDialog)
    {
        pGameLockDialog->DestroyWindow();
        delete pGameLockDialog;
    }

    if (pCashierReports)
    {
        pCashierReports->DestroyWindow();
        delete pCashierReports;
    }

    if (pCashierGbPro)
    {
        pCashierGbPro->DestroyWindow();
        delete pCashierGbPro;
    }
}

void CMenuCashier::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CASHIER_OPEN_DRAWER_BUTTON, m_open_drawer_button);
}


BEGIN_MESSAGE_MAP(CMenuCashier, CDialog)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_EXIT_CASHIER_BUTTON, OnBnClickedExitCashierButton)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_CASHIER_PAY_BUTTON, OnBnClickedCashierPayButton)
	ON_BN_CLICKED(IDC_CASHIER_REPORTS_BUTTON, OnBnClickedCashierReportsButton)
	ON_BN_CLICKED(IDC_CASHIER_OPEN_DRAWER_BUTTON, OnBnClickedCashierOpenDrawerButton)
	ON_BN_CLICKED(IDC_CASHIER_SHIFT_END_BUTTON, OnBnClickedCashierShiftEndButton)
	ON_BN_CLICKED(IDC_CASHIER_GB_PRO_BUTTON, OnBnClickedCashierGbProButton)
	ON_BN_CLICKED(IDC_CASHIER_VLORA_BUTTON, OnBnClickedCashierVloraButton)
	ON_BN_CLICKED(IDC_CASHIER_QUICKENROLL_BUTTON, &CMenuCashier::OnBnClickedCashierQuickenrollButton)
	ON_BN_CLICKED(IDC_CASHIER_GAMETENDER_BUTTON, OnBnClickedCashierGameTenderButton)
END_MESSAGE_MAP()


void CMenuCashier::OnBnClickedExitCashierButton()
{
    theApp.current_dialog_return = IDD_MAIN_MENU_DIALOG;
    DestroyWindow();
}

void CMenuCashier::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

BOOL CMenuCashier::OnInitDialog()
{
	CDialog::OnInitDialog();

    VERIFY(m_pay_button.SubclassDlgItem(IDC_CASHIER_PAY_BUTTON, this));
    m_pay_button.SizeToContent();
    VERIFY(m_end_shift_button.SubclassDlgItem(IDC_CASHIER_SHIFT_END_BUTTON, this));
    m_end_shift_button.SizeToContent();
    VERIFY(m_cashier_vlora_button.SubclassDlgItem(IDC_CASHIER_VLORA_BUTTON, this));
    m_cashier_vlora_button.SizeToContent();
    VERIFY(m_reports_button.SubclassDlgItem(IDC_CASHIER_REPORTS_BUTTON, this));
    m_reports_button.SizeToContent();
    VERIFY(m_quick_enroll_button.SubclassDlgItem(IDC_CASHIER_QUICKENROLL_BUTTON, this));
    m_quick_enroll_button.SizeToContent();
    VERIFY(m_cashier_gametender_button.SubclassDlgItem(IDC_CASHIER_GAMETENDER_BUTTON, this));
    m_cashier_gametender_button.SizeToContent();



    VERIFY(m_exit_button.SubclassDlgItem(IDC_EXIT_CASHIER_BUTTON, this));
    m_exit_button.SizeToContent();

	if (theApp.current_user.user.id / 10000 != CASHIER || !theApp.memory_settings_ini.force_cashier_shifts)
        m_end_shift_button.DestroyWindow();

    switch (theApp.current_user.user.id / 10000)
    {
        case CASHIER:
        case ENGINEERING:
            break;

        default:
            m_pay_button.DestroyWindow();
            break;
    }

    if (theApp.memory_dispenser_config.dispenser_type != DISPENSER_TYPE_CASH_DRAWER)
        m_open_drawer_button.DestroyWindow();

    // only allow the master to end a shift
    if (theApp.memory_settings_ini.masters_ip_address)
        m_end_shift_button.DestroyWindow();

//	if (!theApp.memory_settings_ini.stratus_ip_address || !theApp.memory_settings_ini.stratus_tcp_port || (GetFileAttributes(VLORA_FILE) == 0xFFFFFFFF))
	if (!theApp.memory_settings_ini.idc_ip_address || !theApp.memory_settings_ini.idc_tcp_port || (GetFileAttributes(GAME_TENDER_FILE) == 0xFFFFFFFF))
    {
		m_cashier_gametender_button.DestroyWindow();

        if (GetFileAttributes(VLORA_FILE) == 0xFFFFFFFF)
		    m_cashier_vlora_button.DestroyWindow();
    }
    else
		m_cashier_vlora_button.DestroyWindow();


	if (!theApp.memory_settings_ini.stratus_ip_address || !theApp.memory_settings_ini.stratus_tcp_port || (GetFileAttributes(QUICK_ENROLL_APPLICATION) == 0xFFFFFFFF))
		m_quick_enroll_button.DestroyWindow();

   	if (theApp.memory_settings_ini.idc_ip_address && theApp.memory_settings_ini.idc_tcp_port && !(GetFileAttributes(GAME_TENDER_FILE) == 0xFFFFFFFF))
	    SetTimer(GAME_TENDER_TIMER, TIMEOUT_TO_GAME_TENDER_MSECS, 0);


	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CMenuCashier::OnPaint() 
{
	CRect rect;
	GetClientRect(&rect);
	CPaintDC dc(this); // device context for painting

	CDialog::OnPaint();

    dc.FillSolidRect(&rect, 0x00000000);
}

void CMenuCashier::OnBnClickedCashierPayButton()
{
 int message_length;
 BYTE number_of_game_locks = 0;
 time_t offline_timer, second_display;
 CTimerWindow *pTimerWindow = NULL;
 StratusMessageFormat stratus_msg;
 FileGameLock file_game_lock;
 HANDLE hSearch;
 CString search_string, filename;
 WIN32_FIND_DATA find_return;

    if (pGameLockDialog)
    {
        pGameLockDialog->DestroyWindow();
        delete pGameLockDialog;
        pGameLockDialog = NULL;
    }

    // Pay ticket through master controller using game lock
    if (theApp.memory_settings_ini.masters_ip_address)
    {
        // new request, clear queue
        while (!theApp.stratus_dialog_queue.empty())
            theApp.stratus_dialog_queue.pop();

        if (theApp.stratusSocket.socket_state)
            offline_timer = 0;
        else
        {
            memset(&stratus_msg, 0, sizeof(stratus_msg));
            stratus_msg.message_length = STRATUS_HEADER_SIZE;
            stratus_msg.send_code = SLAVE_TKT_GAME_LOCK_QUERY;
            stratus_msg.task_code = SLAVE_TKT_TASK_TICKET;
            stratus_msg.mux_id = CONTROLLER_MUX_ADDRESS;
            stratus_msg.ucmc_id = theApp.memory_dispenser_config.ucmc_id;
            stratus_msg.property_id = theApp.memory_location_config.property_id;
            stratus_msg.customer_id = theApp.memory_location_config.customer_id;
            stratus_msg.data_to_event_log = true;
            theApp.QueueStratusTxMessage(&stratus_msg, FALSE);

            pTimerWindow = new CTimerWindow;
            pTimerWindow->Create(IDD_TIMER_BAR_DIALOG);
            pTimerWindow->m_timer_window_bar.SetRange(0, OFFLINE_TIMER_LIMIT);
            pTimerWindow->SetDlgItemText(IDC_TIMER_WINDOW_TITLE_STATIC, "Getting Game Locks, Please Wait...");
            offline_timer = time(NULL) + OFFLINE_TIMER_LIMIT;
            second_display = 0;
            EnableWindow(false);
        }

        while (offline_timer > time(NULL) && theApp.stratus_dialog_queue.empty())
        {
            if (second_display != time(NULL))
            {
                pTimerWindow->m_timer_window_bar.SetPos((int)(time(NULL) + OFFLINE_TIMER_LIMIT - offline_timer));
                second_display = time(NULL);
            }
            else
                theApp.OnIdle(0);
        }

        if (pTimerWindow)
        {
            pTimerWindow->DestroyWindow();
            delete pTimerWindow;
            EnableWindow();
        }

        if (theApp.stratus_dialog_queue.empty())
        {
            theApp.printer_busy = true;
            theApp.PrinterHeader(0);
            theApp.PrinterId();
            theApp.PrinterData("\nThe Remote System Is Offline.\n");
            theApp.PrinterData("Unable To Get Game Locks.\n");
            theApp.PrinterData("Call For Service.\n\n");
            theApp.printer_busy = false;
            MessageWindow(false, "SYSTEM OFFLINE", "The Remote System is Offline.\nUnable To Get Game Locks.");
        }
        else
        {
            stratus_msg = theApp.stratus_dialog_queue.front();
            theApp.stratus_dialog_queue.pop();

            if (stratus_msg.task_code == SLAVE_TKT_TASK_TICKET && stratus_msg.send_code == SLAVE_TKT_GAME_LOCK_QUERY)
            {
                pGameLockDialog = new CGameLockDialog();
				memset(&pGameLockDialog->game_lock_list, 0, sizeof(pGameLockDialog->game_lock_list));
				message_length = sizeof(pGameLockDialog->game_lock_list[0]) * stratus_msg.data[0];

				if (message_length > sizeof(pGameLockDialog->game_lock_list))
					message_length = sizeof(pGameLockDialog->game_lock_list);

				memcpy(&pGameLockDialog->game_lock_list, &stratus_msg.data[1], message_length);
                pGameLockDialog->Create(IDD_GAMELOCK_DIALOG);
            }
        }
    }
    else
    {
        pGameLockDialog = new CGameLockDialog();
        memset(&pGameLockDialog->game_lock_list, 0, sizeof(pGameLockDialog->game_lock_list));

        search_string.Format("%s\\*", GAME_LOCKS_PATH);
        hSearch = FindFirstFile(search_string, &find_return);

        if (hSearch != INVALID_HANDLE_VALUE)
        {
            filename.Format("%s\\%s", GAME_LOCKS_PATH, find_return.cFileName);

            if (fileRead(filename.GetBuffer(0), 0, &file_game_lock, sizeof(file_game_lock)))
            {
                pGameLockDialog->game_lock_list[number_of_game_locks].ucmc_id = file_game_lock.game_lock.ucmc_id;
                pGameLockDialog->game_lock_list[number_of_game_locks].ticket_id = file_game_lock.game_lock.ticket_id;
                pGameLockDialog->game_lock_list[number_of_game_locks].ticket_amount = file_game_lock.game_lock.ticket_amount;
                pGameLockDialog->game_lock_list[number_of_game_locks].marker_balance = file_game_lock.game_lock.marker_balance;
                pGameLockDialog->game_lock_list[number_of_game_locks].game_port = file_game_lock.game_lock.game_port;
                pGameLockDialog->game_lock_list[number_of_game_locks].mux_id = file_game_lock.game_lock.mux_id;
                pGameLockDialog->game_lock_list[number_of_game_locks].sas_id = file_game_lock.game_lock.sas_id;
                pGameLockDialog->game_lock_list[number_of_game_locks].jackpot_to_credit_flag = file_game_lock.game_lock.jackpot_to_credit_flag;
                number_of_game_locks++;
            }

            while (number_of_game_locks < MAXIMUM_GAME_LOCKS_LIST && FindNextFile(hSearch, &find_return))
            {
                filename.Format("%s\\%s", GAME_LOCKS_PATH, find_return.cFileName);

                if (fileRead(filename.GetBuffer(0), 0, &file_game_lock, sizeof(file_game_lock)))
                {
                    pGameLockDialog->game_lock_list[number_of_game_locks].ucmc_id = file_game_lock.game_lock.ucmc_id;
                    pGameLockDialog->game_lock_list[number_of_game_locks].ticket_id = file_game_lock.game_lock.ticket_id;
                    pGameLockDialog->game_lock_list[number_of_game_locks].ticket_amount = file_game_lock.game_lock.ticket_amount;
                    pGameLockDialog->game_lock_list[number_of_game_locks].marker_balance = file_game_lock.game_lock.marker_balance;
                    pGameLockDialog->game_lock_list[number_of_game_locks].game_port = file_game_lock.game_lock.game_port;
                    pGameLockDialog->game_lock_list[number_of_game_locks].mux_id = file_game_lock.game_lock.mux_id;
                    pGameLockDialog->game_lock_list[number_of_game_locks].sas_id = file_game_lock.game_lock.sas_id;
                    pGameLockDialog->game_lock_list[number_of_game_locks].jackpot_to_credit_flag = file_game_lock.game_lock.jackpot_to_credit_flag;
                    number_of_game_locks++;
                }
            }

            FindClose(hSearch);
        }

        pGameLockDialog->Create(IDD_GAMELOCK_DIALOG);
    }
}

void CMenuCashier::OnBnClickedCashierReportsButton()
{
    if (pCashierReports)
    {
        pCashierReports->DestroyWindow();
        delete pCashierReports;
    }

    pCashierReports = new CCashierReports;
    pCashierReports->Create(IDD_CASHIER_REPORTS_DIALOG);
}

void CMenuCashier::OnBnClickedCashierOpenDrawerButton()
{
 FileCashierShift cashier_shift;

    if (fileRead(CASHIER_SHIFT_1, 0, &cashier_shift, sizeof(cashier_shift)))
    {
        if (cashier_shift.cashier_id != theApp.current_user.user.id)
            MessageWindow(false, "OPEN DRAWER", "PLEASE END SHIFT OR\nUSE CORRECT USER ID");
        else
        {
            cashier_shift.number_nosale++;
            fileWrite(CASHIER_SHIFT_1, 0, &cashier_shift, sizeof(cashier_shift));
            theApp.OpenCashDrawer();
        }
    }
}

void CMenuCashier::OnBnClickedCashierShiftEndButton()
{
 int denomination_count, calculate_ending_balance;
 DWORD netwin_drop;
 CString filename, filename2, message_string;
 FileCashierShift cashier_shift;
 FileShiftLog shift_log;
 DrinkCompTotal drink_comps;
 CDrawerDenomCount *pDrawerDenomCount;
 EventMessage event_message;

    if ((theApp.current_user.user.id / 10000 == CASHIER) && !theApp.memory_settings_ini.masters_ip_address)
    {
        filename = CASHIER_SHIFT_1;

        if (fileRead(filename.GetBuffer(0), 0, &cashier_shift, sizeof(cashier_shift)))
        {
            EnableWindow(FALSE);

            message_string.Format("ID %u END SHIFT?", theApp.current_user.user.id);
            MessageWindow(true, "END SHIFT", message_string);

            while (theApp.message_window_queue.empty())
                theApp.OnIdle(0);

            EnableWindow();

            if (theApp.message_window_queue.front() == IDOK)
            {
                memset(&event_message, 0, sizeof(event_message));
                event_message.head.time_stamp = time(NULL);
                event_message.head.event_type = SHIFT_END;
                event_message.head.data_length = 4;
                memcpy(event_message.data, &theApp.current_user.user.id, 4);
                theApp.event_message_queue.push(event_message);

                memset(&theApp.current_shift_user, 0, sizeof(theApp.current_shift_user));

                if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CASH_DRAWER)
                {
                    EnableWindow(FALSE);
                    while (!theApp.message_window_queue.empty())
                        theApp.message_window_queue.pop();

                    pDrawerDenomCount = new CDrawerDenomCount;
                    pDrawerDenomCount->Create(IDD_CASHIER_DRAWER_DENOM_DIALOG);

                    while (theApp.message_window_queue.empty())
                        theApp.OnIdle(0);

                    denomination_count = theApp.message_window_queue.front();
                    pDrawerDenomCount->DestroyWindow();
                    delete pDrawerDenomCount;

                    EnableWindow();

                    if (denomination_count == -1)
                    {
                        theApp.printer_busy = false;
                        return;
                    }
                    else
                        cashier_shift.ending_balance = denomination_count;

                    EnableWindow(FALSE);

                    message_string.Format("SAVE DENOMINATION COUNT?\n$9.2f", (double)cashier_shift.ending_balance / 100);
                    MessageWindow(true, "DRAWER DENOMINATION COUNT", message_string);

                    while (theApp.message_window_queue.empty())
                        theApp.OnIdle(0);

                    EnableWindow();

                    if (theApp.message_window_queue.front() != IDOK)
                    {
                        theApp.printer_busy = false;
                        return;
                    }

                    theApp.OpenCashDrawer();

                    calculate_ending_balance = cashier_shift.beginning_balance - cashier_shift.redeem_amount +
                        cashier_shift.active_shift_fills + cashier_shift.mgr_adjust_plus - cashier_shift.mgr_adjust_minus;

                    if (calculate_ending_balance != cashier_shift.ending_balance)
                    {
                        theApp.PrinterData("\nEND OF SHIFT AMOUNTS' CONFLICT\n");
                        message_string.Format("   Cashier Amount: %9.2f\n", (double)cashier_shift.ending_balance / 100);
                        theApp.PrinterData(message_string.GetBuffer(0));
                        message_string.Format(" Calcuated Amount: %9.2f\n", (double)calculate_ending_balance / 100);
                        theApp.PrinterData(message_string.GetBuffer(0));
                        message_string.Format("		 Difference: %9.2f\n\n", ((double)cashier_shift.ending_balance - (double)calculate_ending_balance) / 100);
                        theApp.PrinterData(message_string.GetBuffer(0));
                        theApp.PrinterData("REASON:_____________________________\n____________________________________\n"
                            "____________________________________\n____________________________________\n\n");
                    }
                }

                cashier_shift.end_time = time(NULL);

                if (theApp.memory_dispenser_config.dispenser_type != DISPENSER_TYPE_CASH_DRAWER)
                    cashier_shift.ending_balance = theApp.dispenserControl.memory_dispenser_values.dispense_grand_total -
                        theApp.dispenserControl.memory_dispenser_values.tickets_amount;

                if (!theApp.memory_settings_ini.masters_ip_address)
                {
                    memset(&shift_log, 0, sizeof(shift_log));
                    shift_log.cashier_id = cashier_shift.cashier_id;
                    shift_log.end_time = cashier_shift.end_time;
                    shift_log.redeem_amount = cashier_shift.redeem_amount;


//                    if (GetFileAttributes(MASTER_CONFIG) == 0xFFFFFFFF)
                    {
                        EnableWindow(false);
                        theApp.GetNetwinReport(false, theApp.memory_location_config.customer_id, true);
                        theApp.GetNetwinReport(false, theApp.memory_location_config.customer_id2, false);
                        theApp.GetNetwinReport(false, theApp.memory_location_config.customer_id3, false);
                        EnableWindow();
                        if (theApp.getTotalNetwinDrop() >= cashier_shift.previous_total_drop)
                            cashier_shift.game_drop = theApp.getTotalNetwinDrop() - cashier_shift.previous_total_drop;
                        else
                        {
                            cashier_shift.game_drop = theApp.getTotalNetwinDrop();
                            cashier_shift.previous_total_drop = 0;
                        }
                    }

                    if (theApp.getTotalNetwinHandle() >= cashier_shift.previous_game_handle)
                        cashier_shift.game_handle = theApp.getTotalNetwinHandle() - cashier_shift.previous_game_handle;
                    else
                    {
                        cashier_shift.game_handle = theApp.getTotalNetwinHandle();
                        cashier_shift.previous_game_handle = 0;
                    }

                    fileWrite(SHIFT_LOG, -1, &shift_log, sizeof(shift_log));

                }
                else
                {
                    netwin_drop = cashier_shift.previous_total_drop;
                    cashier_shift.game_drop = 0;
                }

                if (!fileRead(DRINK_COMPS_SHIFT, 0, &drink_comps, sizeof(drink_comps)))
                    memset(&drink_comps, 0, sizeof(drink_comps));

                cashier_shift.drink_comps.number = drink_comps.number;
                cashier_shift.drink_comps.total_amount = drink_comps.total_amount;

                // if we are offline when we end our shift print the disclaimer
                if (theApp.stratusSocket.socket_state)
                    cashier_shift.print_offline_disclaimer = TRUE;    


                fileWrite(filename.GetBuffer(0), 0, &cashier_shift, sizeof(cashier_shift));

                if (theApp.memory_settings_ini.force_cashier_shifts == 2)
                    theApp.PrintCashierShift(theApp.current_user.user.id, 1, true, true);
                else
                    theApp.PrintCashierShift(theApp.current_user.user.id, 1, false, true);

                // shuffle shifts
                DeleteFile(CASHIER_SHIFT_21);
                MoveFile(CASHIER_SHIFT_20, CASHIER_SHIFT_21);
                MoveFile(CASHIER_SHIFT_19, CASHIER_SHIFT_20);
                MoveFile(CASHIER_SHIFT_18, CASHIER_SHIFT_19);
                MoveFile(CASHIER_SHIFT_17, CASHIER_SHIFT_18);
                MoveFile(CASHIER_SHIFT_16, CASHIER_SHIFT_17);
                MoveFile(CASHIER_SHIFT_15, CASHIER_SHIFT_16);
                MoveFile(CASHIER_SHIFT_14, CASHIER_SHIFT_15);
                MoveFile(CASHIER_SHIFT_13, CASHIER_SHIFT_14);
                MoveFile(CASHIER_SHIFT_12, CASHIER_SHIFT_13);
                MoveFile(CASHIER_SHIFT_11, CASHIER_SHIFT_12);
                MoveFile(CASHIER_SHIFT_10, CASHIER_SHIFT_11);
                MoveFile(CASHIER_SHIFT_9, CASHIER_SHIFT_10);
                MoveFile(CASHIER_SHIFT_8, CASHIER_SHIFT_9);
                MoveFile(CASHIER_SHIFT_7, CASHIER_SHIFT_8);
                MoveFile(CASHIER_SHIFT_6, CASHIER_SHIFT_7);
                MoveFile(CASHIER_SHIFT_5, CASHIER_SHIFT_6);
                MoveFile(CASHIER_SHIFT_4, CASHIER_SHIFT_5);
                    MoveFile(CASHIER_SHIFT_3, CASHIER_SHIFT_4);
                    MoveFile(CASHIER_SHIFT_2, CASHIER_SHIFT_3);
                    MoveFile(CASHIER_SHIFT_1, CASHIER_SHIFT_2);

                DeleteFile(SHIFT_TICKETS_21);
                MoveFile(SHIFT_TICKETS_20, SHIFT_TICKETS_21);
                MoveFile(SHIFT_TICKETS_19, SHIFT_TICKETS_20);
                MoveFile(SHIFT_TICKETS_18, SHIFT_TICKETS_19);
                MoveFile(SHIFT_TICKETS_17, SHIFT_TICKETS_18);
                MoveFile(SHIFT_TICKETS_16, SHIFT_TICKETS_17);
                MoveFile(SHIFT_TICKETS_15, SHIFT_TICKETS_16);
                MoveFile(SHIFT_TICKETS_14, SHIFT_TICKETS_15);
                MoveFile(SHIFT_TICKETS_13, SHIFT_TICKETS_14);
                MoveFile(SHIFT_TICKETS_12, SHIFT_TICKETS_13);
                MoveFile(SHIFT_TICKETS_11, SHIFT_TICKETS_12);
                MoveFile(SHIFT_TICKETS_10, SHIFT_TICKETS_11);
                MoveFile(SHIFT_TICKETS_9, SHIFT_TICKETS_10);
                MoveFile(SHIFT_TICKETS_8, SHIFT_TICKETS_9);
                MoveFile(SHIFT_TICKETS_7, SHIFT_TICKETS_8);
                MoveFile(SHIFT_TICKETS_6, SHIFT_TICKETS_7);
                MoveFile(SHIFT_TICKETS_5, SHIFT_TICKETS_6);
                MoveFile(SHIFT_TICKETS_4, SHIFT_TICKETS_5);
                MoveFile(SHIFT_TICKETS_3, SHIFT_TICKETS_4);
                MoveFile(SHIFT_TICKETS_2, SHIFT_TICKETS_3);
                MoveFile(SHIFT_TICKETS_1, SHIFT_TICKETS_2);

                theApp.printer_busy = false;
                theApp.current_user.time_setting = 0;
            }

        }
        else
            // zero out the current shift user
            memset(&theApp.current_shift_user, 0, sizeof(theApp.current_shift_user));

        theApp.current_dialog_return = IDD_LOGON_DIALOG;
        DestroyWindow();

    }
}

void CMenuCashier::OnBnClickedCashierGbProButton()
{
	if (pCashierGbPro)
    {
        pCashierGbPro->DestroyWindow();
        delete pCashierGbPro;
    }

    pCashierGbPro = new CCashierGbPro;
    pCashierGbPro->Create(IDD_CASHIER_GB_PRO_DIALOG);
}

void CMenuCashier::OnBnClickedCashierVloraButton()
{
    char message_buffer[100];

    memset(message_buffer, 0, sizeof(message_buffer));

    sprintf_s(message_buffer, sizeof(message_buffer), """%s %s""", theApp.current_user.user.first_name, theApp.current_user.user.last_name);

//	ShellExecute(NULL, "open", VLORA_FILE, " -m", VLORA_PATH, SW_SHOWMAXIMIZED);
	ShellExecute(NULL, "open", VLORA_FILE, message_buffer, VLORA_PATH, SW_SHOWMAXIMIZED);
}

void CMenuCashier::OnBnClickedCashierGameTenderButton()
{
    char message_buffer[100];

    memset(message_buffer, 0, sizeof(message_buffer));

    sprintf_s(message_buffer, sizeof(message_buffer), """%s %s""", theApp.current_user.user.first_name, theApp.current_user.user.last_name);

//	ShellExecute(NULL, "open", VLORA_FILE, " -m", VLORA_PATH, SW_SHOWMAXIMIZED);
//	ShellExecute(NULL, "open", VLORA_FILE, message_buffer, VLORA_PATH, SW_SHOWMAXIMIZED);
	ShellExecute(NULL, "open", GAME_TENDER_FILE, NULL, NULL, SW_SHOWMAXIMIZED);
}

void CMenuCashier::OnBnClickedCashierQuickenrollButton()
{
	ShellExecute(NULL, "open", QUICK_ENROLL_APPLICATION, NULL, QUICK_ENROLL_APPLICATION_PATH, SW_SHOWMAXIMIZED);
}

void CMenuCashier::OnTimer(UINT nIDEvent)
{
    switch (nIDEvent)
    {
		case GAME_TENDER_TIMER:
			KillTimer(GAME_TENDER_TIMER);
        	if (theApp.memory_settings_ini.idc_ip_address && theApp.memory_settings_ini.idc_tcp_port && !(GetFileAttributes(GAME_TENDER_FILE) == 0xFFFFFFFF))
                if (!theApp.IdcSocket.socket_state || (theApp.IdcSocket.socket_state == WSAEISCONN))
        			ShellExecute(NULL, "open", GAME_TENDER_FILE, NULL, NULL, SW_SHOWMAXIMIZED);
			break;
    }

	CDialog::OnTimer(nIDEvent);
}
