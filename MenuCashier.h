/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   07/02/13    Added code to launch the old GB Trax application if there is no IDC and the GameTenderClient.exe file is not present on the controller hardware.
Robert Fuller   09/17/12    Added quick enroll button to cashier menu.
Robert Fuller   06/09/09    Added code for custom picture buttons.
Dave Elquist    07/15/05    Added Marker support. Added GB Pro support.
Dave Elquist    05/16/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"
#include "GameLockDialog.h"
#include "CashierReports.h"
#include "CashierGbPro.h"
#include "FixedButton.h"


// CMenuCashier dialog

class CMenuCashier : public CDialog
{
	DECLARE_DYNAMIC(CMenuCashier)

public:
	CMenuCashier(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMenuCashier();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_CASHIER_MENU_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nIDEvent);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedExitCashierButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedCashierPayButton();
	afx_msg void OnBnClickedCashierReportsButton();
	afx_msg void OnBnClickedCashierOpenDrawerButton();
	afx_msg void OnBnClickedCashierShiftEndButton();
	afx_msg void OnBnClickedCashierGbProButton();

// private variables
private:
	CButton m_open_drawer_button;
	CFixedButton m_end_shift_button;
	CFixedButton m_pay_button;
	CFixedButton m_quick_enroll_button;
	CFixedButton m_cashier_vlora_button;
	CFixedButton m_cashier_gametender_button;
	CFixedButton m_reports_button;
	CFixedButton m_exit_button;

    CGameLockDialog *pGameLockDialog;
    CCashierReports *pCashierReports;
	CCashierGbPro *pCashierGbPro;
public:
	afx_msg void OnBnClickedCashierVloraButton();
	afx_msg void OnBnClickedCashierGameTenderButton();
	afx_msg void OnBnClickedCashierQuickenrollButton();
};
