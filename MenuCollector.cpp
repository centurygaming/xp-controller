/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	12/04/13	Run reports and clear out the paid ticket buffer when an external fill is complete.
Robert Fuller	07/03/13	Save important Stratus TX messages to a file on disk and do not purge unless Stratus returns an ack.
Robert Fuller	05/10/13	Changed the endian on the ticket fill message sent to the Stratus.
Robert Fuller   03/08/13    Removed obsolete Location Collection code.
Robert Fuller	09/17/12	Added code to fill the ucmc id field in the dispense message we send to the IDC for accounting purposes.
Robert Fuller   09/17/12    Send Fill values to IDC when there is a fill done on the IDC.
Robert Fuller   08/03/11    Do NOT autologout the collector when we are in the middle of a fill.
Robert Fuller   08/03/11    Added code to support Location Collection features.
Robert Fuller   06/28/11    Allow manager to access the dispenser functions in the Collection screen.
Robert Fuller   06/08/11    Added mods to support GB with the XP/SAS direct connect feature.
Robert Fuller   04/26/11    Added the 'do not print dispense info' flag and functionality.
Robert Fuller   08/09/10    Added mods to support the XP/SAS direct connect feature.
Robert Fuller   05/20/10    Print a "DM" on the ticket fill and drop reports when a dispenser malfunction is detected during a payout.
Robert Fuller   01/19/10    Implemented a file redundancy strategy for files which store the paid game locks, dispenses, and drop tickets.
Robert Fuller   12/30/09    Print the customer id on the TKT FILL LOG for accounting dept. convenience.
Robert Fuller   10/21/09    Print the dispenser report above the paid tickets report when doing a fill.
Robert Fuller   09/03/09    Added software mods to support running the XP Controller in stand alone mode which allows locations to operate with basic features without connecting to the main frame.
Robert Fuller   06/09/09    Added code to make printing bag and tag info optional.
Robert Fuller   04/08/09    Make sure that we do drops offline.
Robert Fuller   11/11/08    Removed reference to GB Pro manager awarded points and vouchers.
Robert Fuller   08/19/08    Added code to support the GB Advantage feature.
Robert Fuller	07/08/08	Made adjustment to the drop meters to compensate for the wat in being included in the drop value.
Robert Fuller	03/25/08	Added routines to get and put the mux id in the bag and tag structure and sort by mux id.
Robert Fuller	01/31/08	Added total drop reconcile section to the bag and tag printout.
Robert Fuller	01/11/08	Added bag and tag modification routine.
Robert Fuller   09/05/07    Added newline on printer report to fix printing problem.
Dave Elquist    07/15/05    Added GB Pro support.
Dave Elquist    05/16/05    Initial Revision.
*/

#define _CRT_RAND_S

#include <stdlib.h>

// MenuCollector.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include ".\menucollector.h"


// CMenuCollector dialog

IMPLEMENT_DYNAMIC(CMenuCollector, CDialog)
CMenuCollector::CMenuCollector(CWnd* pParent /*=NULL*/)
	: CDialog(CMenuCollector::IDD, pParent)
{
    pCollectorImpressSet = NULL;
}

CMenuCollector::CMenuCollector(bool manager_entry, CWnd* pParent /*=NULL*/)
	: CDialog(CMenuCollector::IDD, pParent)
{
    pCollectorImpressSet = NULL;
}

CMenuCollector::~CMenuCollector()
{
    if (pCollectorImpressSet)
        delete pCollectorImpressSet;
}

void CMenuCollector::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COLLECTOR_GAME_DROP_BUTTON, m_collector_game_drop_button);
	DDX_Control(pDX, IDC_FORCE_COLLECTION_BUTTON, m_collector_force_collection_button);
}


BEGIN_MESSAGE_MAP(CMenuCollector, CDialog)
	ON_BN_CLICKED(IDC_EXIT_COLLECTOR_BUTTON, OnBnClickedExitCollectorButton)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_COLLECTOR_DISPENSER_FILL_BUTTON, OnBnClickedCollectorDispenserFillButton)
	ON_BN_CLICKED(IDC_COLLECTOR_IMPRESS_BUTTON, OnBnClickedCollectorImpressButton)
	ON_BN_CLICKED(IDC_COLLECTOR_GAME_DROP_BUTTON, OnBnClickedCollectorGameDropButton)
//	ON_WM_ENTERIDLE()
ON_BN_CLICKED(IDC_COLLECTOR_REBOOT_BUTTON, OnBnClickedCollectorRebootButton)
ON_BN_CLICKED(IDC_DISPENSER_CONTROL_BUTTON, OnBnClickedDispenserControlButton)
ON_BN_CLICKED(IDC_FORCE_COLLECTION_BUTTON, OnBnClickedForceCollectionButton)
ON_BN_CLICKED(IDC_COLLECTOR_RESTART_APP_BUTTON, &CMenuCollector::OnBnClickedCollectorRestartAppButton)
END_MESSAGE_MAP()


void CMenuCollector::OnBnClickedExitCollectorButton()
{
    theApp.current_dialog_return = IDD_MAIN_MENU_DIALOG;
    DestroyWindow();
}

void CMenuCollector::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

// Function Description: DISPENSER TICKET FILL
void CMenuCollector::OnBnClickedCollectorDispenserFillButton()
{
 time_t time_stamp;
 DWORD file_index, breakage, impress_amount1, impress_amount2, total_redeem, total_tickets;
 CString prt_buf;
 FileDropTicket file_drop_ticket;
 FilePaidTicket file_paid_ticket;
 FileCashierShift file_cashier_shift;
 FillValues fill_values;
 StratusMessageFormat stratus_msg;
 int i;

    EnableWindow(FALSE);

//    theApp.fill_in_progress = TRUE;

    if (theApp.memory_dispenser_config.dispenser_type == DISPENSER_TYPE_CASH_DRAWER)
        MessageWindow(true, "FILL DRAWER", "DRAWER TICKET FILL?");
    else
        MessageWindow(true, "DISPENSER FILL", "DISPENSER TICKET FILL?");

    while (theApp.message_window_queue.empty())
        theApp.OnIdle(0);

    EnableWindow();

    if (theApp.message_window_queue.front() == IDOK)
    {
        theApp.fillDispenser();
    }

	if (!theApp.message_window_queue.empty())
    	theApp.message_window_queue.pop();

//    theApp.fill_in_progress = FALSE;

}

void CMenuCollector::OnBnClickedCollectorImpressButton()
{
    if (pCollectorImpressSet)
        delete pCollectorImpressSet;

    pCollectorImpressSet = new CCollectorImpressSet;
    pCollectorImpressSet->Create(IDD_COLLECTOR_IMPRESS_DIALOG);
}

// Function Description: DROP START PROCESS
void CMenuCollector::OnBnClickedCollectorGameDropButton()
{
 DWORD file_index;
 CString prt_buf, game_redeem_records_string;
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl;
 FillValues fill_values;
 FileGameRedeemRecord redeem_record;
 StratusMessageFormat stratus_msg;
 FileShiftLog shift_log;

    EnableWindow(FALSE);


    if ((theApp.memory_settings_ini.stratus_ip_address && theApp.memory_settings_ini.stratus_tcp_port) && theApp.stratusSocket.socket_state)
    {
        theApp.printer_busy = true;
        theApp.PrinterHeader(0);
        theApp.PrinterId();
        theApp.PrinterData("\nThe Remote System Is Offline.\n");
        theApp.PrinterData("Unable To Run Game Drop.\n");
        theApp.PrinterData("Call For Service.\n\n");
        theApp.printer_busy = false;
        MessageWindow(true, "SYSTEM OFFLINE", "The Remote System is Offline.\nUnable To Run Game Drop.");

        while (theApp.message_window_queue.empty())
            theApp.OnIdle(0);

        EnableWindow();
    }
    else
    {
        MessageWindow(true, "GAME DROP", "START DROP PROCESS?");

        while (theApp.message_window_queue.empty())
            theApp.OnIdle(0);

        EnableWindow();

        if (theApp.message_window_queue.front() == IDOK)
        {
            theApp.GetCustomerIdSelection(0);

            pGameCommControl = theApp.pHeadGameCommControl;

            while (pGameCommControl)
            {
                pGameDevice = pGameCommControl->pHeadGameDevice;

                while (pGameDevice)
                {
                    if (pGameDevice->memory_game_config.customer_id == theApp.get_customer_id_selection)
                    {
                        if (!(pGameDevice->memory_game_config.mux_id && theApp.GetSasGamePointer(pGameDevice->memory_game_config.ucmc_id)))
                        {
                            pGameDevice->SetGameCollectionState(START_COLLECTION_STATE);
                            if (pGameDevice->memory_game_config.sas_id)
                                pGameDevice->SetGameCollectionState(ARMED_COLLECTION_STATE);

                            if (!pGameDevice->memory_game_config.mux_id && pGameCommControl->sas_com)
                                pGameCommControl->sas_com->non_volatile_ram.game_status |= COLLECT_APPROVED;
                        }
                    }

                    pGameDevice= pGameDevice->pNextGame;
                }

                pGameCommControl = pGameCommControl->pNextGameCommControl;
            }

            theApp.printer_busy = true;
            theApp.GameRedeemReport(theApp.get_customer_id_selection);
            theApp.CashAdjustmentReport();
            DeleteFile(ADJUSTMENT_RECORDS);

            memset(&fill_values, 0, sizeof(fill_values));
            fill_values.type = 'D';
            file_index = 0;

            game_redeem_records_string = GAME_REDEEM_RECORDS;

            while (fileRead(game_redeem_records_string.GetBuffer(0), file_index, &redeem_record, sizeof(redeem_record)))
            {
                if (!redeem_record.customer_id || redeem_record.customer_id == theApp.get_customer_id_selection)
                {
                    fill_values.drop_amount += redeem_record.amount;
                    redeem_record.amount = 0;
                    writeRedundantFile(game_redeem_records_string, file_index, (BYTE*)&redeem_record, sizeof(redeem_record));
                }
                file_index++;
            }

            fill_values.impress_bill_amount_100 = theApp.dispenserControl.memory_dispenser_values.impress_amount1;
            fill_values.impress_bill_amount_20 = theApp.dispenserControl.memory_dispenser_values.impress_amount2;

            // generate a transaction_id
            rand_s((unsigned int*)&fill_values.transaction_id);

            theApp.SendIdcFillValuesMessage(&fill_values);

            endian4(fill_values.drop_amount);
            endian4(fill_values.impress_bill_amount_100);
            endian4(fill_values.impress_bill_amount_20);
            endian4(fill_values.transaction_id);
            fill_values.message_revision = 'A';

            memset(&stratus_msg, 0, sizeof(stratus_msg));
            stratus_msg.send_code = TKT_FILL_MESSAGE;
            stratus_msg.task_code = TKT_TASK_TICKET;
            stratus_msg.mux_id = CONTROLLER_MUX_ADDRESS;
            stratus_msg.ucmc_id = theApp.memory_dispenser_config.ucmc_id;
            stratus_msg.message_length = sizeof(fill_values) + STRATUS_HEADER_SIZE;
            stratus_msg.property_id = theApp.memory_location_config.property_id;
            stratus_msg.club_code = theApp.memory_location_config.club_code;
            stratus_msg.serial_port = 1;
            stratus_msg.customer_id = theApp.get_customer_id_selection;
            stratus_msg.data_to_event_log = true;
            memcpy(stratus_msg.data, &fill_values, sizeof(fill_values));
            theApp.QueueStratusTxMessage(&stratus_msg, TRUE);

            theApp.PrintW2gTickets(theApp.get_customer_id_selection);
            theApp.DropTicketsPrint(theApp.get_customer_id_selection);

            if (theApp.memory_settings_ini.force_cashier_shifts)
            {
                theApp.PrinterData("\n");
                theApp.PrinterHeader(0);
//              theApp.PrinterData(" SHIFT LOG\n\nCASHIER  DATE        TIME     REDEEMED\n--------------------------------------\n");
//                theApp.PrinterData(" SHIFT LOG\n\nCASHIER  DATE        TIME     PAID OUT\n--------------------------------------\n");
                theApp.PrinterData(" SHIFT LOG\n\nCASHIER  DATE        TIME     TKT PAID\n--------------------------------------\n");

                file_index = 0;

                while (fileRead(SHIFT_LOG, file_index, &shift_log, sizeof(shift_log)))
                {
                    prt_buf.Format("%lu    %s %7.2f\n", shift_log.cashier_id, convertTimeStamp(shift_log.end_time),
                        (double)shift_log.redeem_amount / 100);
                    theApp.PrinterData(prt_buf.GetBuffer(0));
                    file_index++;
                }

                if (!file_index)
                    theApp.PrinterData(" NO SHIFT RECORDS\n");
            }

            DeleteFile(SHIFT_LOG);
            EnableWindow(false);
            theApp.GetNetwinReport(true, theApp.get_customer_id_selection, true);
            EnableWindow();

//            if (theApp.memory_settings_ini.print_bag_and_tag)
//                printBagAndTagInfo();

            // zero out the game meter data
            pGameCommControl = theApp.pHeadGameCommControl;
            while (pGameCommControl)
            {
                pGameDevice = pGameCommControl->pHeadGameDevice;

                while (pGameDevice)
                {
                    if (pGameDevice->memory_game_config.customer_id == theApp.get_customer_id_selection)
                    {
                        pGameDevice->SaveGameMeterDropSnapshot();
                    }

                    pGameDevice= pGameDevice->pNextGame;
                }

                pGameCommControl = pGameCommControl->pNextGameCommControl;
            }

            DeleteFile(GB_PRO_MANAGER_POINTS_FILE);
            DeleteFile(GB_PRO_MANAGER_TICKETS_FILE);

            theApp.PrinterData(" END OF REPORT\n\n");
            theApp.printer_busy = false;
        }

        theApp.message_window_queue.pop();
    }
}

void CMenuCollector::getBagAndTagMuxIds ()
{
	int i;
    CGameDevice* game_device;

	for (i=0; i < theApp.total_netwin_games; i++)
	{
        game_device = theApp.GetGamePointer(theApp.bag_and_tag_info[i].ucmc_id);

        if (game_device != NULL)
            theApp.bag_and_tag_info[i].mux_id = game_device->memory_game_config.mux_id;
        else
            theApp.bag_and_tag_info[i].mux_id = 0;
	}
}

int compareBagAndTagInfo (const void * b_and_t1, const void * b_and_t2);


void sortBagAndTagsByMuxId ()
{
	qsort( (void *)theApp.bag_and_tag_info, theApp.total_netwin_games, sizeof(BagAndTagInfo), compareBagAndTagInfo);
}

int compareBagAndTagInfo (const void * b_and_t1, const void * b_and_t2)
{
    BagAndTagInfo* bag_and_tag1 = (BagAndTagInfo*) b_and_t1;
    BagAndTagInfo* bag_and_tag2 = (BagAndTagInfo*) b_and_t2;


	if (bag_and_tag1->mux_id < bag_and_tag2->mux_id)
		return -1;
	if (bag_and_tag1->mux_id > bag_and_tag2->mux_id)
		return 1;
	return 0;
}

void CMenuCollector::printBagAndTagInfo()
{
	int i;
    CString prt_buf;
    double cash_drop = 0;

    getBagAndTagMuxIds();
	sortBagAndTagsByMuxId();

	theApp.PrinterData(" \n\n");
	theApp.PrinterData(" \n_________________________________________\n");
    theApp.PrinterTitle("BAG AND TAG");
    theApp.PrinterId();
    prt_buf.Format("\nTOTAL CASH DROP: %9.2f\n", (double)theApp.total_cash_drop / 100);
    theApp.PrinterData(prt_buf.GetBuffer(0));
	theApp.PrinterData(" \nTOTAL HANDHELD DROP _________\n");
	theApp.PrinterData(" \nDIFFERENCE  _________________\n");
	theApp.PrinterData(" \nRESOLUTION  _________________\n");
	theApp.PrinterData(" \n_____________________________\n");
	theApp.PrinterData(" \n_____________________________\n");
	theApp.PrinterData(" \nINITIALS    _________________\n");
	theApp.PrinterData(" \n_________________________________________\n");

	for (i=0; i < theApp.total_netwin_games; i++)
	{
        theApp.PrinterTitle("BAG AND TAG");
        theApp.PrinterId();

        prt_buf.Format("\nGAME #: %ld\n", theApp.bag_and_tag_info[i].ucmc_id);
        theApp.PrinterData(prt_buf.GetBuffer(0));

        prt_buf.Format("\nMUX ID: %ld\n", theApp.bag_and_tag_info[i].mux_id);
        theApp.PrinterData(prt_buf.GetBuffer(0));

        // cash drop
        if (theApp.bag_and_tag_info[i].game_drop >= theApp.bag_and_tag_info[i].wat_in)
            cash_drop = (double)theApp.bag_and_tag_info[i].game_drop - (double)theApp.bag_and_tag_info[i].wat_in;
        else
            cash_drop = 0;

        prt_buf.Format("\nLAT DROP: %9.2f\n", (double)cash_drop / 100);
        theApp.PrinterData(prt_buf.GetBuffer(0));

		theApp.PrinterData(" \nSOFT METER _________________\n");
		theApp.PrinterData(" \nCOLLECTOR INITIALS _________\n");
		theApp.PrinterData(" \nTO BE COMPLETED BY SOFT COUNT TEAM\n");
		theApp.PrinterData(" \nDROP COUNTED  _______________\n");
		theApp.PrinterData(" \nDROP VARIANCE _______________\n");
		theApp.PrinterData(" \nINITIALS      _______________\n");
		theApp.PrinterData(" \n_________________________________________\n");
	}

}

BOOL CMenuCollector::OnInitDialog()
{
	CDialog::OnInitDialog();

    if (theApp.memory_settings_ini.masters_ip_address || theApp.current_shift_user.user.id)
	{
        m_collector_game_drop_button.DestroyWindow();
		m_collector_force_collection_button.DestroyWindow();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CMenuCollector::OnBnClickedCollectorRebootButton()
{
	EnableWindow(FALSE);

	MessageWindow(true, "REBOOT?", "REBOOT SYSTEM NOW?");

	while (theApp.message_window_queue.empty())
		theApp.OnIdle(0);

	EnableWindow();

	if (theApp.message_window_queue.front() == IDOK)
	{
		DestroyWindow();
		theApp.ProcessExitProgramRequest(true);
	}
}

void CMenuCollector::OnBnClickedDispenserControlButton()
{
	if (theApp.pDispenserDialog)
		delete theApp.pDispenserDialog;

	theApp.pDispenserDialog = new CDispenserDialog;
	theApp.pDispenserDialog->Create(IDD_DISPENSER_INFO_DIALOG);
}

void CMenuCollector::OnBnClickedForceCollectionButton()
{
    if ((theApp.memory_settings_ini.stratus_ip_address && theApp.memory_settings_ini.stratus_tcp_port) && theApp.stratusSocket.socket_state)
    {
        theApp.printer_busy = true;
        theApp.PrinterHeader(0);
        theApp.PrinterId();
        theApp.PrinterData("\nThe Remote System Is Offline.\n");
        theApp.PrinterData("Unable To Run Game Drop.\n");
        theApp.PrinterData("Call For Service.\n\n");
        theApp.printer_busy = false;
        MessageWindow(true, "SYSTEM OFFLINE", "The Remote System is Offline.\nUnable To Run Game Drop.");

        while (theApp.message_window_queue.empty())
            theApp.OnIdle(0);

        EnableWindow();
    }
    else
    {
	    if (theApp.pGameCollectionState)
		    delete theApp.pGameCollectionState;

	    theApp.pGameCollectionState = new CGameCollectionState();
	    theApp.pGameCollectionState->Create(IDD_FORCE_COLLECTION_DIALOG);
    }
}

void CMenuCollector::OnBnClickedCollectorRestartAppButton()
{
	theApp.ProcessExitProgramRequest(false);
//	ShellExecute(NULL, "open", "controller.bat", NULL, NULL, SW_SHOWMAXIMIZED);
}
