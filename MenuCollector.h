/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	12/04/13	Run reports and clear out the paid ticket buffer when an external fill is complete.
Robert Fuller   03/08/13    Removed obsolete Location Collection code.
Robert Fuller   06/28/11    Allow manager to access the dispenser functions in the Collection screen.
Robert Fuller	03/25/08	Added routine to get and put the mux id in the bag and tag structure.
Robert Fuller	01/11/08	Added bag and tag modification routine.
Dave Elquist    07/15/05    Changed resource id tag.
Dave Elquist    05/16/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"
#include "CollectorImpressSet.h"


#define MAX_GAMES 300


// CMenuCollector dialog

class CMenuCollector : public CDialog
{
	DECLARE_DYNAMIC(CMenuCollector)

public:
	CMenuCollector(CWnd* pParent = NULL);   // standard constructor
	CMenuCollector(bool manager_entry, CWnd* pParent = NULL);   // custom constructor
	virtual ~CMenuCollector();

// Dialog Data
	enum { IDD = IDD_COLLECTOR_MENU_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedExitCollectorButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedCollectorDispenserFillButton();
	afx_msg void OnBnClickedCollectorImpressButton();
	afx_msg void OnBnClickedCollectorGameDropButton();
	afx_msg void OnBnClickedCollectorRebootButton();
	afx_msg void OnBnClickedDispenserControlButton();
	afx_msg void OnBnClickedForceCollectionButton();
	virtual BOOL OnInitDialog();

// public variables
public:
	CButton m_collector_game_drop_button;
	CButton m_collector_force_collection_button;

// private variables
private:
    CCollectorImpressSet	*pCollectorImpressSet;


	afx_msg void OnBnClickedCollectorRestartAppButton();

	void printBagAndTagInfo();
	void getBagAndTagMuxIds ();
};
