/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    07/15/05    Changed dialog layout.
Dave Elquist    05/16/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"


// CMenuEngineer dialog

class CMenuEngineer : public CDialog
{
	DECLARE_DYNAMIC(CMenuEngineer)

public:
	CMenuEngineer(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMenuEngineer();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_ENGINEER_MENU_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

// private variables
private:
    DWORD current_edit_focus;

public:
	afx_msg void OnBnClickedExitEngineerButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedEngrControlPanelButton();
	afx_msg void OnBnClickedEngrKeyboardButton();
	afx_msg void OnBnClickedEngrWindowsExplorerButton();
	afx_msg void OnBnClickedEngrTaskManagerButton();
};
