/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   03/08/13    Added Manual Dispense mechanism in the Technician menu.
Robert Fuller   04/16/09    Removed code associated with PRAC interface.
Robert Fuller   01/29/09    Added code to support the new GB Advantage technician screen.
Dave Elquist    07/15/05    Added GB Pro support. Added master/slave support.
Dave Elquist    05/16/05    Initial Revision.
*/


#pragma once


#include "TechnicianSystemUsers.h"
#include "TechnicianDispense.h"
#include "TechnicianDisplayGameInfoDialog.h"
#include "TechnicianGbAdvantageInfo.h"
#include "TechnicianMasterSlaveDialog.h"
#include "EmailSettings.h"
#include "NetworkSetup.h"
#include "CashVoucherDialog.h"
#include "GbProAsciiMsg.h"
#include "afxwin.h"


// CMenuTechnician dialog

class CMenuTechnician : public CDialog
{
	DECLARE_DYNAMIC(CMenuTechnician)

public:
	CMenuTechnician(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMenuTechnician();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_TECHNICIAN_MENU_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedExitTechnicianButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedTechnicianMenuUsersButton();
	afx_msg void OnBnClickedTechnicianMenuGameDataButton();
	afx_msg void OnBnClickedTechnicianDisplayGameInfoButton();
	afx_msg void OnBnClickedTechnicianMenuGbAdvantageButton();
	afx_msg void OnBnClickedTechnicianMenuMasterSlaveButton();
	afx_msg void OnBnClickedTechnicianMenuEmailButton();
	afx_msg void OnBnClickedTechnicianMenuTouchScreenButton();
	afx_msg void OnBnClickedTechnicianRasCreateButton();
	afx_msg void OnBnClickedTechnicianMenuCashVoucherButton();
	afx_msg void OnBnClickedGbProMachineAsciiMsgsButton();
	afx_msg void OnBnClickedTechRestartAppButton();
	afx_msg void OnBnClickedTechnicianMenuRasStatusButton();
	afx_msg void OnBnClickedTechnicianMenuDispenseButton();

private:
    CTechnicianSystemUsers              *pTechnicianSystemUsers;
    CTechnicianDispense                 *pTechnicianDispense;
    CTechnicianDisplayGameInfoDialog    *pTechnicianDisplayGameInfoDialog;
    CTechnicianGbAdvantageInfo          *pTechnicianGbAdvantageDialog;
    CTechnicianMasterSlaveDialog        *pTechnicianMasterSlaveDialog;
    CEmailSettings                      *pEmailSettings;
	CNetworkSetup						*pNetworkSetup;
	CCashVoucherDialog					*pCashVoucherDialog;
	CGbProAsciiMsg						*pGbProAsciiMsg;

	CButton m_technician_menu_gb_advantage_button;
	CButton m_master_slave_button;
	CButton m_email_settings_button;
	CButton m_prac_setup_button;
	CButton m_technician_menu_cash_voucher_button;
	CButton m_technician_menu_ascii_msgs_button;
	CButton m_technician_ras_status_button;
};
