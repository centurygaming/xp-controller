/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   03/11/10    Added ability to put up a message box without an ok button.
Dave Elquist    07/15/05    Changed variable names.
Dave Elquist    05/13/05    Initial Revision.
*/

// MessageBox.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include ".\messagebox.h"


// CMessageBox dialog

IMPLEMENT_DYNAMIC(CMessageBox, CDialog)
CMessageBox::CMessageBox(BOOL wantCancelButton, CString windowCaption, CString windowText,
    CWnd *pParent /*=NULL*/) : CDialog(CMessageBox::IDD, pParent)
{
    want_ok_button = TRUE;

    want_cancel_button = wantCancelButton;

    window_title = windowCaption;
	window_text = windowText;

    while (!theApp.message_window_queue.empty())
        theApp.message_window_queue.pop();
}

CMessageBox::CMessageBox(BOOL wantOkButton, BOOL wantCancelButton, CString windowCaption, CString windowText,
    CWnd *pParent /*=NULL*/) : CDialog(CMessageBox::IDD, pParent)
{
    want_ok_button = wantOkButton;
    want_cancel_button = wantCancelButton;

    window_title = windowCaption;
	window_text = windowText;

    while (!theApp.message_window_queue.empty())
        theApp.message_window_queue.pop();
}

CMessageBox::~CMessageBox()
{
}

void CMessageBox::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_MESSAGE_BOX_CANCEL_BUTTON, m_cancel_button);
	DDX_Control(pDX, IDC_MESSAGE_BOX_OK_BUTTON, m_ok_button);
	DDX_Control(pDX, IDC_MESSAGE_DIALOG_LIST, m_message_listbox);
	DDX_Control(pDX, IDC_MALFUNCTION_OVERRIDE_BUTTON, m_malfunction_override_button);
}


BEGIN_MESSAGE_MAP(CMessageBox, CDialog)
ON_BN_CLICKED(IDC_MESSAGE_BOX_OK_BUTTON, OnBnClickedMessageBoxOkButton)
ON_BN_CLICKED(IDC_MESSAGE_BOX_CANCEL_BUTTON, OnBnClickedMessageBoxCancelButton)
ON_BN_CLICKED(IDC_MALFUNCTION_OVERRIDE_BUTTON, OnBnClickedMessageBoxMalfunctionOverrideButton)
ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


BOOL CMessageBox::OnInitDialog()
{
 char *buffer_pointer, buffer[5000];
 DWORD buffer_index, buffer_pointer_index;

	CDialog::OnInitDialog();

    if (!want_ok_button)
        m_ok_button.DestroyWindow();

    if (!want_cancel_button)
        m_cancel_button.DestroyWindow();

	m_malfunction_override_button.ShowWindow(SW_HIDE);

	m_message_listbox.SetHorizontalExtent(3000);

    SetDlgItemText(IDC_MESSAGE_DIALOG_TITLE_STATIC, window_title);

    if (window_text.GetLength())
    {
        memset(buffer, 0, sizeof(buffer));
        if (window_text.GetLength() < 5000)
            memcpy(buffer, window_text.GetBuffer(0), window_text.GetLength());
        else
            memcpy(buffer, window_text.GetBuffer(0), 4999);

        buffer_index = 0;
        buffer_pointer_index = 0;
        buffer_pointer = buffer;

        while (buffer_pointer[buffer_pointer_index])
        {
            if (buffer_pointer[buffer_pointer_index] == '\n')
            {
                buffer_pointer[buffer_pointer_index] = 0;
                m_message_listbox.AddString(buffer_pointer);
                buffer_pointer = &buffer[buffer_index + 1];
                buffer_pointer_index = 0;
            }
            else
                buffer_pointer_index++;

            buffer_index++;
        }

        if (buffer_pointer[0])
            m_message_listbox.AddString(buffer_pointer);
    }

	return TRUE;
}

void CMessageBox::OnBnClickedMessageBoxOkButton()
{
    theApp.message_window_queue.push(IDOK);
    DestroyWindow();
}

void CMessageBox::OnBnClickedMessageBoxCancelButton()
{
    theApp.message_window_queue.push(IDCANCEL);
    DestroyWindow();
}

void CMessageBox::OnBnClickedMessageBoxMalfunctionOverrideButton()
{
    theApp.PrinterData("\n\n MALFUNCTION OVERRIDE \n\n");
    theApp.message_window_queue.push(IDCANCEL);
    DestroyWindow();
}

void CMessageBox::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}


void CMessageBox::ShowMalfunctionOverrideButton()
{
	m_malfunction_override_button.ShowWindow(SW_SHOW);
}
