/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   03/11/10    Added ability to put up a message box without an ok button.
Dave Elquist    07/15/05    Changed variable names.
Dave Elquist    05/13/05    Initial Revision.
*/

#pragma once
#include "afxwin.h"
#include "resource.h"


// CMessageBox dialog

class CMessageBox : public CDialog
{
	DECLARE_DYNAMIC(CMessageBox)

public:
	CMessageBox(BOOL wantOkButton, BOOL wantCancelButton, CString windowCaption, CString windowText, CWnd *pParent = NULL);
	CMessageBox(BOOL wantCancelButton, CString windowCaption, CString windowText, CWnd *pParent = NULL);
	virtual ~CMessageBox();
	virtual BOOL OnInitDialog();

	afx_msg void OnBnClickedMessageBoxOkButton();
	afx_msg void OnBnClickedMessageBoxCancelButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedMessageBoxMalfunctionOverrideButton();

	void ShowMalfunctionOverrideButton();

// Dialog Data
	enum { IDD = IDD_MESSAGE_BOX_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

private:
	CString window_title, window_text;

	CButton m_ok_button;
	CButton m_cancel_button;
    BOOL want_cancel_button;
    BOOL want_ok_button;

	CButton m_malfunction_override_button;

public:
	CListBox m_message_listbox;
};
