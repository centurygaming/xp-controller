/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    05/11/05    Initial Revision.
*/

#pragma once


class CMultiControllerServer;


// CMultiControllerClient command target
class CMultiControllerClient : public CMyAsyncSocket
{
    DECLARE_DYNCREATE(CMultiControllerClient)

public:
	CMultiControllerClient(CMultiControllerServer *pSocket = NULL);
	virtual ~CMultiControllerClient();
	virtual void OnClose(int nErrorCode);

// public variables
public:
    bool delete_socket;
	ReceiveIpBuffer rx_buffer;

// private variables
private:
    CMultiControllerServer *pServerSocket;
};


// CMultiControllerServer command target
class CMultiControllerServer : public CAsyncSocket
{
    DECLARE_DYNCREATE(CMultiControllerServer)

public:
	CMultiControllerServer();
	virtual ~CMultiControllerServer();
	virtual void OnAccept(int nErrorCode);
	virtual void OnClose(int nErrorCode);

// public variables
public:
    CMultiControllerClient    *pClientSocket[MAXIMUM_MULTI_CONTROLLER_CONNECTIONS];
};
