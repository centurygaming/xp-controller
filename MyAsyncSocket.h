/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	07/03/13	Added a communication interface to the Station's Casino Casino Management System (CMS).
Robert Fuller   05/02/13    Added code for future support for the GBA interface to Stations Casino Management System.
Robert Fuller   05/31/12    Added code to communicate with the Interactive Display Controller.
Robert Fuller   08/19/08    Added code to support the GB Advantage feature.
*/

#pragma once

// CMyAsyncSocket command target

class CMyAsyncSocket : public CAsyncSocket
{
	DECLARE_DYNCREATE(CMyAsyncSocket)

public:
	CMyAsyncSocket();
	virtual ~CMyAsyncSocket();
	virtual void OnReceive(int nErrorCode);
	virtual void OnClose(int nErrorCode);
	virtual void OnConnect(int nErrorCode);

	WORD GetBuffer(BYTE *buffer, WORD length);

	WORD rx_len;
	BYTE rx_buf[TCPIP_MESSAGE_BUFFER_SIZE];

	bool reading_socket;

	int socket_state;
};


class CMediaBoxAsyncSocket : public CMyAsyncSocket
{
	DECLARE_DYNCREATE(CMediaBoxAsyncSocket)

public:
	CMediaBoxAsyncSocket();
	virtual ~CMediaBoxAsyncSocket();
	virtual void OnClose(int nErrorCode);
	virtual void OnConnect(int nErrorCode);

// private variables
private:
	WORD rx_len;
	BYTE rx_buf[TCPIP_MESSAGE_BUFFER_SIZE];

	bool reading_socket;

};

class CIDCAsyncSocket : public CMyAsyncSocket
{
	DECLARE_DYNCREATE(CIDCAsyncSocket)

public:
	CIDCAsyncSocket();
	virtual ~CIDCAsyncSocket();
	virtual void OnClose(int nErrorCode);
	virtual void OnConnect(int nErrorCode);

// private variables
private:
	WORD rx_len;
	BYTE rx_buf[TCPIP_MESSAGE_BUFFER_SIZE];

	bool reading_socket;

};

class CCmsAsyncSocket : public CMyAsyncSocket
{
	DECLARE_DYNCREATE(CCmsAsyncSocket)

public:
	CCmsAsyncSocket();
	virtual ~CCmsAsyncSocket();
	virtual void OnClose(int nErrorCode);
	virtual void OnConnect(int nErrorCode);

// private variables
private:
	WORD rx_len;
	BYTE rx_buf[TCPIP_MESSAGE_BUFFER_SIZE];

	bool reading_socket;

};
