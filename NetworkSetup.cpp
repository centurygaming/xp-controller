// NetworkSetup.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "NetworkSetup.h"
#include ".\networksetup.h"


// CNetworkSetup dialog

IMPLEMENT_DYNAMIC(CNetworkSetup, CDialog)
CNetworkSetup::CNetworkSetup(CWnd* pParent /*=NULL*/)
	: CDialog(CNetworkSetup::IDD, pParent)
{
}

CNetworkSetup::~CNetworkSetup()
{
}

void CNetworkSetup::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_NETWORK_SETUP_TYPE_COMBO, m_network_setup_type_combobox);
}


BEGIN_MESSAGE_MAP(CNetworkSetup, CDialog)
	ON_BN_CLICKED(IDC_NETWORK_SETUP_EXIT_BUTTON, OnBnClickedNetworkSetupExitButton)
	ON_BN_CLICKED(IDC_NETWORK_SETUP_SAVE_BUTTON, OnBnClickedNetworkSetupSaveButton)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


// CNetworkSetup message handlers

void CNetworkSetup::OnBnClickedNetworkSetupExitButton()
{
	DestroyWindow();
}

void CNetworkSetup::OnBnClickedNetworkSetupSaveButton()
{
 int combobox_value = m_network_setup_type_combobox.GetCurSel();
 FileNetworkSettingsRecord file_settings;

	if (fileRead(NETWORK_SETTINGS, 0, &file_settings, sizeof(file_settings)) &&
		file_settings.settings.network_type != combobox_value && combobox_value >= 0)
	{
		file_settings.settings.network_type = combobox_value;
		fileWrite(NETWORK_SETTINGS, 0, &file_settings, sizeof(file_settings));

		MessageWindow(false, "NETWORK SETTINGS", "CHANGES SAVED\nREBOOT THE CONTROLLER\n\n"
			"If lease line or dialup,\nverify RAS connection named\n'United Coin' exists\n"
			"and is configured properly.");
	}
}

void CNetworkSetup::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

BOOL CNetworkSetup::OnInitDialog()
{
 FileNetworkSettingsRecord file_settings;

	CDialog::OnInitDialog();

	m_network_setup_type_combobox.AddString("Local Area Network (LAN)");
	m_network_setup_type_combobox.AddString("Lease Line Network (Dedicated Circuit)");
	m_network_setup_type_combobox.AddString("Dial Up Network (Analog Phone Line)");

	if (!fileRead(NETWORK_SETTINGS, 0, &file_settings, sizeof(file_settings)))
		DestroyWindow();
	else
		m_network_setup_type_combobox.SetCurSel(file_settings.settings.network_type);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}
