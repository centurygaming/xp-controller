// RasConnectionStatus.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "RasConnectionStatus.h"


// CRasConnectionStatus dialog

IMPLEMENT_DYNAMIC(CRasConnectionStatus, CDialog)

CRasConnectionStatus::CRasConnectionStatus(CWnd* pParent /*=NULL*/)
	: CDialog(CRasConnectionStatus::IDD, pParent)
{
	display_window_active = true;
}

CRasConnectionStatus::~CRasConnectionStatus()
{
}

void CRasConnectionStatus::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RAS_STATUS_LIST, m_ras_status_listbox);
}


BEGIN_MESSAGE_MAP(CRasConnectionStatus, CDialog)
	ON_BN_CLICKED(IDC_RAS_CONNECTION_STATUS_OK_BUTTON, &CRasConnectionStatus::OnBnClickedRasConnectionStatusOkButton)
END_MESSAGE_MAP()

BOOL CRasConnectionStatus::OnInitDialog()
{
	CDialog::OnInitDialog();

	m_ras_status_listbox.SetHorizontalExtent(4000);

	if (!theApp.pNetworkSettingsRecord)
		m_ras_status_listbox.AddString("Unable To Determine Network Type");
	else
	{
		if (!theApp.pNetworkSettingsRecord->network_type)
			m_ras_status_listbox.AddString("Direct Network Connection");
		else if (theApp.pNetworkSettingsRecord->network_type == REMOTE_NETWORK_LEASE_LINE)
			m_ras_status_listbox.AddString("Lease Line Network Connection");
		else if (theApp.pNetworkSettingsRecord->network_type == REMOTE_NETWORK_DIAL_UP)
			m_ras_status_listbox.AddString("Dialup Network Connection");
		else
			m_ras_status_listbox.AddString("Unknown Type Network Connection");
	}

	if (theApp.network_connection_active)
		m_ras_status_listbox.AddString("Network Connection Active");
	else
		m_ras_status_listbox.AddString("Network Connection Inactive");

 return TRUE;
}

// CRasConnectionStatus message handlers

void CRasConnectionStatus::OnBnClickedRasConnectionStatusOkButton()
{
	display_window_active = false;
	DestroyWindow();
}
