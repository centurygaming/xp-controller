/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   07/06/09    Added diagnostic error statistics in the technician rx/tx screen to help troublshoot game/controller communications.
Dave Elquist    07/15/05    Added game display information.
Dave Elquist    06/15/05    Initial Revision.
*/


// TechnicianDisplay.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include ".\techniciandisplay.h"


// CTechnicianDisplay dialog

IMPLEMENT_DYNAMIC(CTechnicianDisplay, CDialog)
CTechnicianDisplay::CTechnicianDisplay(CWnd* pParent /*=NULL*/)	: CDialog(CTechnicianDisplay::IDD, pParent)
{
	pDisplayGameCommControl = NULL;

    destroy_dialog = false;
    display_game_port_data = 0;

    offline_poll_error = 0;
    mux_idle_error = 0;
    extra_bytes_error = 0;
    buffer_overflow_error = 0;
    invalid_handle_error = 0;
    invalid_read_error = 0;
    message_header_error = 0;
    failed_crc_error = 0;
    unrequested_msg_error = 0;
    sequence_error = 0;
    offline_mux_id_error = 0;
    offline_command_error = 0;
    unknown_error = 0;

}

CTechnicianDisplay::~CTechnicianDisplay()
{
}

void CTechnicianDisplay::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TECHNICIAN_TX_DISPLAY_LIST, m_technician_tx_display_list_box);
	DDX_Control(pDX, IDC_TECHNICIAN_RX_DISPLAY_LIST, m_technician_rx_display_list_box);
	DDX_Control(pDX, IDC_TECHNICAN_DISPLAY_CHECK, m_game_data_pause);
	DDX_Control(pDX, IDC_TECHNICIAN_DISPLAY_RESET_BUTTON, m_reset_button);
	DDX_Control(pDX, IDC_TECHNICIAN_DISPLAY_POLLING_BUTTON, m_polling_button);
	DDX_Control(pDX, IDC_TECHNICIAN_DISPLAY_MUX_ID_COMBO, m_mux_id_combo_box);
	DDX_Control(pDX, IDC_TECH_CHANGE_PORT_DISPLAY_BUTTON, m_tech_change_port_display_button);
	DDX_Control(pDX, IDC_TECHICIAN_MESSAGE_ERROR_LIST, m_technician_message_error_list_box);
	DDX_Control(pDX, IDC_TECHICIAN_MESSAGE_ERROR_LIST2, m_technician_message_error_list_box2);
}


BEGIN_MESSAGE_MAP(CTechnicianDisplay, CDialog)
	ON_BN_CLICKED(IDC_TECHNICIAN_DISPLAY_EXIT_BUTTON, OnBnClickedTechnicianDisplayExitButton)
	ON_BN_CLICKED(IDC_TECHNICAN_DISPLAY_CHECK, OnBnClickedTechnicanDisplayCheck)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_TECHNICIAN_DISPLAY_POLLING_BUTTON, OnBnClickedTechnicianDisplayPollingButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_DISPLAY_RESET_BUTTON, OnBnClickedTechnicianDisplayResetButton)
	ON_CBN_SELCHANGE(IDC_TECHNICIAN_DISPLAY_MUX_ID_COMBO, OnCbnSelchangeTechnicianDisplayMuxIdCombo)
	ON_BN_CLICKED(IDC_TECH_CHANGE_PORT_DISPLAY_BUTTON, OnBnClickedTechChangePortDisplayButton)
	ON_BN_CLICKED(IDC_UPDATE_ERRORS_BUTTON, &CTechnicianDisplay::OnBnClickedUpdateErrorsButton)
END_MESSAGE_MAP()


void CTechnicianDisplay::DisplayGameData(GameDataDisplayStruct *pGameDataDisplayStruct)
{
 BYTE iii;
 int rx_string_count, tx_string_count;
 CString display_string, convert_string;

    if (pGameDataDisplayStruct->port_number == display_game_port_data)
    {
        for (iii=0; iii < pGameDataDisplayStruct->length; iii++)
        {
            if (iii == 4 || (iii == 5 && (pGameDataDisplayStruct->data[4] == COMMAND_HEADER ||
                pGameDataDisplayStruct->data[4] == POUND_HEADER || pGameDataDisplayStruct->data[4] == PERCENT_HEADER ||
                pGameDataDisplayStruct->data[4] == AMPERSAND_HEADER || pGameDataDisplayStruct->data[4] == PAYLINE_PROGRESSIVE_HEADER ||
				pGameDataDisplayStruct->data[4] == ASTERISK_HEADER || pGameDataDisplayStruct->data[4] == CASH_FRENZY_HEADER))
				&& pGameDataDisplayStruct->data[0] == MESSAGE_HEADER)
            {
                // print actual character if within printable range
                if (pGameDataDisplayStruct->data[iii] > 0x20 && pGameDataDisplayStruct->data[iii] < 0x7F)
                    convert_string.Format("%c  ", pGameDataDisplayStruct->data[iii]);
                else
                    convert_string.Format("%2.2X ", pGameDataDisplayStruct->data[iii]);
            }
            else
            {
                if (iii == 1 && pGameDataDisplayStruct->data[0] == MESSAGE_HEADER)
                    convert_string.Format("%2.2X(%2.2u) ", pGameDataDisplayStruct->data[iii], pGameDataDisplayStruct->data[iii]);
                else
                    convert_string.Format("%2.2X ", pGameDataDisplayStruct->data[iii]);
            }

            display_string += convert_string;
        }

		if (pGameDataDisplayStruct->data_type == MACHINE_TRANSMIT)
			m_technician_tx_display_list_box.AddString(display_string);
		else if (pGameDataDisplayStruct->data_type == MACHINE_VALID_RECEIVE)
			m_technician_rx_display_list_box.AddString(display_string);
		else if (pGameDataDisplayStruct->data_type > MACHINE_INVALID_RECEIVE &&
			pGameDataDisplayStruct->data_type <= INVALID_RX_OFFLINE_COMMAND)
		{
			convert_string = "INVALID RX: ";

			if (pGameDataDisplayStruct->data_type == INVALID_RX_OFFLINE_POLL)
            {
				convert_string += "offline poll command";
                offline_poll_error++;
            }
			else if (pGameDataDisplayStruct->data_type == INVALID_RX_MUX_IDLE)
            {
				convert_string += "mux idle command";
                mux_idle_error++;
            }
			else if (pGameDataDisplayStruct->data_type == INVALID_RX_EXTRA_BYTES)
            {
				convert_string += "received too many bytes";
                extra_bytes_error++;
            }
			else if (pGameDataDisplayStruct->data_type == INVALID_RX_BUFFER_OVERFLOW)
            {
				convert_string += "buffer overflow";
                buffer_overflow_error++;
            }
			else if (pGameDataDisplayStruct->data_type == INVALID_RX_INVALID_HANDLE)
            {
				convert_string += "invalid serial handle";
                invalid_handle_error++;
            }
			else if (pGameDataDisplayStruct->data_type == INVALID_RX_INVALID_READ)
            {
				convert_string += "invalid serial read";
                invalid_read_error++;
            }
			else if (pGameDataDisplayStruct->data_type == INVALID_RX_MESSAGE_HEADER)
            {
				convert_string += "invalid message header";
                message_header_error++;
            }
			else if (pGameDataDisplayStruct->data_type == INVALID_RX_FAILED_CRC)
            {
				convert_string += "failed crc";
                failed_crc_error++;
            }
			else if (pGameDataDisplayStruct->data_type == INVALID_RX_UNREQUESTED_MSG)
            {
				convert_string += "unrequested message";
                unrequested_msg_error++;
            }
			else if (pGameDataDisplayStruct->data_type == INVALID_RX_SEQUENCE_ERROR)
            {
				convert_string += "message id sequence error";
                sequence_error++;
            }
			else if (pGameDataDisplayStruct->data_type == INVALID_RX_OFFLINE_MUX_ID)
            {
				convert_string += "invalid mux id for offline message";
                offline_mux_id_error++;
            }
			else if (pGameDataDisplayStruct->data_type == INVALID_RX_OFFLINE_COMMAND)
            {
				convert_string += "invalid command for offline message";
                offline_command_error++;
            }
			else
            {
				convert_string += "UNKNOWN ERROR";
                unknown_error++;
            }

			m_technician_rx_display_list_box.AddString(convert_string);

			convert_string = "       ";
			convert_string += display_string;
			m_technician_rx_display_list_box.AddString(convert_string);
		}
		else
		{
			convert_string = "UNKNOWN DATA DISPLAY!: ";
			convert_string += display_string;
			m_technician_tx_display_list_box.AddString(convert_string);
		}

		rx_string_count = m_technician_rx_display_list_box.GetCount();
		tx_string_count = m_technician_tx_display_list_box.GetCount();

		if (rx_string_count > 50000)
			m_technician_rx_display_list_box.ResetContent();
		else if (rx_string_count > 0)
			m_technician_rx_display_list_box.SetTopIndex(rx_string_count - 1);

		if (tx_string_count > 50000)
			m_technician_tx_display_list_box.ResetContent();
		else if (tx_string_count > 0)
			m_technician_tx_display_list_box.SetTopIndex(tx_string_count - 1);
    }
}

void CTechnicianDisplay::OnBnClickedTechnicianDisplayExitButton()
{
	ShowWindow(SW_HIDE);
    destroy_dialog = true;
}

void CTechnicianDisplay::OnBnClickedTechnicanDisplayCheck()
{
 static WORD game_display_port;

    if (m_game_data_pause.GetCheck() == BST_CHECKED)
	{
		game_display_port = display_game_port_data;
        display_game_port_data = 0;
	}
    else if (m_game_data_pause.GetCheck() == BST_UNCHECKED)
	{
		if (!game_display_port)
			OnBnClickedTechChangePortDisplayButton();
		else
			display_game_port_data = game_display_port;
	}
}

void CTechnicianDisplay::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

BOOL CTechnicianDisplay::OnInitDialog()
{
	CDialog::OnInitDialog();

	OnBnClickedTechChangePortDisplayButton();

	// set the horizontal scrollbar pixel width
	m_technician_tx_display_list_box.SetHorizontalExtent(4000);
	m_technician_rx_display_list_box.SetHorizontalExtent(4000);

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPTION: OCX Property Pages should return FALSE
}

void CTechnicianDisplay::OnBnClickedTechnicianDisplayPollingButton()
{
 CString edit_string, button_text = "POLLING";
 int item_selected;
 CGameDevice *pGameDevice;
 CGameCommControl *pGameCommControl;

	GetDlgItemText(IDC_TECH_DISPLAY_PORT_EDIT, edit_string);
	item_selected = atoi(edit_string);

    if (item_selected > 0)
    {
        pGameCommControl = theApp.pHeadGameCommControl;
        while (pGameCommControl)
        {
            if (pGameCommControl->port_info.port_number == item_selected)
            {
                item_selected = m_mux_id_combo_box.GetCurSel();
                if (item_selected != CB_ERR)
                {
                    m_mux_id_combo_box.GetLBText(item_selected, edit_string);
                    item_selected = atoi(edit_string);
                    if (item_selected > 0)
                    {
                        pGameDevice = pGameCommControl->GetGamePointer((BYTE)item_selected);
                        if (pGameDevice)
                        {
                            if (pGameDevice->memory_game_config.stop_polling)
                            {
                                pGameDevice->memory_game_config.stop_polling = 0;
                                button_text = "STOP POLLING";
                            }
                            else
                            {
                                pGameDevice->memory_game_config.stop_polling = 1;
                                button_text = "START POLLING";
                            }

                            pGameDevice->WriteModifiedGameConfig();
                        }
                    }
                }

                pGameCommControl = NULL;
            }
            else
                pGameCommControl = pGameCommControl->pNextGameCommControl;
        }
    }

    m_polling_button.SetWindowText(button_text);
}

CGameDevice* CTechnicianDisplay::GetGamePointer()
{
 CString edit_string;
 int item_selected;
 CGameCommControl *pGameCommControl;

	GetDlgItemText(IDC_TECH_DISPLAY_PORT_EDIT, edit_string);
	item_selected = atoi(edit_string);

    if (item_selected > 0)
    {
        pGameCommControl = theApp.pHeadGameCommControl;
        while (pGameCommControl)
        {
            if (pGameCommControl->port_info.port_number == item_selected)
            {
                item_selected = m_mux_id_combo_box.GetCurSel();
                if (item_selected != CB_ERR)
                {
                    m_mux_id_combo_box.GetLBText(item_selected, edit_string);
                    item_selected = atoi(edit_string);
                    if (item_selected > 0)
                        return pGameCommControl->GetGamePointer((BYTE)item_selected);
                }

                pGameCommControl = NULL;
            }
            else
                pGameCommControl = pGameCommControl->pNextGameCommControl;
        }
    }

 return NULL;
}

void CTechnicianDisplay::OnBnClickedTechnicianDisplayResetButton()
{
 CGameDevice *pGameDevice = GetGamePointer();

    if (pGameDevice)
        pGameDevice->ResetGame();
}

void CTechnicianDisplay::OnCbnSelchangeTechnicianDisplayMuxIdCombo()
{
 CGameDevice *pGameDevice = GetGamePointer();

    if (pGameDevice)
    {
        if (pGameDevice->memory_game_config.stop_polling)
            m_polling_button.SetWindowText("START POLLING");
        else
            m_polling_button.SetWindowText("STOP POLLING");
    }
    else
        m_polling_button.SetWindowText("POLLING");
}

void CTechnicianDisplay::OnBnClickedTechChangePortDisplayButton()
{
 bool pointer_found;
 CString edit_string, display_string;
 CGameCommControl *pGameCommControl;
 CGameDevice *pGameDevice;

	display_game_port_data = 0;
	SetDlgItemText(IDC_TECH_DISPLAY_PORT_EDIT, "");
	m_technician_tx_display_list_box.ResetContent();
	m_technician_rx_display_list_box.ResetContent();
	m_mux_id_combo_box.ResetContent();
	m_polling_button.SetWindowText("POLLING");

	if (m_game_data_pause.GetCheck() == BST_CHECKED)
		m_game_data_pause.SetCheck(BST_UNCHECKED);

	if (!pDisplayGameCommControl)
		pDisplayGameCommControl = theApp.pHeadGameCommControl;
	else
	{
		// verify pointer in case port is removed while viewing
		pointer_found = false;
		pGameCommControl = theApp.pHeadGameCommControl;
		while (pGameCommControl)
		{
			if (pDisplayGameCommControl == pGameCommControl)
			{
				pointer_found = true;
				break;
			}
			else
				pGameCommControl = pGameCommControl->pNextGameCommControl;
		}

		if (!pointer_found)
			pDisplayGameCommControl = NULL;
		else if (pDisplayGameCommControl->pNextGameCommControl)
        {
    		pDisplayGameCommControl = pDisplayGameCommControl->pNextGameCommControl;
			while (pDisplayGameCommControl && (pDisplayGameCommControl->sas_com != NULL))
        		pDisplayGameCommControl = pDisplayGameCommControl->pNextGameCommControl;
        }
		else
			pDisplayGameCommControl = theApp.pHeadGameCommControl;
	}

	if (pDisplayGameCommControl)
	{
		display_game_port_data = pDisplayGameCommControl->port_info.port_number;
		edit_string.Format("%u", display_game_port_data);
		SetDlgItemText(IDC_TECH_DISPLAY_PORT_EDIT, edit_string);



        m_technician_message_error_list_box.ResetContent();
        display_string.Format("Invalid Byte Count: %u", pDisplayGameCommControl->message_errors.invalid_byte_count);
	    m_technician_message_error_list_box.AddString(display_string);
        display_string.Format("Header: %u", pDisplayGameCommControl->message_errors.header);
	    m_technician_message_error_list_box.AddString(display_string);
        display_string.Format("CRC: %u", pDisplayGameCommControl->message_errors.crc);
	    m_technician_message_error_list_box.AddString(display_string);
        display_string.Format("No Request: %u", pDisplayGameCommControl->message_errors.no_request);
	    m_technician_message_error_list_box.AddString(display_string);
        display_string.Format("Sequence: %u", pDisplayGameCommControl->message_errors.sequence);
	    m_technician_message_error_list_box.AddString(display_string);
        display_string.Format("Mux ID: %u", pDisplayGameCommControl->message_errors.mux_id);
	    m_technician_message_error_list_box.AddString(display_string);
        display_string.Format("No Response: %u", pDisplayGameCommControl->message_errors.no_response);
	    m_technician_message_error_list_box.AddString(display_string);

        m_technician_message_error_list_box2.ResetContent();
        display_string.Format("offline poll command: %u", offline_poll_error);
	    m_technician_message_error_list_box2.AddString(display_string);
        display_string.Format("mux idle command: %u", mux_idle_error);
	    m_technician_message_error_list_box2.AddString(display_string);
        display_string.Format("extra bytes: %u", extra_bytes_error);
	    m_technician_message_error_list_box2.AddString(display_string);
        display_string.Format("buffer overflow: %u", buffer_overflow_error);
	    m_technician_message_error_list_box2.AddString(display_string);
        display_string.Format("invalid serial handle: %u", invalid_handle_error);
	    m_technician_message_error_list_box2.AddString(display_string);
        display_string.Format("invalid serial read: %u", invalid_read_error);
	    m_technician_message_error_list_box2.AddString(display_string);
        display_string.Format("invalid message header: %u", message_header_error);
	    m_technician_message_error_list_box2.AddString(display_string);
        display_string.Format("failed crc: %u", failed_crc_error);
	    m_technician_message_error_list_box2.AddString(display_string);
        display_string.Format("unrequested message: %u", unrequested_msg_error);
	    m_technician_message_error_list_box2.AddString(display_string);
        display_string.Format("sequence error: %u", sequence_error);
	    m_technician_message_error_list_box2.AddString(display_string);
        display_string.Format("offline mux id message: %u", offline_mux_id_error);
	    m_technician_message_error_list_box2.AddString(display_string);
        display_string.Format("offline command : %u", offline_command_error);
	    m_technician_message_error_list_box2.AddString(display_string);
        display_string.Format("unknown error : %u", unknown_error);
	    m_technician_message_error_list_box2.AddString(display_string);


		pGameDevice = pDisplayGameCommControl->pHeadGameDevice;
		while (pGameDevice)
		{
			edit_string.Format("%u", pGameDevice->memory_game_config.mux_id);
			m_mux_id_combo_box.AddString(edit_string);
			pGameDevice = pGameDevice->pNextGame;
		}
	}
}

void CTechnicianDisplay::OnBnClickedUpdateErrorsButton()
{
    CString display_string;

    m_technician_message_error_list_box.ResetContent();
    display_string.Format("Invalid Byte Count: %u", pDisplayGameCommControl->message_errors.invalid_byte_count);
	m_technician_message_error_list_box.AddString(display_string);
    display_string.Format("Header: %u", pDisplayGameCommControl->message_errors.header);
	m_technician_message_error_list_box.AddString(display_string);
    display_string.Format("CRC: %u", pDisplayGameCommControl->message_errors.crc);
	m_technician_message_error_list_box.AddString(display_string);
    display_string.Format("No Request: %u", pDisplayGameCommControl->message_errors.no_request);
	m_technician_message_error_list_box.AddString(display_string);
    display_string.Format("Sequence: %u", pDisplayGameCommControl->message_errors.sequence);
	m_technician_message_error_list_box.AddString(display_string);
    display_string.Format("Mux ID: %u", pDisplayGameCommControl->message_errors.mux_id);
	m_technician_message_error_list_box.AddString(display_string);
    display_string.Format("No Response: %u", pDisplayGameCommControl->message_errors.no_response);
	m_technician_message_error_list_box.AddString(display_string);

    m_technician_message_error_list_box2.ResetContent();
    display_string.Format("offline poll command: %u", offline_poll_error);
	m_technician_message_error_list_box2.AddString(display_string);
    display_string.Format("mux idle command: %u", mux_idle_error);
	m_technician_message_error_list_box2.AddString(display_string);
    display_string.Format("extra bytes: %u", extra_bytes_error);
	m_technician_message_error_list_box2.AddString(display_string);
    display_string.Format("buffer overflow: %u", buffer_overflow_error);
	m_technician_message_error_list_box2.AddString(display_string);
    display_string.Format("invalid serial handle: %u", invalid_handle_error);
	m_technician_message_error_list_box2.AddString(display_string);
    display_string.Format("invalid serial read: %u", invalid_read_error);
	m_technician_message_error_list_box2.AddString(display_string);
    display_string.Format("invalid message header: %u", message_header_error);
	m_technician_message_error_list_box2.AddString(display_string);
    display_string.Format("failed crc: %u", failed_crc_error);
	m_technician_message_error_list_box2.AddString(display_string);
    display_string.Format("unrequested message: %u", unrequested_msg_error);
	m_technician_message_error_list_box2.AddString(display_string);
    display_string.Format("sequence error: %u", sequence_error);
	m_technician_message_error_list_box2.AddString(display_string);
    display_string.Format("offline mux id message: %u", offline_mux_id_error);
	m_technician_message_error_list_box2.AddString(display_string);
    display_string.Format("offline command : %u", offline_command_error);
	m_technician_message_error_list_box2.AddString(display_string);
    display_string.Format("unknown error : %u", unknown_error);
	m_technician_message_error_list_box2.AddString(display_string);
}
