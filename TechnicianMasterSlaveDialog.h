/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    07/15/05    Initial Revision.
*/


#pragma once
#include "afxwin.h"


// CTechnicianMasterSlaveDialog dialog

class CTechnicianMasterSlaveDialog : public CDialog
{
	DECLARE_DYNAMIC(CTechnicianMasterSlaveDialog)

public:
	CTechnicianMasterSlaveDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTechnicianMasterSlaveDialog();
	virtual BOOL OnInitDialog();

// Dialog Data
	enum { IDD = IDD_TECHNICIAN_MASTER_SLAVE_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedTechnicianMasterSlaveButton1();
	afx_msg void OnBnClickedTechnicianMasterSlaveButton2();
	afx_msg void OnBnClickedTechnicianMasterSlaveButton3();
	afx_msg void OnBnClickedTechnicianMasterSlaveButton4();
	afx_msg void OnBnClickedTechnicianMasterSlaveButton5();
	afx_msg void OnBnClickedTechnicianMasterSlaveButton6();
	afx_msg void OnBnClickedTechnicianMasterSlaveButton7();
	afx_msg void OnBnClickedTechnicianMasterSlaveButton8();
	afx_msg void OnBnClickedTechnicianMasterSlaveButton9();
	afx_msg void OnBnClickedTechnicianMasterSlavePeriodButton();
	afx_msg void OnBnClickedTechnicianMasterSlaveButton0();
	afx_msg void OnBnClickedTechnicianMasterSlaveClearButton();
	afx_msg void OnBnClickedTechnicianSlaveControllerAddButton();
	afx_msg void OnBnClickedTechnicianMasterControllerAddButton();
	afx_msg void OnBnClickedTechnicianControllerRemoveButton();
	afx_msg void OnBnClickedTechnicianMasterSlaveExitButton();

// private functions
private:
    void ProcessButtonClick(BYTE button_press);
    void FillMasterSlaveListBox();

// private variables
private:
	CListBox m_controller_list_box;
};
