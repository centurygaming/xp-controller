/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    07/15/05    Changed user level calculation.
Dave Elquist    06/14/05    Initial Revision.
*/


// TechnicianSystemUsers.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "TechnicianSystemUsers.h"
#include ".\techniciansystemusers.h"


// CTechnicianSystemUsers dialog

IMPLEMENT_DYNAMIC(CTechnicianSystemUsers, CDialog)
CTechnicianSystemUsers::CTechnicianSystemUsers(CWnd* pParent /*=NULL*/)
	: CDialog(CTechnicianSystemUsers::IDD, pParent)
{
    current_edit_focus = 0;
    memset(&system_user, 0, sizeof(system_user));
}

CTechnicianSystemUsers::~CTechnicianSystemUsers()
{
}

void CTechnicianSystemUsers::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTechnicianSystemUsers, CDialog)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USERS_EXIT_BUTTON, OnBnClickedTechnicianSystemUsersExitButton)
	ON_WM_MOUSEMOVE()
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_ADD_BUTTON, OnBnClickedTechnicianSystemUserAddButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_REMOVE_BUTTON, OnBnClickedTechnicianSystemUserRemoveButton)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON1, OnBnClickedTechnicianSystemUserButton1)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON2, OnBnClickedTechnicianSystemUserButton2)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON3, OnBnClickedTechnicianSystemUserButton3)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON4, OnBnClickedTechnicianSystemUserButton4)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON5, OnBnClickedTechnicianSystemUserButton5)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON6, OnBnClickedTechnicianSystemUserButton6)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON7, OnBnClickedTechnicianSystemUserButton7)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON8, OnBnClickedTechnicianSystemUserButton8)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON9, OnBnClickedTechnicianSystemUserButton9)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_BUTTON0, OnBnClickedTechnicianSystemUserButton0)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_CLEAR_BUTTON, OnBnClickedTechnicianSystemUserClearButton)
	ON_EN_SETFOCUS(IDC_TECHNICIAN_SYSTEM_USER_ID_EDIT, OnEnSetfocusTechnicianSystemUserIdEdit)
	ON_EN_SETFOCUS(IDC_TECHNICIAN_SYSTEM_USER_PASSWORD_EDIT, OnEnSetfocusTechnicianSystemUserPasswordEdit)
	ON_EN_SETFOCUS(IDC_TECHNICIAN_SYSTEM_USER_FIRST_NAME_EDIT, OnEnSetfocusTechnicianSystemUserFirstNameEdit)
	ON_EN_SETFOCUS(IDC_TECHNICIAN_SYSTEM_USER_LAST_NAME_EDIT, OnEnSetfocusTechnicianSystemUserLastNameEdit)
	ON_BN_CLICKED(IDC_TECHNICIAN_SYSTEM_USER_PRINT_BUTTON, OnBnClickedTechnicianSystemUserPrintButton)
END_MESSAGE_MAP()


// CTechnicianSystemUsers message handlers

void CTechnicianSystemUsers::OnBnClickedTechnicianSystemUsersExitButton()
{
	DestroyWindow();
}

void CTechnicianSystemUsers::OnMouseMove(UINT nFlags, CPoint point)
{
	theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}

void CTechnicianSystemUsers::OnBnClickedTechnicianSystemUserAddButton()
{
 CString prt_buf;
 CashierAndManagerUserFile cashier_and_manager_user;

    if (system_user.id / 10000 != CASHIER && system_user.id / 10000 != MANAGER)
    {
        prt_buf.Format("INVALID USER ID: %u", system_user.id);
        MessageWindow(false, "SYSTEM USERS", prt_buf);
    }
    else
    {
        if (!system_user.password)
            MessageWindow(false, "SYSTEM USERS", "INVALID PASSWORD");
        else
        {
            if (!system_user.first_name[0] || !system_user.last_name[0])
                MessageWindow(false, "SYSTEM USERS", "INVALID SYSTEM USER NAME");
            else
            {
                if (fileGetRecord(LOCATION_IDS, &cashier_and_manager_user,
                    sizeof(cashier_and_manager_user), system_user.id) >= 0)
                {
                    prt_buf.Format("SYSTEM USER ALREADY EXISTS\nUser ID: %u", system_user.id);
                    MessageWindow(false, "SYSTEM USERS", prt_buf);
                }
                else
                {
                    memset(&cashier_and_manager_user, 0, sizeof(cashier_and_manager_user));
                    memcpy(&cashier_and_manager_user.user, &system_user, sizeof(cashier_and_manager_user.user));

                    if (!fileWrite(LOCATION_IDS, -1, &cashier_and_manager_user, sizeof(cashier_and_manager_user)))
                        MessageWindow(false, "SYSTEM USERS", "ERROR WRITING SYSTEM USER FILE.");
                    else
                    {
                        theApp.printer_busy = true;
                        theApp.PrinterHeader(0);
                        prt_buf.Format(" SYSTEM USER ADDED\n USER ID: %u\n\n", system_user.id);
                        theApp.PrinterData(prt_buf.GetBuffer(0));
                        theApp.printer_busy = false;
						MessageWindow(false, "SYSTEM USERS", "System User Added");
                    }
                }
            }
        }
    }
}

void CTechnicianSystemUsers::OnBnClickedTechnicianSystemUserRemoveButton()
{
 int record_index;
 CString prt_buf;
 CashierAndManagerUserFile cashier_and_manager_user;

    if (system_user.id > 9999)
    {
        record_index = fileGetRecord(LOCATION_IDS, &cashier_and_manager_user,
            sizeof(cashier_and_manager_user), system_user.id);

        if (record_index < 0)
        {
            prt_buf.Format("USER ID NOT FOUND: %u", system_user.id);
            MessageWindow(false, "SYSTEM USERS", prt_buf);
        }
        else
        {
            if (fileDeleteRecord(LOCATION_IDS, sizeof(cashier_and_manager_user), record_index))
            {
                prt_buf.Format("CTechnicianSystemUsers::OnBnClickedTechnicianSystemUserRemoveButton - user deleted: %u.", system_user.id);
                logMessage(prt_buf);

                theApp.PrinterHeader(0);
                prt_buf.Format(" SYSTEM USER DELETED\n ID Number: %u\n\n", system_user.id);
                theApp.PrinterData(prt_buf.GetBuffer(0));
            }
            else
                MessageWindow(false, "SYSTEM USERS", "ERROR DELETING USER ID");
        }
    }
}

void CTechnicianSystemUsers::ProcessButtonClick(BYTE button_press)
{
 char *pNameString, *pDialogNameString, button_char;
 int iii;
 static DWORD last_button_press_timer;
 CString edit_string;

    if (current_edit_focus == IDC_TECHNICIAN_SYSTEM_USER_ID_EDIT)
    {
        if (!system_user.id)
        {
            if (button_press == 1 || button_press == 2)
            {
                system_user.id = button_press;
                edit_string.Format("%u", system_user.id);
                SetDlgItemText(IDC_TECHNICIAN_SYSTEM_USER_ID_EDIT, edit_string);
            }
            else
                MessageWindow(false, "SYSTEM USERS", "CASHIER ID MUST START WITH '1'\nMANAGER ID MUST START WITH '2'");
        }
        else
        {
            if (system_user.id < 9999)
            {
                system_user.id = system_user.id * 10 + button_press;
                edit_string.Format("%u", system_user.id);
                SetDlgItemText(IDC_TECHNICIAN_SYSTEM_USER_ID_EDIT, edit_string);
            }
        }
    }
    else if (current_edit_focus == IDC_TECHNICIAN_SYSTEM_USER_PASSWORD_EDIT)
    {
        if (system_user.password <= 999)
        {
            system_user.password = system_user.password * 10 + button_press;
            edit_string.Format("%u", system_user.password);
            SetDlgItemText(IDC_TECHNICIAN_SYSTEM_USER_PASSWORD_EDIT, edit_string);
        }
    }
    else if (current_edit_focus == IDC_TECHNICIAN_SYSTEM_USER_FIRST_NAME_EDIT ||
             current_edit_focus == IDC_TECHNICIAN_SYSTEM_USER_LAST_NAME_EDIT)
    {
        if (current_edit_focus == IDC_TECHNICIAN_SYSTEM_USER_FIRST_NAME_EDIT)
            pDialogNameString = system_user.first_name;
        else
            pDialogNameString = system_user.last_name;

        if (strlen(pDialogNameString) < USER_FILE_RECORD_STRING_LENGTH - 1)
        {
            switch (button_press)
            {
                case 0:
                    button_char = ' ';
                    break;

                case 1:
                    button_char = 'A';
                    break;

                case 2:
                    button_char = 'D';
                    break;

                case 3:
                    button_char = 'G';
                    break;

                case 4:
                    button_char = 'J';
                    break;

                case 5:
                    button_char = 'M';
                    break;

                case 6:
                    button_char = 'P';
                    break;

                case 7:
                    button_char = 'S';
                    break;

                case 8:
                    button_char = 'V';
                    break;

                case 9:
                    button_char = 'Y';
                    break;
            }

            if (!pDialogNameString[0])
                pDialogNameString[0] = button_char;
            else
            {
                iii = 0;
                pNameString = NULL;
                while (!pNameString)
                {
                    if (!pDialogNameString[iii])
                        pNameString = &pDialogNameString[iii - 1];
                    else
                        iii++;
                }

                if (last_button_press_timer >= GetTickCount())
                {
                    switch (button_press)
                    {
                        case 0:
                            pNameString[1] = ' ';
                            break;

                        case 1:
                            if (pNameString[0] == 'A')
                                pNameString[0] = 'B';
                            else if (pNameString[0] == 'B')
                                pNameString[0] = 'C';
                            else if (pNameString[0] == 'C')
                                pNameString[0] = 'A';
                            else
                                pNameString[1] = 'A';
                            break;

                        case 2:
                            if (pNameString[0] == 'D')
                                pNameString[0] = 'E';
                            else if (pNameString[0] == 'E')
                                pNameString[0] = 'F';
                            else if (pNameString[0] == 'F')
                                pNameString[0] = 'D';
                            else
                                pNameString[1] = 'D';
                            break;

                        case 3:
                            if (pNameString[0] == 'G')
                                pNameString[0] = 'H';
                            else if (pNameString[0] == 'H')
                                pNameString[0] = 'I';
                            else if (pNameString[0] == 'I')
                                pNameString[0] = 'G';
                            else
                                pNameString[1] = 'G';
                            break;

                        case 4:
                            if (pNameString[0] == 'J')
                                pNameString[0] = 'K';
                            else if (pNameString[0] == 'K')
                                pNameString[0] = 'L';
                            else if (pNameString[0] == 'L')
                                pNameString[0] = 'J';
                            else
                                pNameString[1] = 'J';
                            break;

                        case 5:
                            if (pNameString[0] == 'M')
                                pNameString[0] = 'N';
                            else if (pNameString[0] == 'N')
                                pNameString[0] = 'O';
                            else if (pNameString[0] == 'O')
                                pNameString[0] = 'M';
                            else
                                pNameString[1] = 'M';
                            break;

                        case 6:
                            if (pNameString[0] == 'P')
                                pNameString[0] = 'Q';
                            else if (pNameString[0] == 'Q')
                                pNameString[0] = 'R';
                            else if (pNameString[0] == 'R')
                                pNameString[0] = 'P';
                            else
                                pNameString[1] = 'P';
                            break;

                        case 7:
                            if (pNameString[0] == 'S')
                                pNameString[0] = 'T';
                            else if (pNameString[0] == 'T')
                                pNameString[0] = 'U';
                            else if (pNameString[0] == 'U')
                                pNameString[0] = 'S';
                            else
                                pNameString[1] = 'S';
                            break;

                        case 8:
                            if (pNameString[0] == 'V')
                                pNameString[0] = 'W';
                            else if (pNameString[0] == 'W')
                                pNameString[0] = 'X';
                            else if (pNameString[0] == 'X')
                                pNameString[0] = 'V';
                            else
                                pNameString[1] = 'V';
                            break;

                        case 9:
                            if (pNameString[0] == 'Y')
                                pNameString[0] = 'Z';
                            else if (pNameString[0] == 'Z')
                                pNameString[0] = 'Y';
                            else
                                pNameString[1] = 'Y';
                            break;
                    }
                }
                else
                    pNameString[1] = button_char;
            }
        }

        last_button_press_timer = GetTickCount() + LAST_BUTTON_PRESS_TIMEOUT;
        SetDlgItemText(current_edit_focus, pDialogNameString);
    }
}

void CTechnicianSystemUsers::OnBnClickedTechnicianSystemUserButton1()
{
    ProcessButtonClick(1);
}

void CTechnicianSystemUsers::OnBnClickedTechnicianSystemUserButton2()
{
    ProcessButtonClick(2);
}

void CTechnicianSystemUsers::OnBnClickedTechnicianSystemUserButton3()
{
    ProcessButtonClick(3);
}

void CTechnicianSystemUsers::OnBnClickedTechnicianSystemUserButton4()
{
    ProcessButtonClick(4);
}

void CTechnicianSystemUsers::OnBnClickedTechnicianSystemUserButton5()
{
    ProcessButtonClick(5);
}

void CTechnicianSystemUsers::OnBnClickedTechnicianSystemUserButton6()
{
    ProcessButtonClick(6);
}

void CTechnicianSystemUsers::OnBnClickedTechnicianSystemUserButton7()
{
    ProcessButtonClick(7);
}

void CTechnicianSystemUsers::OnBnClickedTechnicianSystemUserButton8()
{
    ProcessButtonClick(8);
}

void CTechnicianSystemUsers::OnBnClickedTechnicianSystemUserButton9()
{
    ProcessButtonClick(9);
}

void CTechnicianSystemUsers::OnBnClickedTechnicianSystemUserButton0()
{
    ProcessButtonClick(0);
}

void CTechnicianSystemUsers::OnBnClickedTechnicianSystemUserClearButton()
{
    if (current_edit_focus == IDC_TECHNICIAN_SYSTEM_USER_ID_EDIT)
    {
        system_user.id = 0;
        SetDlgItemText(IDC_TECHNICIAN_SYSTEM_USER_ID_EDIT, "");
    }
    else if (current_edit_focus == IDC_TECHNICIAN_SYSTEM_USER_PASSWORD_EDIT)
    {
        system_user.password = 0;
        SetDlgItemText(IDC_TECHNICIAN_SYSTEM_USER_PASSWORD_EDIT, "");
    }
    else if (current_edit_focus == IDC_TECHNICIAN_SYSTEM_USER_FIRST_NAME_EDIT)
    {
        memset(system_user.first_name, 0, sizeof(system_user.first_name));
        SetDlgItemText(IDC_TECHNICIAN_SYSTEM_USER_FIRST_NAME_EDIT, "");
    }
    else if (current_edit_focus == IDC_TECHNICIAN_SYSTEM_USER_LAST_NAME_EDIT)
    {
        memset(system_user.last_name, 0, sizeof(system_user.last_name));
        SetDlgItemText(IDC_TECHNICIAN_SYSTEM_USER_LAST_NAME_EDIT, "");
    }
}

void CTechnicianSystemUsers::OnEnSetfocusTechnicianSystemUserIdEdit()
{
    current_edit_focus = IDC_TECHNICIAN_SYSTEM_USER_ID_EDIT;
}

void CTechnicianSystemUsers::OnEnSetfocusTechnicianSystemUserPasswordEdit()
{
    current_edit_focus = IDC_TECHNICIAN_SYSTEM_USER_PASSWORD_EDIT;
}

void CTechnicianSystemUsers::OnEnSetfocusTechnicianSystemUserFirstNameEdit()
{
    current_edit_focus = IDC_TECHNICIAN_SYSTEM_USER_FIRST_NAME_EDIT;
}

void CTechnicianSystemUsers::OnEnSetfocusTechnicianSystemUserLastNameEdit()
{
    current_edit_focus = IDC_TECHNICIAN_SYSTEM_USER_LAST_NAME_EDIT;
}

void CTechnicianSystemUsers::OnBnClickedTechnicianSystemUserPrintButton()
{
 DWORD file_index = 0;
 CString prt_buf;
 CashierAndManagerUserFile cashier_and_manager_user;

    theApp.printer_busy = true;
    theApp.PrinterTitle("CASHIER AND MANAGER IDS");
    theApp.PrinterData("\n");

    while (fileRead(LOCATION_IDS, file_index, &cashier_and_manager_user, sizeof(cashier_and_manager_user)))
    {
        prt_buf.Format(" %u: %s %s\n", cashier_and_manager_user.user.id,
            cashier_and_manager_user.user.first_name, cashier_and_manager_user.user.last_name);
        theApp.PrinterData(prt_buf.GetBuffer(0));
        file_index++;
    }

    if (!file_index)
        theApp.PrinterData(" ** NO IDS FOUND **\n");

    theApp.PrinterData(PRINTER_FORM_FEED);
    theApp.printer_busy = false;
}
