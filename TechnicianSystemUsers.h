/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    07/15/05    Moved function.
Dave Elquist    06/14/05    Initial Revision.
*/


#pragma once


// CTechnicianSystemUsers dialog

class CTechnicianSystemUsers : public CDialog
{
	DECLARE_DYNAMIC(CTechnicianSystemUsers)

public:
	CTechnicianSystemUsers(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTechnicianSystemUsers();

// Dialog Data
	enum { IDD = IDD_TECHNICIAN_SYSTEM_USERS_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnBnClickedTechnicianSystemUsersExitButton();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedTechnicianSystemUserAddButton();
	afx_msg void OnBnClickedTechnicianSystemUserRemoveButton();
	afx_msg void OnBnClickedTechnicianSystemUserButton1();
	afx_msg void OnBnClickedTechnicianSystemUserButton2();
	afx_msg void OnBnClickedTechnicianSystemUserButton3();
	afx_msg void OnBnClickedTechnicianSystemUserButton4();
	afx_msg void OnBnClickedTechnicianSystemUserButton5();
	afx_msg void OnBnClickedTechnicianSystemUserButton6();
	afx_msg void OnBnClickedTechnicianSystemUserButton7();
	afx_msg void OnBnClickedTechnicianSystemUserButton8();
	afx_msg void OnBnClickedTechnicianSystemUserButton9();
	afx_msg void OnBnClickedTechnicianSystemUserButton0();
	afx_msg void OnBnClickedTechnicianSystemUserClearButton();
	afx_msg void OnEnSetfocusTechnicianSystemUserIdEdit();
	afx_msg void OnEnSetfocusTechnicianSystemUserPasswordEdit();
	afx_msg void OnEnSetfocusTechnicianSystemUserFirstNameEdit();
	afx_msg void OnEnSetfocusTechnicianSystemUserLastNameEdit();
	afx_msg void OnBnClickedTechnicianSystemUserPrintButton();

// private functions
private:
    void ProcessButtonClick(BYTE button_press);

// private variables
private:
    DWORD current_edit_focus;

    SystemUser system_user;
};
