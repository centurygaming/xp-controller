/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    05/26/05    Initial Revision.
*/


// TimerWindow.cpp : implementation file
//

#include "stdafx.h"
#include "xp_controller.h"
#include "TimerWindow.h"
#include ".\timerwindow.h"


// CTimerWindow dialog

IMPLEMENT_DYNAMIC(CTimerWindow, CDialog)
CTimerWindow::CTimerWindow(CWnd* pParent /*=NULL*/)
	: CDialog(CTimerWindow::IDD, pParent)
{
}

CTimerWindow::~CTimerWindow()
{
}

void CTimerWindow::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TIMER_WINDOW_PROGRESS, m_timer_window_bar);
}


BEGIN_MESSAGE_MAP(CTimerWindow, CDialog)
	ON_WM_MOUSEMOVE()
END_MESSAGE_MAP()


void CTimerWindow::OnMouseMove(UINT nFlags, CPoint point)
{
    theApp.OnMouseMove();
	CDialog::OnMouseMove(nFlags, point);
}
