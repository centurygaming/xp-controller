/* Modification History:
Name            Date        Notes
----            ----        -----
Dave Elquist    05/26/05    Initial Revision.
*/


#pragma once
#include "afxcmn.h"


// CTimerWindow dialog

class CTimerWindow : public CDialog
{
	DECLARE_DYNAMIC(CTimerWindow)

public:
	CTimerWindow(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTimerWindow();

// Dialog Data
	enum { IDD = IDD_TIMER_BAR_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	CProgressCtrl m_timer_window_bar;
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
};
