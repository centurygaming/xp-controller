/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	12/05/13	Added black flush, red flush, any flush, and any straight win types to the GBA win awards.
Robert Fuller   09/22/09    Made sure to exclude straight flushes from simple flush evaluation routines.
Robert Fuller   02/24/09    Added new specific flush and face card full house promos.
Robert Fuller   11/04/08    Made sure the win evaluation descriptor we are using to determine a straight flush is NOT a royal flush.
Robert Fuller   09/19/08    Made sure the win evaluation descriptor we are using to determine a straight flush is NOT a royal flush.
*/

#include "stdafx.h"
#include "xp_controller.h"
#include "GbProManager.h"

#define MAX_HAND             5
#define STD_HAND_SIZE        MAX_HAND

#define CARD_BUFFER_SIZE     10

#define RANK_MASK   0x0f
#define SUIT_MASK   0x70

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CWinEvaluation::CWinEvaluation()
{

}

CWinEvaluation::~CWinEvaluation()
{

}

void CWinEvaluation::evaluateHand (unsigned char* cards)
{
    initEvaluationMatrixes ();
    fillEvaluationMatrixes (cards);
    fillEvaluationDescriptor ();
}

bool CWinEvaluation::checkForWin (GbProWinCategory* gbpro_win_category)
{
    bool match = FALSE;

    if (royalFlushFound (gbpro_win_category->evaluation_descriptor))
        match = TRUE;
    else if (straightFlushFound (gbpro_win_category->evaluation_descriptor))
        match = TRUE;
    else if (fourOfKindFound (gbpro_win_category->evaluation_descriptor))
        match = TRUE;
    else if (faceCardsFullHouseFound (gbpro_win_category->evaluation_descriptor))
        match = TRUE;
    else if (fullHouseFound (gbpro_win_category->evaluation_descriptor))
        match = TRUE;
    else if (clubsFlushFound (gbpro_win_category->evaluation_descriptor))
        match = TRUE;
    else if (diamondsFlushFound (gbpro_win_category->evaluation_descriptor))
        match = TRUE;
    else if (heartsFlushFound (gbpro_win_category->evaluation_descriptor))
        match = TRUE;
    else if (spadesFlushFound (gbpro_win_category->evaluation_descriptor))
        match = TRUE;
    else if (blackFlushFound (gbpro_win_category->evaluation_descriptor))
        match = TRUE;
    else if (redFlushFound (gbpro_win_category->evaluation_descriptor))
        match = TRUE;
    else if (anyFlushFound (gbpro_win_category->evaluation_descriptor))
        match = TRUE;
    else if (straightFound (gbpro_win_category->evaluation_descriptor))
        match = TRUE;
    else if (threeOfKindFound (gbpro_win_category->evaluation_descriptor))
        match = TRUE;
            
    return match;


}

void CWinEvaluation::fillEvaluationMatrixes (unsigned char* cards)
{
    int i;
    char card_value;
    char rank;
    char suit;

    for (i=0; i < STD_HAND_SIZE; i++)
    {
        card_value = cards[i];
        rank = card_value & RANK_MASK;
//        suit = (card_value & SUIT_MASK) >> 4;   ???
        suit = (card_value & SUIT_MASK) >> 4;
//        suit = card_value & SUIT_MASK;
        hand_matrix[suit][rank]++;
        suit_matrix[suit]++;
        rank_matrix[rank]++;
    }
}

void CWinEvaluation::initEvaluationMatrixes (void)
{
    int i, j;

    for (j=0; j < MAX_CARD_SUIT; j++ )
    {
        suit_matrix[j] = 0;
        for (i=0; i < MAX_CARD_RANK + RANK_INDEX_VALUE_OFFSET; i++)
            hand_matrix[j][i] = 0;
    }            

    for (i=0; i < MAX_CARD_RANK + RANK_INDEX_VALUE_OFFSET; i++)
        rank_matrix[i] = 0;
}

void CWinEvaluation::fillEvaluationDescriptor (void)
{
    evaluation_descriptor.flush_flag = flushFound();
    evaluation_descriptor.suit_attr = getSuitAttr();
    evaluation_descriptor.primary_rank_attr = getPrimaryRankCount();
    evaluation_descriptor.auxilary_rank_attr = getAuxilaryRankCount();
    evaluation_descriptor.straight_flag = straightFound();
    evaluation_descriptor.face_card_count = getFaceCardRankCount();
    evaluation_descriptor.rank_limit = getRankLimit();
    evaluation_descriptor.rank_max = getRankMax();
}

char CWinEvaluation::getSuitAttr (void)
{
    char suit_attr = MAX_CARD_SUIT;

    if (evaluation_descriptor.flush_flag)
        suit_attr = getFlushSuit();

    return suit_attr;
}

bool CWinEvaluation::clubsFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr)
{
    bool match = FALSE;

    if (evaluation_descriptor_ptr->flush_flag && evaluation_descriptor.flush_flag &&
        (evaluation_descriptor_ptr->suit_attr == SUIT_CLUBS) &&
        (evaluation_descriptor.suit_attr == SUIT_CLUBS) &&
        (!evaluation_descriptor_ptr->straight_flag) && (!evaluation_descriptor.straight_flag))
            match = TRUE;

    return match;
}

bool CWinEvaluation::diamondsFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr)
{
    bool match = FALSE;

    if (evaluation_descriptor_ptr->flush_flag && evaluation_descriptor.flush_flag &&
        (evaluation_descriptor_ptr->suit_attr == SUIT_DIAMONDS) &&
        (evaluation_descriptor.suit_attr == SUIT_DIAMONDS) &&
        (!evaluation_descriptor_ptr->straight_flag) && (!evaluation_descriptor.straight_flag))
            match = TRUE;

    return match;
}

bool CWinEvaluation::heartsFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr)
{
    bool match = FALSE;

    if (evaluation_descriptor_ptr->flush_flag && evaluation_descriptor.flush_flag &&
        (evaluation_descriptor_ptr->suit_attr == SUIT_HEARTS) &&
        (evaluation_descriptor.suit_attr == SUIT_HEARTS) &&
        (!evaluation_descriptor_ptr->straight_flag) && (!evaluation_descriptor.straight_flag))
            match = TRUE;

    return match;
}

bool CWinEvaluation::spadesFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr)
{
    bool match = FALSE;

    if (evaluation_descriptor_ptr->flush_flag && evaluation_descriptor.flush_flag &&
        (evaluation_descriptor_ptr->suit_attr == SUIT_SPADES) &&
        (evaluation_descriptor.suit_attr == SUIT_SPADES) &&
        (!evaluation_descriptor_ptr->straight_flag) && (!evaluation_descriptor.straight_flag))
            match = TRUE;

    return match;
}

bool CWinEvaluation::blackFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr)
{
    bool match = FALSE;

    if (evaluation_descriptor_ptr->flush_flag && evaluation_descriptor.flush_flag &&
        (evaluation_descriptor_ptr->suit_attr == SUIT_BLACK) &&
        ((evaluation_descriptor.suit_attr == SUIT_CLUBS) || (evaluation_descriptor.suit_attr == SUIT_SPADES)) &&
        (!evaluation_descriptor_ptr->straight_flag) && (!evaluation_descriptor.straight_flag))
            match = TRUE;

    return match;
}

bool CWinEvaluation::redFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr)
{
    bool match = FALSE;

    if (evaluation_descriptor_ptr->flush_flag && evaluation_descriptor.flush_flag &&
        (evaluation_descriptor_ptr->suit_attr == SUIT_RED) &&
        ((evaluation_descriptor.suit_attr == SUIT_HEARTS) || (evaluation_descriptor.suit_attr == SUIT_DIAMONDS)) &&
        (!evaluation_descriptor_ptr->straight_flag) && (!evaluation_descriptor.straight_flag))
            match = TRUE;

    return match;
}

bool CWinEvaluation::anyFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr)
{
    bool match = FALSE;

    if (evaluation_descriptor_ptr->flush_flag && evaluation_descriptor.flush_flag &&
        (evaluation_descriptor_ptr->suit_attr == MAX_CARD_SUIT) &&
        ((evaluation_descriptor.suit_attr == SUIT_HEARTS) || (evaluation_descriptor.suit_attr == SUIT_DIAMONDS) ||
         (evaluation_descriptor.suit_attr == SUIT_CLUBS) || (evaluation_descriptor.suit_attr == SUIT_SPADES)) &&
        (!evaluation_descriptor_ptr->straight_flag) && (!evaluation_descriptor.straight_flag))
            match = TRUE;

    return match;
}

bool CWinEvaluation::flushFound (void)
{
    bool flush_found = FALSE;
    int i;

    for (i=0; i < MAX_CARD_SUIT; i++)
        if (suit_matrix[i] == 5)
            flush_found = TRUE;
    
    return flush_found;
}

char CWinEvaluation::getFlushSuit (void)
{
    int i;
    char suit = 0;

    for (i=0; i < MAX_CARD_SUIT; i++)
        if (suit_matrix[i] == 5)
            suit = i;
    
    return suit;
}

// returns the highest rank count
int CWinEvaluation::getPrimaryRankCount (void)
{
    int i;
    int high_count = 0;

    for (i=0; i < MAX_CARD_RANK + RANK_INDEX_VALUE_OFFSET; i++)
        if (rank_matrix[i] > high_count)
            high_count = rank_matrix[i];

    return high_count;
}
/*
// returns the next highest rank count
int CWinEvaluation::getAuxilaryRankCount (void)
{
    int i;
    int aux_count = 0;
    int high_count = getPrimaryRankCount();
    bool high_count_found = FALSE;

    for (i=0; i < MAX_CARD_RANK + RANK_INDEX_VALUE_OFFSET; i++)
    {
        if (!high_count_found && (rank_matrix[i] == high_count))
            high_count_found = TRUE;
        else if (rank_matrix[i] > aux_count)
            aux_count = rank_matrix[i];
    }        

    return aux_count;
}
*/

// returns the next highest rank count
int CWinEvaluation::getAuxilaryRankCount (void)
{
    int i;
    int aux_count = 1;
    int high_count = getPrimaryRankCount();

    for (i=0; i < MAX_CARD_RANK + RANK_INDEX_VALUE_OFFSET; i++)
    {
        if ((rank_matrix[i] < high_count) && (rank_matrix[i] > aux_count))
            aux_count = rank_matrix[i];
    }        

    return aux_count;
}

/*
// returns the next highest rank count
int CWinEvaluation::getAuxilaryRankCount (void)
{
    int i;
    int highest_rank;
    int aux_count = 0;
    bool high_count_found = FALSE;

    highest_rank = 0;

    // find the next highest rank
    for (i=(MAX_CARD_RANK + RANK_INDEX_VALUE_OFFSET - 1); i > 0 ; i--)
    {
        if (rank_matrix[i])
        {
            if (!highest_rank)
                highest_rank = rank_matrix[i];
            else if (rank_matrix[i] > highest_rank)
                highest_rank = rank_matrix[i];
            else
            {
                aux_count = rank_matrix[i];
                break;
            }
        }
    }        

    return aux_count;
}
*/
bool CWinEvaluation::straightFound (void)
{
    bool straight_found = FALSE;
    int i;
    int consecutives = 0;

    for (i=0; i < MAX_CARD_RANK + RANK_INDEX_VALUE_OFFSET; i++)
    {
        if (rank_matrix[i])
            consecutives++;
        else    
            consecutives = 0;

        if (consecutives == 5)
        {
            straight_found = TRUE;
            break;
        }
    }        

    // check for low straight possibility
    if (!straight_found && rank_matrix[RANK_ACE + RANK_INDEX_VALUE_OFFSET])
    {
        consecutives = 0;

        for (i = RANK_TWO + RANK_INDEX_VALUE_OFFSET; i < RANK_SIX + RANK_INDEX_VALUE_OFFSET; i++)
        {
            if (rank_matrix[i])
                consecutives++;
            else    
                consecutives = 0;
        }
        if (consecutives == 4)
        {
            straight_found = TRUE;
        }
    }

    return straight_found;
}

BYTE CWinEvaluation::getFaceCardRankCount (void)
{
    int i;
    BYTE face_card_count = 0;

    for (i=0; i < MAX_CARD_RANK + RANK_INDEX_VALUE_OFFSET; i++)
        if (rank_matrix[i] && (i <= (RANK_KING + RANK_INDEX_VALUE_OFFSET)) && (i >= (RANK_JACK + RANK_INDEX_VALUE_OFFSET)))
            face_card_count += rank_matrix[i];

    return face_card_count;
}

char CWinEvaluation::getRankLimit (void)
{
    char rank_limit = 0;

    if (evaluation_descriptor.flush_flag && evaluation_descriptor.straight_flag)
        rank_limit = getLowestRank();
    else if (isFourOfAKind())
        rank_limit = getFourOfAKindRank();
    else if (isFullHouse())
        rank_limit = getThreeOfAKindRank();
    else if (isThreeOfAKind())
        rank_limit = getThreeOfAKindRank();
    
    return rank_limit;
}

char CWinEvaluation::getRankMax (void)
{
    char rank_max = 0;

    if (evaluation_descriptor.flush_flag && evaluation_descriptor.straight_flag)
        rank_max = getHighestRank();
    else if (isFourOfAKind())
        rank_max = getFourOfAKindRank();
    else if (isFullHouse())
        rank_max = getThreeOfAKindRank();
    else if (isThreeOfAKind())
        rank_max = getThreeOfAKindRank();
    
    return rank_max;
}

char CWinEvaluation::getLowestRank (void)
{
    char i;
    char low_rank = 0;
    
    for (i=0; i < MAX_CARD_RANK + RANK_INDEX_VALUE_OFFSET; i++)
        if (rank_matrix[i])
        {
//            low_rank = i + RANK_INDEX_VALUE_OFFSET;   ???
            low_rank = i;
            break;
        }

    return low_rank;
}

char CWinEvaluation::getHighestRank (void)
{
    char i;
    char high_rank = 0;
    
    for (i=(MAX_CARD_RANK + RANK_INDEX_VALUE_OFFSET); i > 0 ; i--)
        if (rank_matrix[i])
        {
//            high_rank = i + RANK_INDEX_VALUE_OFFSET;
            high_rank = i;
            break;
        }

    return high_rank;
}

char CWinEvaluation::getPairRank (void)
{
    char i;
    char pair_rank = 0;
    
    for (i=0; i < MAX_CARD_RANK + RANK_INDEX_VALUE_OFFSET; i++)
        if (rank_matrix[i] == 2)
        {
            pair_rank = i;
            break;
        }

    return pair_rank;
}

char CWinEvaluation::getFourOfAKindRank (void)
{
    char i;
    char fok_rank = 0;
    
    for (i=0; i < MAX_CARD_RANK + RANK_INDEX_VALUE_OFFSET; i++)
        if (rank_matrix[i] == 4)
        {
            fok_rank = i;
            break;
        }

    return fok_rank;
}

char CWinEvaluation::getThreeOfAKindRank (void)
{
    char i;
    char tok_rank = 0;
    
    for (i=0; i < MAX_CARD_RANK + RANK_INDEX_VALUE_OFFSET; i++)
        if (rank_matrix[i] == 3)
        {
            tok_rank = i;
            break;
        }

    return tok_rank;
}

bool CWinEvaluation::royalFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr)
{
    bool match = FALSE;

    // check for royal flush
    if (evaluation_descriptor_ptr->flush_flag && evaluation_descriptor.flush_flag &&
        evaluation_descriptor_ptr->straight_flag && evaluation_descriptor.straight_flag &&
        (evaluation_descriptor_ptr->rank_limit == 0x0A) && (evaluation_descriptor.rank_limit == 0x0A))
    {
        if (evaluation_descriptor_ptr->suit_attr == MAX_CARD_SUIT)
            match = TRUE;
        else
        {
            switch (evaluation_descriptor_ptr->suit_attr)
            {
                case SUIT_CLUBS:
                    if (evaluation_descriptor.suit_attr == SUIT_CLUBS)
                        match = TRUE;
                    break;
                case SUIT_HEARTS:
                    if (evaluation_descriptor.suit_attr == SUIT_HEARTS)
                        match = TRUE;
                    break;
                case SUIT_DIAMONDS:
                    if (evaluation_descriptor.suit_attr == SUIT_DIAMONDS)
                        match = TRUE;
                    break;
                case SUIT_SPADES:
                    if (evaluation_descriptor.suit_attr == SUIT_SPADES)
                        match = TRUE;
                    break;
                default :
                    match = TRUE;
                    break;
            }
        }
    }        

    return match;
}

bool CWinEvaluation::straightFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr)
{
    bool match = FALSE;

    // check for royal flush
    if (evaluation_descriptor_ptr->flush_flag && evaluation_descriptor.flush_flag &&
        evaluation_descriptor_ptr->straight_flag && evaluation_descriptor.straight_flag &&
//        evaluation_descriptor_ptr->rank_limit < 0x0A)  // don't count royal flushes
        (evaluation_descriptor_ptr->rank_limit < 0x0A) && (evaluation_descriptor.rank_limit < 0x0A))
    {
        if (evaluation_descriptor_ptr->suit_attr == MAX_CARD_SUIT)
            match = TRUE;
        else
        {
            switch (evaluation_descriptor_ptr->suit_attr)
            {
                case SUIT_CLUBS:
                    if (evaluation_descriptor.suit_attr == SUIT_CLUBS)
                        match = TRUE;
                    break;
                case SUIT_HEARTS:
                    if (evaluation_descriptor.suit_attr == SUIT_HEARTS)
                        match = TRUE;
                    break;
                case SUIT_DIAMONDS:
                    if (evaluation_descriptor.suit_attr == SUIT_DIAMONDS)
                        match = TRUE;
                    break;
                case SUIT_SPADES:
                    if (evaluation_descriptor.suit_attr == SUIT_SPADES)
                        match = TRUE;
                    break;
                default :
                    match = TRUE;
                    break;
            }
        }
    }        

    return match;
}

bool CWinEvaluation::straightFound (const EvaluationDescriptor* evaluation_descriptor_ptr)
{
    bool match = FALSE;

    if ((evaluation_descriptor_ptr->straight_flag && evaluation_descriptor.straight_flag) &&
        !evaluation_descriptor_ptr->flush_flag && !evaluation_descriptor.flush_flag)
        match = TRUE;

    return match;
}

bool CWinEvaluation::fourOfKindFound (const EvaluationDescriptor* evaluation_descriptor_ptr)
{
    bool four_of_kind = FALSE;
    
    if ((evaluation_descriptor_ptr->primary_rank_attr == 4) && (evaluation_descriptor.primary_rank_attr == 4) &&
        (evaluation_descriptor_ptr->auxilary_rank_attr == 1) && (evaluation_descriptor.auxilary_rank_attr == 1))
    {
        if ((evaluation_descriptor.rank_limit >= evaluation_descriptor_ptr->rank_limit) &&
            (evaluation_descriptor.rank_max <= evaluation_descriptor_ptr->rank_max)) 
            four_of_kind = TRUE;
    }
                
    return four_of_kind;
}

bool CWinEvaluation::faceCardsFullHouseFound (const EvaluationDescriptor* evaluation_descriptor_ptr)
{
    bool face_card_full_house = FALSE;

    if ((evaluation_descriptor_ptr->primary_rank_attr == 3) && (evaluation_descriptor.primary_rank_attr == 3) &&
        (evaluation_descriptor_ptr->auxilary_rank_attr == 2) && (evaluation_descriptor.auxilary_rank_attr == 2) &&
        (evaluation_descriptor_ptr->face_card_count == 5) && (evaluation_descriptor.face_card_count == 5))
    {
        if ((evaluation_descriptor.rank_limit >= evaluation_descriptor_ptr->rank_limit) &&
            (evaluation_descriptor.rank_max <= evaluation_descriptor_ptr->rank_max)) 
            face_card_full_house = TRUE;
    }
                
    return face_card_full_house;
}

bool CWinEvaluation::fullHouseFound (const EvaluationDescriptor* evaluation_descriptor_ptr)
{
    bool full_house = FALSE;
    
    if ((evaluation_descriptor_ptr->primary_rank_attr == 3) && (evaluation_descriptor.primary_rank_attr == 3) &&
        (evaluation_descriptor_ptr->auxilary_rank_attr == 2) && (evaluation_descriptor.auxilary_rank_attr == 2) &&
        (evaluation_descriptor_ptr->face_card_count != 5) )
    {
        if ((evaluation_descriptor.rank_limit >= evaluation_descriptor_ptr->rank_limit) &&
            (evaluation_descriptor.rank_max <= evaluation_descriptor_ptr->rank_max)) 
            full_house = TRUE;
    }
                
    return full_house;
}

bool CWinEvaluation::threeOfKindFound (const EvaluationDescriptor* evaluation_descriptor_ptr)
{
    bool three_of_kind = FALSE;
    
    if ((evaluation_descriptor_ptr->primary_rank_attr == 3) && (evaluation_descriptor.primary_rank_attr == 3) &&
        (evaluation_descriptor_ptr->auxilary_rank_attr == 1) && (evaluation_descriptor.auxilary_rank_attr == 1))
    {
        if ((evaluation_descriptor.rank_limit >= evaluation_descriptor_ptr->rank_limit) &&
            (evaluation_descriptor.rank_max <= evaluation_descriptor_ptr->rank_max)) 
            three_of_kind = TRUE;
    }
                
    return three_of_kind;
}

bool CWinEvaluation::isOnePair (void)
{
    bool one_pair = FALSE;
    
    if ((evaluation_descriptor.primary_rank_attr == 2) &&
        (evaluation_descriptor.auxilary_rank_attr == 1) &&
        (evaluation_descriptor.flush_flag == FALSE) &&
        (evaluation_descriptor.straight_flag == FALSE))
            one_pair = TRUE;
            
    return one_pair;
}

bool CWinEvaluation::isFourOfAKind (void)
{
    bool four_of_kind = FALSE;
    
    if ((evaluation_descriptor.primary_rank_attr == 4) &&
        (evaluation_descriptor.auxilary_rank_attr == 1) &&
        (evaluation_descriptor.flush_flag == FALSE) &&
        (evaluation_descriptor.straight_flag == FALSE))
            four_of_kind = TRUE;
            
    return four_of_kind;
}

bool CWinEvaluation::isFullHouse (void)
{
    bool full_house = FALSE;
    
    if ((evaluation_descriptor.primary_rank_attr == 3) &&
        (evaluation_descriptor.auxilary_rank_attr == 2) &&
        (evaluation_descriptor.flush_flag == FALSE) &&
        (evaluation_descriptor.straight_flag == FALSE))
            full_house = TRUE;
            
    return full_house;
}

bool CWinEvaluation::isThreeOfAKind (void)
{
    bool three_of_kind = FALSE;
    
    if ((evaluation_descriptor.primary_rank_attr == 3) &&
        (evaluation_descriptor.auxilary_rank_attr == 1) &&
        (evaluation_descriptor.flush_flag == FALSE) &&
        (evaluation_descriptor.straight_flag == FALSE))
            three_of_kind = TRUE;
                
    return three_of_kind;
}
