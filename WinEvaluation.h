/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	12/05/13	Added black flush, red flush, any flush, and any straight win types to the GBA win awards.
Robert Fuller	07/04/13	Added ability to award cashable and non-cashable credits for GBA based wins.
Robert Fuller   03/08/13    Added cashable and promotional credit fields to the GBA win category for future use.
Robert Fuller   06/01/12    Added code to communicate with the Interactive Display Controller.
Robert Fuller   06/09/09    Added code to award GB Advantage wins to non-logged in players depending on configuration selection.

*/

#pragma once

#include "afxwin.h"
//#include "PokerPaytable.h"
#include "WinEvaluation.h"
#include "Utility.h"

#pragma pack(1)
typedef struct
{
    // helps determine royal flush, 5ok, 4ok, 3ok, full house, 1 pair, 2 pair
    char primary_rank_attr;
    // helps determine royal flush, full house, two pair, 1 pair
    char auxilary_rank_attr;
    // helps determine type of flush
    BYTE suit_attr;
    char flush_flag;  // TRUE if pay category contains a flush
    char straight_flag;  // TRUE if pay category contains a straight
    BYTE face_card_count;  // number of face cards in hand
    char rank_limit;  // used to determine minimum pay constraints for a honor pair
    char rank_max;  // used to determine maximum pay constraints for a honor pair
} EvaluationDescriptor;
#pragma pack()


#pragma pack(1)
// GB Pro Win evaluation structs
struct GbProWinCategory
{
	DWORD points_award;
	DWORD cash_voucher_award;
//	DWORD number_of_mdts;
	BYTE  gb_login_not_required; // non-zero means that GB login is NOT required for promo eligibility
	BYTE  print_drawing_ticket; // non-zero means that GB login is NOT required for promo eligibility
	BYTE  scalable_award; // scalable if non-zero
	BYTE  i_rewards_paid_win;
	DWORD start_time;
	DWORD end_time;
	DWORD minimum_money_bet;
	WORD  minimum_denomination;
	BYTE  minimum_bet;
	BYTE  promotion_code;
	const EvaluationDescriptor* evaluation_descriptor;
	char  media_string [MAX_MEDIA_STRING_LENGTH];
	char  drawing_ticket_string [30];
	DWORD cashable_credits;
	DWORD noncashable_credits;
};
#pragma pack()

#pragma pack(1)
// GB Pro Win evaluation structs
struct IdcGbAdvantageWinCategory
{
	DWORD points_award;
	DWORD cash_voucher_award;
//	DWORD number_of_mdts;
	BYTE  gb_login_not_required; // non-zero means that GB login is NOT required for promo eligibility
	BYTE  print_drawing_ticket; // non-zero means that GB login is NOT required for promo eligibility
	BYTE  scalable_award; // scalable if non-zero
	BYTE  i_rewards_paid_win;
	DWORD start_time;
	DWORD end_time;
	DWORD minimum_money_bet;
	WORD  minimum_denomination;
	BYTE  minimum_bet;
	BYTE  promotion_code;
	DWORD cashable_credits;
	DWORD noncashable_credits;
};
#pragma pack()

#pragma pack(1)
// GB Pro Win evaluation structs
struct DrawingTicketText
{
	BYTE	promotion_code;
	char	drawing_ticket[30];
};
#pragma pack()

//const long NUMBER_OF_WIN_CATEGORIES = 52;
//const long NUMBER_OF_WIN_CATEGORIES = 61;
const long NUMBER_OF_WIN_CATEGORIES = 65;
const BYTE WIN_INDEX_NOT_FOUND = 0xFF;


enum CARD_RANK
{
	RANK_TWO,
	RANK_THREE,
	RANK_FOUR,
	RANK_FIVE,
	RANK_SIX,
	RANK_SEVEN,
	RANK_EIGHT,
	RANK_NINE,
	RANK_TEN,
	RANK_JACK,
	RANK_QUEEN,
	RANK_KING,
	RANK_ACE,
	MAX_CARD_RANK
};

enum CARD_SUIT
{
	SUIT_CLUBS,
	SUIT_DIAMONDS,
	SUIT_HEARTS,
	SUIT_SPADES,
	MAX_CARD_SUIT,
	SUIT_BLACK,
	SUIT_RED
};

#define RANK_INDEX_VALUE_OFFSET   0x02



class CWinEvaluation  
{
public:
	CWinEvaluation();
	virtual ~CWinEvaluation();

	void evaluateHand (unsigned char* cards);
	bool checkForWin (GbProWinCategory* gbpro_win_category);
	bool fourOfKindFound (const EvaluationDescriptor* evaluation_descriptor_ptr);

private:
	char hand_matrix[MAX_CARD_SUIT][MAX_CARD_RANK + RANK_INDEX_VALUE_OFFSET];	/* matrix containing players cards */
	char suit_matrix[MAX_CARD_SUIT];
	char rank_matrix[MAX_CARD_RANK + RANK_INDEX_VALUE_OFFSET];
	EvaluationDescriptor evaluation_descriptor;

	void fillEvaluationMatrixes (unsigned char* cards);
	void initEvaluationMatrixes (void);
	void fillEvaluationDescriptor (void);
	char getSuitAttr (void);
	bool flushFound (void);
	char getFlushSuit (void);
	int getPrimaryRankCount (void);
	int getAuxilaryRankCount (void);
	BYTE getFaceCardRankCount (void);
	bool straightFound (void);
	char getRankLimit (void);
	char getRankMax (void);
	char getLowestRank (void);
	char getHighestRank (void);
	char getPairRank (void);
	char getFourOfAKindRank (void);
	char getThreeOfAKindRank (void);
	bool isOnePair (void);
	bool isFourOfAKind (void);
	bool isFullHouse (void);
	bool isThreeOfAKind (void);
	bool royalFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr);
	bool straightFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr);
	bool fullHouseFound (const EvaluationDescriptor* evaluation_descriptor_ptr);
	bool faceCardsFullHouseFound (const EvaluationDescriptor* evaluation_descriptor_ptr);
	bool clubsFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr);
	bool diamondsFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr);
	bool heartsFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr);
	bool spadesFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr);
	bool blackFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr);
	bool redFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr);
	bool anyFlushFound (const EvaluationDescriptor* evaluation_descriptor_ptr);
	bool straightFound (const EvaluationDescriptor* evaluation_descriptor_ptr);
	bool threeOfKindFound (const EvaluationDescriptor* evaluation_descriptor_ptr);

//	friend class CPokerDlg;
};
