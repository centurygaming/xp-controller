#if !defined( _AES_H )
#define _AES_H

/*  This include is used to find 8 & 32 bit unsigned integer types  */
#include "limits.h"

#if defined(__cplusplus)
extern "C"
{
#endif

/* The following must also be set in assembler files if being used  */

#define aes_08t unsigned char
#define aes_32t unsigned int

// Stratus encryption/decryption functions
extern aes_32t stratus_encryption_key_schedule[44];
extern aes_32t stratus_decryption_key_schedule[44];
void StratusEncryptionKey();
void StratusDecryptionKey();
void StratusEncryptData(const unsigned char *in_blk, unsigned char *out_blk);
void StratusDecryptData(const unsigned char *in_blk,  unsigned char *out_blk);

// machine encryption/decryption functions
extern aes_32t machine_encryption_key_schedule[44];
extern aes_32t machine_decryption_key_schedule[44];
void MachineEncryptionKey();
void MachineDecryptionKey();
void MachineEncryptData(const unsigned char *in_blk, unsigned char *out_blk);
void MachineDecryptData(const unsigned char *in_blk,  unsigned char *out_blk);

#if defined(__cplusplus)
}
#endif

#endif
