#include "aesopt.h"
#include "aestab.h"

#if defined(__cplusplus)
extern "C"
{
#endif

aes_32t stratus_encryption_key_schedule[44];
aes_32t stratus_decryption_key_schedule[44];
aes_32t machine_encryption_key_schedule[44];
aes_32t machine_decryption_key_schedule[44];


#define ke4(k,i) \
{   k[4*(i)+4] = ss[0] ^= ls_box(ss[3],3) ^ t_use(r,c)[i]; k[4*(i)+5] = ss[1] ^= ss[0]; \
    k[4*(i)+6] = ss[2] ^= ss[1]; k[4*(i)+7] = ss[3] ^= ss[2]; \
}
#define kel4(k,i) \
{   k[4*(i)+4] = ss[0] ^= ls_box(ss[3],3) ^ t_use(r,c)[i]; k[4*(i)+5] = ss[1] ^= ss[0]; \
    k[4*(i)+6] = ss[2] ^= ss[1]; k[4*(i)+7] = ss[3] ^= ss[2]; \
}

#define ke6(k,i) \
{   k[6*(i)+ 6] = ss[0] ^= ls_box(ss[5],3) ^ t_use(r,c)[i]; k[6*(i)+ 7] = ss[1] ^= ss[0]; \
    k[6*(i)+ 8] = ss[2] ^= ss[1]; k[6*(i)+ 9] = ss[3] ^= ss[2]; \
    k[6*(i)+10] = ss[4] ^= ss[3]; k[6*(i)+11] = ss[5] ^= ss[4]; \
}
#define kel6(k,i) \
{   k[6*(i)+ 6] = ss[0] ^= ls_box(ss[5],3) ^ t_use(r,c)[i]; k[6*(i)+ 7] = ss[1] ^= ss[0]; \
    k[6*(i)+ 8] = ss[2] ^= ss[1]; k[6*(i)+ 9] = ss[3] ^= ss[2]; \
}

#define ke8(k,i) \
{   k[8*(i)+ 8] = ss[0] ^= ls_box(ss[7],3) ^ t_use(r,c)[i]; k[8*(i)+ 9] = ss[1] ^= ss[0]; \
    k[8*(i)+10] = ss[2] ^= ss[1]; k[8*(i)+11] = ss[3] ^= ss[2]; \
    k[8*(i)+12] = ss[4] ^= ls_box(ss[3],0); k[8*(i)+13] = ss[5] ^= ss[4]; \
    k[8*(i)+14] = ss[6] ^= ss[5]; k[8*(i)+15] = ss[7] ^= ss[6]; \
}
#define kel8(k,i) \
{   k[8*(i)+ 8] = ss[0] ^= ls_box(ss[7],3) ^ t_use(r,c)[i]; k[8*(i)+ 9] = ss[1] ^= ss[0]; \
    k[8*(i)+10] = ss[2] ^= ss[1]; k[8*(i)+11] = ss[3] ^= ss[2]; \
}

void StratusEncryptionKey()
{
 aes_32t ss[4];
 unsigned char encryption_key[16] = {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c};

	stratus_encryption_key_schedule[0] = ss[0] = word_in(encryption_key, 0);
	stratus_encryption_key_schedule[1] = ss[1] = word_in(encryption_key, 1);
	stratus_encryption_key_schedule[2] = ss[2] = word_in(encryption_key, 2);
	stratus_encryption_key_schedule[3] = ss[3] = word_in(encryption_key, 3);

	ke4(stratus_encryption_key_schedule, 0);
	ke4(stratus_encryption_key_schedule, 1);
	ke4(stratus_encryption_key_schedule, 2);
	ke4(stratus_encryption_key_schedule, 3);
	ke4(stratus_encryption_key_schedule, 4);
	ke4(stratus_encryption_key_schedule, 5);
	ke4(stratus_encryption_key_schedule, 6);
	ke4(stratus_encryption_key_schedule, 7);
	ke4(stratus_encryption_key_schedule, 8);
	kel4(stratus_encryption_key_schedule, 9);
}

void MachineEncryptionKey()
{
 aes_32t ss[4];
 unsigned char encryption_key[16] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0xfe, 0xdc, 0xba, 0x98, 0x76, 0x54, 0x32, 0x10};

	machine_encryption_key_schedule[0] = ss[0] = word_in(encryption_key, 0);
	machine_encryption_key_schedule[1] = ss[1] = word_in(encryption_key, 1);
	machine_encryption_key_schedule[2] = ss[2] = word_in(encryption_key, 2);
	machine_encryption_key_schedule[3] = ss[3] = word_in(encryption_key, 3);

	ke4(machine_encryption_key_schedule, 0);
	ke4(machine_encryption_key_schedule, 1);
	ke4(machine_encryption_key_schedule, 2);
	ke4(machine_encryption_key_schedule, 3);
	ke4(machine_encryption_key_schedule, 4);
	ke4(machine_encryption_key_schedule, 5);
	ke4(machine_encryption_key_schedule, 6);
	ke4(machine_encryption_key_schedule, 7);
	ke4(machine_encryption_key_schedule, 8);
	kel4(machine_encryption_key_schedule, 9);
}


#define ff(x)   inv_mcol(x)

#define kdf4(k,i) \
{   ss[0] = ss[0] ^ ss[2] ^ ss[1] ^ ss[3]; ss[1] = ss[1] ^ ss[3]; ss[2] = ss[2] ^ ss[3]; ss[3] = ss[3]; \
    ss[4] = ls_box(ss[(i+3) % 4], 3) ^ t_use(r,c)[i]; ss[i % 4] ^= ss[4]; \
    ss[4] ^= k[4*(i)];   k[4*(i)+4] = ff(ss[4]); ss[4] ^= k[4*(i)+1]; k[4*(i)+5] = ff(ss[4]); \
    ss[4] ^= k[4*(i)+2]; k[4*(i)+6] = ff(ss[4]); ss[4] ^= k[4*(i)+3]; k[4*(i)+7] = ff(ss[4]); \
}
#define kd4(k,i) \
{   ss[4] = ls_box(ss[(i+3) % 4], 3) ^ t_use(r,c)[i]; ss[i % 4] ^= ss[4]; ss[4] = ff(ss[4]); \
    k[4*(i)+4] = ss[4] ^= k[4*(i)]; k[4*(i)+5] = ss[4] ^= k[4*(i)+1]; \
    k[4*(i)+6] = ss[4] ^= k[4*(i)+2]; k[4*(i)+7] = ss[4] ^= k[4*(i)+3]; \
}
#define kdl4(k,i) \
{   ss[4] = ls_box(ss[(i+3) % 4], 3) ^ t_use(r,c)[i]; ss[i % 4] ^= ss[4]; \
    k[4*(i)+4] = (ss[0] ^= ss[1]) ^ ss[2] ^ ss[3]; k[4*(i)+5] = ss[1] ^ ss[3]; \
    k[4*(i)+6] = ss[0]; k[4*(i)+7] = ss[1]; \
}

#define kdf6(k,i) \
{   ss[0] ^= ls_box(ss[5],3) ^ t_use(r,c)[i]; k[6*(i)+ 6] = ff(ss[0]); ss[1] ^= ss[0]; k[6*(i)+ 7] = ff(ss[1]); \
    ss[2] ^= ss[1]; k[6*(i)+ 8] = ff(ss[2]); ss[3] ^= ss[2]; k[6*(i)+ 9] = ff(ss[3]); \
    ss[4] ^= ss[3]; k[6*(i)+10] = ff(ss[4]); ss[5] ^= ss[4]; k[6*(i)+11] = ff(ss[5]); \
}
#define kd6(k,i) \
{   ss[6] = ls_box(ss[5],3) ^ t_use(r,c)[i]; \
    ss[0] ^= ss[6]; ss[6] = ff(ss[6]); k[6*(i)+ 6] = ss[6] ^= k[6*(i)]; \
    ss[1] ^= ss[0]; k[6*(i)+ 7] = ss[6] ^= k[6*(i)+ 1]; \
    ss[2] ^= ss[1]; k[6*(i)+ 8] = ss[6] ^= k[6*(i)+ 2]; \
    ss[3] ^= ss[2]; k[6*(i)+ 9] = ss[6] ^= k[6*(i)+ 3]; \
    ss[4] ^= ss[3]; k[6*(i)+10] = ss[6] ^= k[6*(i)+ 4]; \
    ss[5] ^= ss[4]; k[6*(i)+11] = ss[6] ^= k[6*(i)+ 5]; \
}
#define kdl6(k,i) \
{   ss[0] ^= ls_box(ss[5],3) ^ t_use(r,c)[i]; k[6*(i)+ 6] = ss[0]; ss[1] ^= ss[0]; k[6*(i)+ 7] = ss[1]; \
    ss[2] ^= ss[1]; k[6*(i)+ 8] = ss[2]; ss[3] ^= ss[2]; k[6*(i)+ 9] = ss[3]; \
}

#define kdf8(k,i) \
{   ss[0] ^= ls_box(ss[7],3) ^ t_use(r,c)[i]; k[8*(i)+ 8] = ff(ss[0]); ss[1] ^= ss[0]; k[8*(i)+ 9] = ff(ss[1]); \
    ss[2] ^= ss[1]; k[8*(i)+10] = ff(ss[2]); ss[3] ^= ss[2]; k[8*(i)+11] = ff(ss[3]); \
    ss[4] ^= ls_box(ss[3],0); k[8*(i)+12] = ff(ss[4]); ss[5] ^= ss[4]; k[8*(i)+13] = ff(ss[5]); \
    ss[6] ^= ss[5]; k[8*(i)+14] = ff(ss[6]); ss[7] ^= ss[6]; k[8*(i)+15] = ff(ss[7]); \
}
#define kd8(k,i) \
{   aes_32t g = ls_box(ss[7],3) ^ t_use(r,c)[i]; \
    ss[0] ^= g; g = ff(g); k[8*(i)+ 8] = g ^= k[8*(i)]; \
    ss[1] ^= ss[0]; k[8*(i)+ 9] = g ^= k[8*(i)+ 1]; \
    ss[2] ^= ss[1]; k[8*(i)+10] = g ^= k[8*(i)+ 2]; \
    ss[3] ^= ss[2]; k[8*(i)+11] = g ^= k[8*(i)+ 3]; \
    g = ls_box(ss[3],0); \
    ss[4] ^= g; g = ff(g); k[8*(i)+12] = g ^= k[8*(i)+ 4]; \
    ss[5] ^= ss[4]; k[8*(i)+13] = g ^= k[8*(i)+ 5]; \
    ss[6] ^= ss[5]; k[8*(i)+14] = g ^= k[8*(i)+ 6]; \
    ss[7] ^= ss[6]; k[8*(i)+15] = g ^= k[8*(i)+ 7]; \
}
#define kdl8(k,i) \
{   ss[0] ^= ls_box(ss[7],3) ^ t_use(r,c)[i]; k[8*(i)+ 8] = ss[0]; ss[1] ^= ss[0]; k[8*(i)+ 9] = ss[1]; \
    ss[2] ^= ss[1]; k[8*(i)+10] = ss[2]; ss[3] ^= ss[2]; k[8*(i)+11] = ss[3]; \
}

void StratusDecryptionKey()
{
 aes_32t ss[5];
 unsigned char decryption_key[16] = {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c};

	stratus_decryption_key_schedule[0] = ss[0] = word_in(decryption_key, 0);
	stratus_decryption_key_schedule[1] = ss[1] = word_in(decryption_key, 1);
	stratus_decryption_key_schedule[2] = ss[2] = word_in(decryption_key, 2);
	stratus_decryption_key_schedule[3] = ss[3] = word_in(decryption_key, 3);

	kdf4(stratus_decryption_key_schedule, 0);
	kd4(stratus_decryption_key_schedule, 1);
	kd4(stratus_decryption_key_schedule, 2);
	kd4(stratus_decryption_key_schedule, 3);
	kd4(stratus_decryption_key_schedule, 4);
	kd4(stratus_decryption_key_schedule, 5);
	kd4(stratus_decryption_key_schedule, 6);
	kd4(stratus_decryption_key_schedule, 7);
	kd4(stratus_decryption_key_schedule, 8);
	kdl4(stratus_decryption_key_schedule, 9);
}

void MachineDecryptionKey()
{
 aes_32t ss[5];
 unsigned char decryption_key[16] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0xfe, 0xdc, 0xba, 0x98, 0x76, 0x54, 0x32, 0x10};

	machine_decryption_key_schedule[0] = ss[0] = word_in(decryption_key, 0);
	machine_decryption_key_schedule[1] = ss[1] = word_in(decryption_key, 1);
	machine_decryption_key_schedule[2] = ss[2] = word_in(decryption_key, 2);
	machine_decryption_key_schedule[3] = ss[3] = word_in(decryption_key, 3);

	kdf4(machine_decryption_key_schedule, 0);
	kd4(machine_decryption_key_schedule, 1);
	kd4(machine_decryption_key_schedule, 2);
	kd4(machine_decryption_key_schedule, 3);
	kd4(machine_decryption_key_schedule, 4);
	kd4(machine_decryption_key_schedule, 5);
	kd4(machine_decryption_key_schedule, 6);
	kd4(machine_decryption_key_schedule, 7);
	kd4(machine_decryption_key_schedule, 8);
	kdl4(machine_decryption_key_schedule, 9);
}

#if defined(__cplusplus)
}
#endif
