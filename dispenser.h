/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller	05/02/12	Added support for the 4 cassette Puloon dispenser
Robert Fuller   03/08/13	Added code to handle Puloon dispense errors.
Robert Fuller   09/14/12    Added code to facilitate external fill operations initiated by location owners.
Robert Fuller   06/28/10    Added code to keep track of the last impress amount. 
Robert Fuller   10/27/10    Use the upper and lower Puloon dispense command to avoid unnecessary cashier dispenser interaction.
Robert Fuller   06/10/10    Added code to support the new $5 $20 LCDM-2000 configuration. 
Robert Fuller   06/02/10    Added code to support the new Puloon LCDM-2000 dual denomination dispenser. 
Robert Fuller   03/11/10    Fixed problem with Puloon dispenses over 69 bills.
Robert Fuller   01/19/10    Added code to support the new Puloon dispenser.
Robert Fuller   10/30/09    When the JCM dispenser has a problem during a dispense, alert the cashier to service the dispenser,
                            and print problem on lat tape, then write the ticket to the paid ticket file.
Robert Fuller   05/02/08    Added code to support CD2000 dispenser.
Robert Fuller   11/05/07    Added support for Diebold dispenser.
Dave Elquist    04/14/05    Initial Revision.
*/


#pragma once


// generic dispenser commands
#define NULL_COMMAND                0x00    // selected at random

// cash drawer dispenser commands
#define OPEN_CASH_DRAWER            0x02    // selected at random

// JCM dispenser commands
#define JCM_POLL_COMMAND            0x01    // selected at random
#define JCM_ERROR_CONDITION         0x37    // selected at random
#define JCM_BILLS_REMOVED           0x38    // selected at random
#define JCM_SELECT_DISPENSER        0x39    // selected at random
#define JCM_POLL_RESPONSE           0x40
#define JCM_DISPENSER_RESET         0x41
#define JCM_DISPENSER_INITIALIZE    0x42
#define JCM_DISPENSER_ID            0x43
#define JCM_COUNTING                0x44    // not used
#define JCM_DISPENSING              0x45    // not used
#define JCM_REJECT                  0x46
#define JCM_COUNTING_AND_DISPENSING 0x47

// ECASH dispenser commands
#define ECASH_INITIALIZATION                            'I'
#define ECASH_SENSOR_POLLING                            'S'
#define ECASH_CDU_VERSION_READ                          'V'
#define ECASH_DISPENSING                                'D'
#define ECASH_TEST_DISPENSING                           'R'
#define ECASH_NOTE_INDEX_SETTING                        'P'
#define ECASH_NATION_AND_NUMBER_OF_INSTALLED_CASSETTES  'M'

// CD2000 dispenser commands
#define CD2000_INITIALIZATION                            'I'
#define CD2000_SENSOR_POLLING                            'S'
#define CD2000_DISPENSING                                'D'
#define CD2000_TEST_DISPENSING                           'R'

// DIEBOLD dispenser commands
#define DIEBOLD_DISPENSING                              'D'
#define DISPENSE_BILLS									'B'
#define DISP_HEADER                 					0xFB

// PULOON dispenser commands
#define PULOON_PURGE									0x44
#define PULOON_PURGE_4000								0x51
#define PULOON_UPPER_DISPENSE							0x45
#define PULOON_DISPENSE_4000							0x52
#define PULOON_LOWER_DISPENSE							0x55
#define PULOON_UPPER_AND_LOWER_DISPENSE					0x56
#define PULOON_STATUS									0x46
#define PULOON_STATUS_4000								0x50
#define PULOON_ROM_VERSION								0x47
#define PULOON_TEST_UPPER_DISPENSE						0x76

// dispenser serial constants
#define SOH     0x01    // Start Of Header character
#define STX     0x02    // Start of TeXt character
#define ETX     0x03    // End of TeXt character
#define EOT     0x04    // End Of Transmission
#define ENQ     0x05    // ENQuiry character
#define ACK     0x06    // ACKnowledgement character
#define DC1     0x11    // DC1 character
#define NAK     0x15    // Not AcKnowledged character

// Puloon dispenser id
#define PULOON_COMMUNICATION_ID   			0x50
#define PULOON_COMMUNICATION_ID_4000		0x30

// Puloon dispenser response message sizes
#define PULOON_PURGE_MESSAGE_SIZE							6
#define PULOON_UPPER_DISPENSE_MESSAGE_SIZE					8
#define PULOON_LOWER_DISPENSE_MESSAGE_SIZE					8
#define PULOON_UPPER_AND_LOWER_DISPENSE_MESSAGE_SIZE		10
#define PULOON_TEST_UPPER_DISPENSE_MESSAGE_SIZE				6
#define PULOON_STATUS_MESSAGE_SIZE							6
#define PULOON_ROM_VERSION_MESSAGE_SIZE						6
#define PULOON_4000_DISPENSE_MESSAGE_SIZE					21

#define PULOON_PURGE_RESPONSE_MESSAGE_SIZE							7
#define PULOON_4000_PURGE_RESPONSE_MESSAGE_SIZE						20
#define PULOON_UPPER_DISPENSE_RESPONSE_MESSAGE_SIZE					14
#define PULOON_LOWER_DISPENSE_RESPONSE_MESSAGE_SIZE					14
#define PULOON_UPPER_AND_LOWER_DISPENSE_RESPONSE_MESSAGE_SIZE		21
#define PULOON_4000_DISPENSE_RESPONSE_MESSAGE_SIZE					29
#define PULOON_TEST_UPPER_DISPENSE_RESPONSE_MESSAGE_SIZE 			14
#define PULOON_STATUS_RESPONSE_MESSAGE_SIZE							10
#define PULOON_4000_STATUS_RESPONSE_MESSAGE_SIZE					24
#define PULOON_ROM_VERSION_RESPONSE_MESSAGE_SIZE					14

// JCM dispenser addressing constants
#define JCM_POLL_SA 0x4F    // station address byte for poll message
#define JCM_SEL_SA  0x3F    // station address byte for select message
#define JCM_UA      0x01    // unit address byte

#define DISPENSER_CRC(crc, data) crc ^= data
#define MAXIMUM_DISPENSER_RECEIVE 100

#define ECASH_COMMAND_TIMEOUT_IN_SECONDS	30	// default was 180
#define CD2000_COMMAND_TIMEOUT_IN_SECONDS	30	// default was 180

#pragma pack(1)
struct GeneralDispenserSettings
{
    BYTE    last_dispenser_command;
    bool    response_received;
    time_t  last_dispenser_poll_time;
    bool    ack_received;
    BYTE    nack_count;
};

struct EcashDispenserSettings
{
    GeneralDispenserSettings    sub;
    BYTE message_status;
    BYTE sensor_status[13];
    BYTE error_code[5];
    BYTE country_code;
    BYTE dispenser_mode[3];
    BYTE number_of_cassettes;
    BYTE version[6];
    BYTE notes_index[3];
};

// ecash dispenser errors and counters
struct EcashDispenserErrors
{
    DWORD failed_command_unknown;               // failed command unknown
    DWORD failed_crc;                           // invalid message crc
    DWORD failed_initializations;               // failed initialization command count
    DWORD nak_received;                         // NAK received for message
    DWORD failed_sensor_polling;
    DWORD failed_cdu_version_read;
    DWORD failed_dispensing;
    DWORD failed_test_dispensing;
    DWORD failed_note_index_settings;
    DWORD failed_nation_and_number_of_installed_cassettes;
    DWORD failed_message_header;
};

struct Cd2000DispenserSettings
{
    GeneralDispenserSettings    sub;
    BYTE message_status;
    BYTE sensor_status[13];
    BYTE error_code[5];
//    BYTE country_code;
    BYTE dispenser_mode[3];
//    BYTE number_of_cassettes;
//    BYTE version[6];
//    BYTE notes_index[3];
};

// cd2000 dispenser errors and counters
struct Cd2000DispenserErrors
{
    DWORD failed_command_unknown;               // failed command unknown
    DWORD failed_crc;                           // invalid message crc
    DWORD failed_initializations;               // failed initialization command count
    DWORD nak_received;                         // NAK received for message
    DWORD failed_sensor_polling;
    DWORD failed_dispensing;
    DWORD failed_test_dispensing;
    DWORD failed_message_header;
};

// jcm dispenser errors and counters
struct JcmDispenserErrors
{
    DWORD unknown_message_type;                 // unknown message command
    DWORD failed_command_unknown;               // failed command unknown
    DWORD failed_dispenser_response_to_poll;    // failed dispenser response to poll
    DWORD failed_crc;                           // invalid message crc
    DWORD failed_message_length;                // invalid message length
    DWORD failed_device_address;                // invalid station address
    DWORD failed_initializations;               // failed initialization command count
    DWORD nak_received;                         // NAK received for message
    DWORD resets;                               // reset count
    DWORD failed_resets;                        // failed reset command count
    DWORD failed_selecting_error;               // failed selecting count
    DWORD failed_dispenser_id;                  // failed dispenser rom id
    DWORD failed_reject;                        // failed reject
    DWORD failed_counting_and_dispensing;       // failed counting and dispensing
    DWORD failed_eot_message;                   // invalid EOT message
    DWORD failed_ack_message;                   // invalid ACK message
    DWORD failed_nak_message;                   // invalid NAK message
    DWORD initializations;                      // initialization count
};

struct PuloonDispenserErrors
{
    DWORD unknown_message_type;                 // unknown message command
    DWORD bcc_mismatch;                         // invalid message crc
    DWORD message_length_error;                 // invalid message length
    DWORD nak_received;                         // NAK received for message

	DWORD pickup_error;
	DWORD jam_at_chk;							// JAM at CHK1,2 Sensor
	DWORD overflow_bill;
	DWORD jam_at_exit;							// jam at exit sensor or EJT sensor
	DWORD jam_at_div;							// jam at div sensor
	DWORD undefined_command;
	DWORD bill_end;
	DWORD note_request_error;
	DWORD counting_error1;						// between div and ejt sensors
	DWORD counting_error2;						// between ejt and exit sensors
	DWORD reject_tray;							// reject tray is not recognized
	DWORD motor_stop;
	DWORD jam_div;								// jam at div sensor
	DWORD timeout;								// from div sensor to ejt sensor
	DWORD over_reject;
	DWORD cassette_not_recognized;
	DWORD dispensing_timeout;
	DWORD diverter_error;						// diverter solenoid or SOL Sensor error
	DWORD sol_sensor_error;
	DWORD purge_error;

};

struct MemoryDispenserValues
{
    DWORD impress_amount1;
    DWORD impress_amount2;  // used only for MULTI-DENOM
    DWORD dispense_total1;  // used only for MULTI-DENOM
    DWORD dispense_total2;  // used only for MULTI-DENOM
    DWORD dispense_grand_total;
    DWORD tickets_amount;
    DWORD number_tickets;
    DWORD reject_bills1;
    DWORD reject_bills2;    // used only for MULTI-DENOM
    DWORD last_impress_amount1;
    DWORD last_impress_amount2;  // used only for MULTI-DENOM
	DWORD reserved [10];
};

struct FileDispenserValues
{
    MemoryDispenserValues   memory;
    BYTE                    unused_space[200];
};

struct DispenserReceive
{
    WORD index;
    BYTE data[MAXIMUM_DISPENSER_RECEIVE];
};

struct JcmDispenserSettings
{
    GeneralDispenserSettings sub;
    bool dispenser_idle;
};

struct PuloonDispenserSettings
{
    GeneralDispenserSettings sub;
    bool dispenser_idle;
	WORD timeout_in_seconds;
};

struct DispenserCommand
{
    WORD length;
    BYTE data[500];
};

#pragma pack()


typedef enum DISPENSER_QUEUE_STATUS
{
	NO_STATUS,
	BILLS_REMOVED,
	REJECT,
	FATAL_ERROR
};


// CDispenserControl
class CDispenserControl
{
// public functions
public:
	CDispenserControl();
	~CDispenserControl();
    void CheckDispenser();
	void PollDispenser();
    void WriteDispenserFile();
	void SetDispenserFillInfo(IdcFillValues* fill_values);
	bool DispenseBills(DWORD ticket_id, DWORD amount, DWORD ucmc_id, BYTE mux_id);
    bool dispenseEcashBills(DWORD ticket_id, DWORD amount);
	bool dispenseCd2000Bills(DWORD ticket_id, DWORD amount);
    bool dispenseJcmBills(DWORD ticket_id, DWORD amount);
    bool dispenseDieboldBills(DWORD ticket_id, DWORD amount);
	bool dispensePuloonBills(DWORD ticket_id, DWORD amount);
	bool dispenseLcdm1000Bills(DWORD amount);
	bool dispenseLcdm2000Bills(DWORD amount);
	bool dispenseLcdm2000HundredsTwentiesBills(DWORD amount);
	void SendPuloonPurgeMessage(void);
	void SendPuloonStatusMessage(void);
	bool dispenserPayoutComplete();
	void printHundredsLow (void);
	void printTwentiesLow (void);

// public variables
public:
    EcashDispenserSettings  ecash_settings;
    EcashDispenserErrors    ecash_dispenser_errors;
    JcmDispenserErrors      jcm_dispenser_errors;
    Cd2000DispenserErrors   cd2000_dispenser_errors;
    MemoryDispenserValues   memory_dispenser_values;
    Cd2000DispenserSettings cd2000_settings;
    PuloonDispenserErrors   puloon_dispenser_errors;

	bool jcm_soft_reset_sent;

	bool pulloon_casette1_low;
	bool pulloon_casette2_low;

// private functions
private:
    void TxDispenserData(BYTE *data, WORD length);
    void CheckDispenserReceive();
    void OpenDispenserSerialPort();
    void ProcessCashDrawerCommands();

    // JCM dispenser functions
    void ProcessJcmDispenserReceive();
    void ProcessJcmDispenserCommands();
    void ProcessValidJcmDispenserReceive(WORD data_length);
    void SendJcmDispenserCommand(DispenserCommand *message);
    void PollJcmDispenser();
    void SendJcmDispenseMessage(WORD bill_count);
	DISPENSER_QUEUE_STATUS CheckJcmDispenserQueue();
    void FatalJcmDispenserError(BYTE error_code);

    // Ecash dispenser functions
    void ProcessEcashDispenserReceive();
    void FailedEcashDispenserCommand();
    void ProcessValidEcashDispenserReceive(BYTE *data);
    void PollEcashDispenser();
    void ProcessEcashDispenserCommands();
    void SendEcashDispenseMessage(WORD bill_count_100, WORD bill_count_20);
    void CheckEcashDispenserQueue();

	// CD2000
	void ProcessCd2000DispenserReceive();
	void FailedCd2000DispenserCommand();
	void ProcessValidCd2000DispenserReceive(BYTE *data);
	void PollCd2000Dispenser();
	void ProcessCd2000DispenserCommands();
	void SendCd2000DispenseMessage(WORD bill_count_100, WORD bill_count_20);
	void CheckCd2000DispenserQueue();

	// Diebold dispenser functions
	void calculateSmuxDispenserKey(void);
	void CheckDieboldDispenserQueue (void);
	void SendDieboldDispenseMessage(WORD bill_count_100, WORD bill_count_20, DWORD ticket_id);
	void ProcessDieboldDispenserReceive();

	// Puloon dispenser functions
	bool ProcessPuloonDispenserReceive();
	void ProcessPuloonDispenserCommands();
	bool ProcessPuloonDispenserReceiveCommands();
	bool processPuloonPurgeReceiveCommand();
	bool processPuloon4000PurgeReceiveCommand();
//	bool processPuloonDispenseReceiveCommand();
	bool processPuloonUpperDispenseReceiveCommand();
	bool processPuloonLowerDispenseReceiveCommand();
//	bool processPuloonUpperAndLowerDispenseReceiveCommand();
	bool processPuloonUpperAndLowerDispenseReceiveCommand(BYTE message_size, BYTE dispenser_error, DWORD reject_bills1, DWORD reject_bills2);

	bool processPuloonStatusReceiveCommand();
	bool processPuloon4000StatusReceiveCommand();
	bool processPuloonRomVersionReceiveCommand();
	bool processPuloonTestDispenseReceiveCommand();
	bool processPuloonDispenseError(BYTE error);
	bool processPuloon4000DispenseError(BYTE error);
	void SendPuloonUpperDispenseMessage(WORD bill_count);
	void SendPuloonLowerDispenseMessage(WORD bill_count);
	void SendPuloonUpperAndLowerDispenseMessage(WORD bill_count_upper, WORD bill_count_lower);
	void SendPuloon4000UpperAndLowerDispenseMessage(WORD bill_count_upper, WORD bill_count_lower);
	void SendPuloonDispenseMessage(void);
	void PollPuloonDispenser();
	DISPENSER_QUEUE_STATUS CheckPuloonDispenserQueue();
	BYTE getPuloonBCC (unsigned char* buffer, int length);
	void FatalPuloonDispenserError(BYTE error_code);
	

// private variables
private:
    DispenserReceive        dispenser_receive;
    JcmDispenserSettings    jcm_settings;
    PuloonDispenserSettings puloon_settings;

	HANDLE	hDispenserSerialPort;
	time_t	dispenser_time_check;
	unsigned char diebold_message_id;
	unsigned long diebold_dispenser_key;
	bool dispenser_payout_complete;
};


