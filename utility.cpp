/* Modification History:
Name            Date        Notes
----            ----        -----
Robert Fuller   03/08/13    Moved all the BCD utility functions from sas_com.cpp to here.
Robert Fuller   01/19/10    Implemented a file redundancy strategy for files which store the paid game locks, dispenses, and drop tickets.
Dave Elquist    04/13/05    Initial Revision.
*/

// Filename: Utility.cpp
// Misc. Functions And Types For Controller Program


#include "stdafx.h"
#include "utility.h"


CMessageBox *pMessageBox;
DWORD crc32_table[256];


// Reflection is a requirement for the official CRC-32 standard.
// You can create CRCs without it, but they won't conform to the standard.
DWORD ReflectCrc32(DWORD reference, BYTE bit_count)
{
 int iii;
 DWORD return_value = 0;

    // Swap bit 0 for bit 7
    // bit 1 for bit 6, etc.
    for (iii=1; iii < bit_count + 1; iii++)
    {
        if (reference & 1)
            return_value |= 1 << (bit_count - iii);

        reference >>= 1;
	}

 return return_value;
}

// uses crc32_table to calcualte 32-bit crc
DWORD GetCrc32(BYTE *buffer, DWORD length)
{
 DWORD crc = 0xFFFFFFFF;

    while (length--)
        crc = (crc >> 8) ^ crc32_table[(crc & 0xFF) ^ *buffer++];

 return crc ^ 0xFFFFFFFF;
}

WORD CalculateStratusCrc(BYTE *buffer, int length)
{
 WORD crc = 0;
 int iii;

    for (iii=0; iii < length; iii++)
        crc = (WORD)((crc < 0X80) ? (crc << 1) + buffer[iii] : (crc << 1) + buffer[iii] + 1);

 return crc;
}

BYTE CalculateMuxCrc(BYTE *buffer, BYTE size)
{
 BYTE iii, crc = 0;

    for (iii=0; iii < size; iii++)
        crc = (crc < 0X80) ? (crc << 1) + buffer[iii] : (crc << 1) + buffer[iii] + 1;

 return crc;
}

void logMessage(CString message)
{
 SYSTEMTIME time_struct;
 CString string_value;
 char filename[255];
 static time_t last_write_time;
 static CString first_last_write_string;
 static CString second_last_write_string;

    // prevent message log from growing out of control
    if (message != first_last_write_string && message != second_last_write_string)
    {
        second_last_write_string = first_last_write_string;
        first_last_write_string = message;
    }
    else
        if (time(NULL) < last_write_time + 120) // add 2 minute delay
            return;

    last_write_time = time(NULL);
    GetLocalTime(&time_struct);

    string_value.Format("%d/%d/%d %2.2d:%2.2d:%2.2d    ", time_struct.wMonth, time_struct.wDay, time_struct.wYear,
        time_struct.wHour, time_struct.wMinute, time_struct.wSecond);
    string_value += message + "\r\n";
    sprintf_s(filename, sizeof(filename), "%s\\%d_%d.log", CONTROLLER_LOG_PATH, time_struct.wYear, time_struct.wMonth);
    fileWrite(filename, -1, string_value.GetBuffer(0), string_value.GetLength());
}

// successful file write returns true, otherwise false
BOOL fileWriteSizeLimited(char *filename, void *file_struct, DWORD file_struct_size)
{
 BOOL successful_write = TRUE;

    if (fileWrite(filename, -1, file_struct, file_struct_size))
    {
        // check file records
        if (fileRecordTotal(filename, file_struct_size) > 2000) // maximum records to store
            if (!fileDeleteRecord(filename, file_struct_size, 0))
            {
                logMessage("fileWriteSizeLimited - unable to delete record.");
                successful_write = FALSE;
            }
    }
    else
    {
        logMessage("fileWriteSizeLimited - unable to write file.");
        successful_write = FALSE;
    }

 return successful_write;
}

// successful file write returns true, otherwise false
BOOL fileWrite(char *filename, int index, void *file_struct, DWORD file_struct_size)
{
 BOOL successful_write = TRUE;
 int fseek_result;
 FILE *file_handle;

    if (GetFileAttributes(filename) == 0xFFFFFFFF && index && index != -1)
    {
        logMessage("fileWrite - " + CString(filename) + "does not exist.");
        successful_write = FALSE;
    }
    else
    {
        if (index == -1)
            fopen_s(&file_handle, filename, "ab");
        else
        {
            if (GetFileAttributes(filename) == 0xFFFFFFFF)
                fopen_s(&file_handle, filename, "w+b");
            else
                fopen_s(&file_handle, filename, "r+b");
        }

        if (file_handle)
        {
            if (index != -1)
                fseek_result = fseek(file_handle, index * file_struct_size, SEEK_SET);
            else
                fseek_result = 0;

            if (!fseek_result)
            {
                if (fwrite(file_struct, file_struct_size, 1, file_handle) != 1)
                {
                    logMessage("fileWrite - cannot write to file " + CString(filename) + ".");
                    successful_write = FALSE;
                }
            }
            else
            {
                logMessage("fileWrite - cannot position pointer in " + CString(filename) + ".");
                successful_write = FALSE;
            }

            fclose(file_handle);
        }
        else
        {
            logMessage("fileWrite - cannot open " + CString(filename) + ".");
            successful_write = FALSE;
        }
    }

 return successful_write;
}

// successful file read returns true, otherwise false
BOOL fileRead(char *filename, DWORD index, void *file_struct, DWORD file_struct_size)
{
 FILE *file_handle;
 BOOL successful_read = FALSE;

    if (GetFileAttributes(filename) != 0xFFFFFFFF)
    {
        fopen_s(&file_handle, filename, "rb");

        if (file_handle)
        {
        	if (!fseek(file_handle, index * file_struct_size, SEEK_SET) &&
				fread(file_struct, file_struct_size, 1, file_handle))
                	successful_read = TRUE;

            fclose(file_handle);
        }
    }

    if (!successful_read)
        memset(file_struct, 0, sizeof(file_struct_size));

 return successful_read;
}

DWORD FileByteTotal(char *filename)
{
 DWORD number_of_bytes = 0;
 HANDLE hFileHandle;

    hFileHandle = CreateFile(filename, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

    if (hFileHandle != INVALID_HANDLE_VALUE)
    {
        number_of_bytes = GetFileSize(hFileHandle, NULL);
        CloseHandle(hFileHandle);

        if (number_of_bytes == 0xFFFFFFFF)
            number_of_bytes = 0;
    }

 return number_of_bytes;
}

// get number of records in file
DWORD fileRecordTotal(char *filename, DWORD record_size)
{
 DWORD number_of_records = 0;
 HANDLE hFileHandle;

    hFileHandle = CreateFile(filename, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

    if (hFileHandle != INVALID_HANDLE_VALUE)
    {
        number_of_records = GetFileSize(hFileHandle, NULL);
        CloseHandle(hFileHandle);

        if (number_of_records)
        {
            if (number_of_records != 0xFFFFFFFF)
                number_of_records /= record_size;
            else
                number_of_records = 0;
        }
    }

 return number_of_records;
}

// return record index if found, otherwise failure = -1, no record or empty file = -2
int fileGetRecord(char *filename, void *return_value, DWORD record_size, DWORD search_record)
{
 BOOL record_found = FALSE;
 int record_index = -1;
 DWORD compare_value;
 FILE *file_handle;

    if (GetFileAttributes(filename) != 0xFFFFFFFF)
    {
		fopen_s(&file_handle, filename, "rb");

        if (file_handle)
        {
            fseek(file_handle, 0, SEEK_SET);

            while (!record_found && fread(return_value, record_size, 1, file_handle))
            {
                record_index++;
                memcpy(&compare_value, return_value, sizeof(compare_value));

                if (compare_value == search_record)
                    record_found = TRUE;
            }

            if (!record_found)
                record_index = -2;

            fclose(file_handle);
        }
        else
            record_index = -1;
    }
    else
        record_index = -2;

    if (record_index < 0)
        memset(return_value, 0, record_size);

 return record_index;
}

// delete file record, return true if successful, otherwise false
BOOL fileDeleteRecord(char *filename, DWORD record_size, DWORD record_position)
{
 BOOL successful_operation = FALSE;
 FILE *file_handle;
 FILE *file_handle2;
 char *buffer;
 int record_counter = 0;
 CString old_filename, new_filename;

    buffer = new char[record_size];

    if (buffer)
    {
        fopen_s(&file_handle, filename, "rb");
        if (file_handle)
        {
            fopen_s(&file_handle2, TEMP_FILE, "wb");
            if (file_handle2)
            {
                while (fread(buffer, record_size, 1, file_handle))
                {
                    if (record_counter != record_position)
                    {
                        if (!fwrite(buffer, record_size, 1, file_handle2))
                        {
                            successful_operation = FALSE;
                            break;
                        }
                    }
                    else
                        successful_operation = TRUE;

                    record_counter++;
                }

                fclose(file_handle2);
            }

            fclose(file_handle);
        }

        delete[] buffer;
    }

    if (successful_operation)
    {
        old_filename = filename;
        new_filename = TEMP_FILE;

        if (!DeleteFile(old_filename) || !MoveFile(new_filename, old_filename))
            successful_operation = FALSE;
    }

    if (!successful_operation)
        logMessage("fileDeleteRecord - unable to delete record.");

 return successful_operation;
}

void writeRedundantFile(CString file_name, int index, BYTE* struct_ptr, int byte_count)
{
    BYTE write_count = 0;
    BOOL successful_write = FALSE;
    CString REDUNDANT_FILE = file_name + "~";
    DWORD total_file_records, total_redundant_file_records;

    while (!successful_write && (write_count < 3))
    {
        if (!fileWrite(file_name.GetBuffer(0), index, struct_ptr, byte_count))
            write_count++;
		else
			successful_write = TRUE;
    }    

    write_count = 0;
    successful_write = FALSE;

    while (!successful_write && (write_count < 3))
    {
		if (!fileWrite(REDUNDANT_FILE.GetBuffer(0), index, struct_ptr, byte_count))
            write_count++;
		else
			successful_write = TRUE;
    }    


    total_file_records = fileRecordTotal(file_name.GetBuffer(0), byte_count);
    total_redundant_file_records = fileRecordTotal(REDUNDANT_FILE.GetBuffer(0), byte_count);

    if (total_file_records != total_redundant_file_records)
    {
    	logMessage("CControllerApp::writeRedundantFile - record mismatch");

        if (total_file_records > total_redundant_file_records)
        {
            DeleteFile(REDUNDANT_FILE.GetBuffer(0));
            CopyFile(file_name.GetBuffer(0), REDUNDANT_FILE.GetBuffer(0), TRUE);
        }

        if (total_redundant_file_records > total_file_records)
        {
            DeleteFile(file_name.GetBuffer(0));
            CopyFile(REDUNDANT_FILE.GetBuffer(0), file_name.GetBuffer(0), TRUE);
        }
    }
}

char* convertTimeStamp(time_t time_stamp)
{
 static char time_string[50];
 char am_or_pm[3];
 int hour;
 struct tm convert_time_struct;
 time_t time_base = time_stamp;

	if (time_base > 2000000000 || localtime_s(&convert_time_struct, &time_base))
		memset(&convert_time_struct, 0, sizeof(convert_time_struct));

    hour = convert_time_struct.tm_hour;

    if (hour < 12)
        sprintf_s(am_or_pm, sizeof(am_or_pm), "am");
    else
    {
        if (hour > 12)
            hour -= 12;

        sprintf_s(am_or_pm, sizeof(am_or_pm), "pm");
    }

    sprintf_s(time_string, sizeof(time_string), "%d/%d/%d %2.2d:%2.2d:%2.2d %s", convert_time_struct.tm_mon + 1, convert_time_struct.tm_mday,
    	convert_time_struct.tm_year + 1900, hour, convert_time_struct.tm_min, convert_time_struct.tm_sec, am_or_pm);

 return time_string;
}

// set time stamp to current time
char* SetTimeStamp()
{
 static char time_string[50];
 time_t current_time = time(NULL);
 struct tm current_time_struct;

    memset(time_string, 0, sizeof(time_string));

    // time stamp format: char year[4], month[2], day[2], hour[2], min[2], sec[2]
	if (!localtime_s(&current_time_struct, &current_time))
        strftime(time_string, 16, "%Y%m%d%H%M%S", &current_time_struct);

 return time_string;
}

void MessageWindow(BOOL wantCancelButton, CString windowCaption, CString windowText)
{
    if (pMessageBox)
    {
        pMessageBox->DestroyWindow();
        delete pMessageBox;
    }

    pMessageBox = new CMessageBox(wantCancelButton, windowCaption, windowText);
    pMessageBox->Create(IDD_MESSAGE_BOX_DIALOG);
}

void SystemShutdown()
{
 #ifndef _DEBUG
 HANDLE hToken;
 TOKEN_PRIVILEGES tkp;

    // Get a token for this process
    if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken))
    {
        // Get the LUID for the shutdown privilege
        LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid);
        tkp.PrivilegeCount = 1;  // one privilege to set
        tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

        // Get the shutdown privilege for this process
        AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0);

        if (GetLastError() == ERROR_SUCCESS)
            ExitWindowsEx(EWX_REBOOT, SHTDN_REASON_MAJOR_SOFTWARE | SHTDN_REASON_MINOR_UPGRADE);
    }
 #endif	// #ifndef _DEBUG
}

unsigned char bcdToUnsignedChar (unsigned char bcd)
{
    unsigned char data = (bcd>>4)*10 + (bcd&0xf);

    return (data);
}

ULONG bcd4ToUnsignedLong (unsigned char bcd0,
                                unsigned char bcd1,
                                unsigned char bcd2,
                                unsigned char bcd3)
{
    ULONG data0 = (long)(bcdToUnsignedChar(bcd3) * 1); 
    ULONG data1 = (long)(bcdToUnsignedChar(bcd2) * 100); 
    ULONG data2 = (long)(bcdToUnsignedChar(bcd1) * 10000); 
    ULONG data3 = (long)(bcdToUnsignedChar(bcd0) * 1000000); 

    return (data0 + data1 + data2 + data3);
}

ULONG bcd5ToUnsignedLong (unsigned char bcd0,
                                unsigned char bcd1,
                                unsigned char bcd2,
                                unsigned char bcd3,
                                unsigned char bcd4)
{
    ULONG data0 = (long)(bcdToUnsignedChar(bcd4) * 1); 
    ULONG data1 = (long)(bcdToUnsignedChar(bcd3) * 100); 
    ULONG data2 = (long)(bcdToUnsignedChar(bcd2) * 10000); 
    ULONG data3 = (long)(bcdToUnsignedChar(bcd1) * 1000000); 
    ULONG data4 = (long)(bcdToUnsignedChar(bcd0) * 100000000); 

    return (data0 + data1 + data2 + data3 + data4);
}

ULONG bcd2ToUnsignedLong (unsigned char bcd0,
                                unsigned char bcd1)
{
    ULONG data0 = (long)(bcdToUnsignedChar(bcd1) * 1); 
    ULONG data1 = (long)(bcdToUnsignedChar(bcd0) * 100); 

    return (data0 + data1);
}

void unsignedLongToBcd4 (ULONG u_long, BCD* bcd)
{
    unsigned char dig[8];

    memset(dig, 0, 8);

    dig[0] = (unsigned char)(u_long % 10);
    dig[1] = (unsigned char)((u_long / 10) % 10);
    dig[2] = (unsigned char)((u_long / 100) % 10);
    dig[3] = (unsigned char)((u_long / 1000) % 10);
    dig[4] = (unsigned char)((u_long / 10000) % 10);
    dig[5] = (unsigned char)((u_long / 100000) % 10);
    dig[6] = (unsigned char)((u_long / 1000000) % 10);
    dig[7] = (unsigned char)((u_long / 10000000) % 10);

    bcd[3] = dig[1] * 16 + dig[0];
    bcd[2] = dig[3] * 16 + dig[2];
    bcd[1] = dig[5] * 16 + dig[4];
    bcd[0] = dig[7] * 16 + dig[6];
}

void unsignedLongToBcd5 (ULONG u_long, BCD* bcd)
{
    unsigned char dig[10];

    memset(dig, 0, 10);

    dig[0] = (unsigned char)(u_long % 10);
    dig[1] = (unsigned char)((u_long / 10) % 10);
    dig[2] = (unsigned char)((u_long / 100) % 10);
    dig[3] = (unsigned char)((u_long / 1000) % 10);
    dig[4] = (unsigned char)((u_long / 10000) % 10);
    dig[5] = (unsigned char)((u_long / 100000) % 10);
    dig[6] = (unsigned char)((u_long / 1000000) % 10);
    dig[7] = (unsigned char)((u_long / 10000000) % 10);
    dig[8] = (unsigned char)((u_long / 100000000) % 10);
    dig[9] = (unsigned char)((u_long / 1000000000) % 10);

    bcd[4] = dig[1] * 16 + dig[0];
    bcd[3] = dig[3] * 16 + dig[2];
    bcd[2] = dig[5] * 16 + dig[4];
    bcd[1] = dig[7] * 16 + dig[6];
    bcd[0] = dig[9] * 16 + dig[8];
}
